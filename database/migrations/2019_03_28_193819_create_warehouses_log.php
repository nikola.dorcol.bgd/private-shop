<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('warehouse_from')->nullable();
            $table->unsignedBigInteger('warehouse_to')->nullable();
            $table->unsignedBigInteger('product_variant_id');
            $table->bigInteger('quantity');
            $table->float('value'); // Total quantity price
            $table->text('comment')->nullable();

            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('warehouse_from')->references('id')->on('warehouses')->onDelete('cascade');
            $table->foreign('warehouse_to')->references('id')->on('warehouses')->onDelete('cascade');
            $table->foreign('product_variant_id')->references('id')->on('product_variants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_log');
    }
}
