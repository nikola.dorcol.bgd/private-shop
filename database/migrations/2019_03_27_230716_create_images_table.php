<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('imageable_id');
            $table->string('imageable_type');
            $table->string('src');
            $table->integer('type'); // Instead of enum field use CONST in model when populating.
            $table->string('storage'); // Instead of enum field use CONST in model when populating.
            $table->timestamps();

            $table->index([
                'imageable_id',
                'imageable_type'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
