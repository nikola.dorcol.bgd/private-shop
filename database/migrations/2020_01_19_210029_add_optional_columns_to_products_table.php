<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptionalColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('price_note')->default('')->after('active')->nullable();
            $table->string('width')->default('')->after('price_note')->nullable();
            $table->string('height')->default('')->after('width')->nullable();
            $table->string('length')->default('')->after('height')->nullable();
            $table->string('weight')->default('')->after('length')->nullable();
            $table->string('thickness')->default('')->after('weight')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('price_note');
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('length');
            $table->dropColumn('weight');
            $table->dropColumn('thickness');
        });
    }
}
