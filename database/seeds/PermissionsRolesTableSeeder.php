<?php

use Illuminate\Database\Seeder;

class PermissionsRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = \App\Src\Roles\Role::create([
            'name'          => 'admin',
            'description'   => 'Super admin role',
            'deletable'     => 0,
            'created_at'    => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $clientRole = \App\Src\Roles\Role::create([
            'name'          => 'client',
            'description'   => 'Client role',
            'deletable'     => 0,
            'created_at'    => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);


        $adminAccessPermission = \App\Src\Permissions\Permission::create([
            'name'          => 'admin-access',
            'description'   => 'Access to admin panel page'
        ]);

        $adminGetCategoriesPermission = \App\Src\Permissions\Permission::create([
            'name'          => 'categories-get',
            'description'   => 'Get all categories'
        ]);

        $adminViewCategoryPermission = \App\Src\Permissions\Permission::create([
            'name'          => 'category-view',
            'description'   => 'View category'
        ]);

        $adminUpdateCategoryPermission = \App\Src\Permissions\Permission::create([
            'name'          => 'category-update',
            'description'   => 'Update category'
        ]);

        $adminCreateCategoryPermission = \App\Src\Permissions\Permission::create([
            'name'          => 'category-create',
            'description'   => 'Create category'
        ]);

        $adminDeleteCategoryPermission = \App\Src\Permissions\Permission::create([
            'name'          => 'category-delete',
            'description'   => 'Delete category'
        ]);


        $adminRole->permissions()->sync([
            $adminAccessPermission->id,

            $adminGetCategoriesPermission->id,
            $adminViewCategoryPermission->id,
            $adminUpdateCategoryPermission->id,
            $adminCreateCategoryPermission->id,
            $adminDeleteCategoryPermission->id
        ]);
    }
}
