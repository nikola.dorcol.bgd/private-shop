<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    // TODO change data before deploy.
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'role_id'           => \App\Src\Roles\Role::role('admin')->first()->id,
            'name'              => 'Master Admin',
            'password'          => bcrypt(123456),
            'dob'               => \Carbon\Carbon::today()->format('Y-m-d'),
            'email'             => 'harcstefan@gmail.com',
            'email_verified_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'company_name'      => 'Harc Fitness',
            'created_at'        => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'        => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
