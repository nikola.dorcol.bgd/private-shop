<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',


    /*
    |--------------------------------------------------------------------------
    | Available Image Types
    |--------------------------------------------------------------------------
    |
    | List of all available image filters in application.
    |
    */
    'availableImageTypes' => [
        'icon',
        'logo'
    ],


    /*
    |--------------------------------------------------------------------------
    | List of image filters and their specs.
    |--------------------------------------------------------------------------
    |
    | Notice: Always define extension without dot!
    |
    */

    'icon' => [
        'extension' => 'jpg',
        'quality'   => 100,
        'width'     => 100,
        'height'    => 100
    ],

    'logo' => [
        'extension' => 'jpg',
        'quality'   => 100,
        'width'     => 200,
        'height'    => 200
    ]
];
