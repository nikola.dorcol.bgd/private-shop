<?php

return [

    'groups' => [
        'admin' => [
            'admin.*'
        ],

        'cache' => [
            'images.cache'
        ]
    ]
];