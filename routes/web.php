<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    Route::group(['middleware' => ['auth:web', 'can:admin']], function () {

        Route::get('admin', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);

        Route::get('dashboard', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);

    // Attributes routes
        include('admin/attributes.php');

    // Categories routes
        include('admin/categories.php');

    // Brands routes
        include('admin/brands.php');

    // Warehouses routes
        include('admin/warehouses.php');

    // Products routes
        include('admin/products.php');

    // Images routes
        include('admin/images.php');

    // Orders routes
        include('admin/orders.php');

    // Users routes
        include('admin/users.php');

    // Roles routes
        include('admin/roles.php');

    });
});

Route::group(['namespace' => 'Client', 'as' => 'client.'], function () {

    include('client/auth.php');

    Route::get('/'                              , ['as' => 'home'           , 'uses' => 'HomeController@index']);
    Route::get('o-nama'                         , ['as' => 'about_us'       , 'uses' => 'HomeController@aboutUs']);
    Route::get('kontakt'                        , ['as' => 'contact'        , 'uses' => 'ContactController@index']);
    Route::post('poruka'                        , ['as' => 'contact.send'   , 'uses' => 'ContactController@send']);
    Route::get('proizvod/{product}/{slug}'      , ['as' => 'product'        , 'uses' => 'ProductsController@show']);

    Route::get('proizvodi/{category?}/{slug?}/pretraga' , ['as' => 'search'     , 'uses' => 'ProductsController@search']);
    Route::get('json/{category?}/{slug?}/pretraga'      , ['as' => 'search.json', 'uses' => 'ProductsController@searchJson']);

    Route::get('privremena-korpa'                               , ['as' => 'cart.preview'           , 'uses' => 'CartController@preview']);
    Route::get('korpa'                                          , ['as' => 'cart.index'             , 'uses' => 'CartController@index'])->middleware(['auth', 'verified']);
    Route::get('cart/all'                                       , ['as' => 'cart.all'               , 'uses' => 'CartController@all'])->middleware(['auth', 'verified']);
    Route::get('cart/destroy/{cart}'                            , ['as' => 'cart.destroy'           , 'uses' => 'CartController@destroy'])->middleware(['auth', 'verified']);
    Route::post('cart/store/{activeProduct}/{productVariant}'   , ['as' => 'cart.create'            , 'uses' => 'CartController@store']);
    Route::post('cart/quantity/update'                          , ['as' => 'cart.quantity.update'   , 'uses' => 'CartController@quantityUpdate'])->middleware(['auth', 'verified']);
    Route::post('order/create'                                  , ['as' => 'order.create'           , 'uses' => 'OrderController@store'])->middleware(['auth', 'verified']);
    Route::get('porudzbina/{order}'                             , ['as' => 'order.show'             , 'uses' => 'OrderController@show'])->middleware(['auth', 'verified']);
});
