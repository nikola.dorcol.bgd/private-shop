<?php

/*
|--------------------------------------------------------------------------
| Client Routes
|--------------------------------------------------------------------------
|
| Client auth routes
|
*/

Route::get('client/callback/{provider}', ['as' => 'callback', 'uses' => 'Auth\LoginController@handleProviderCallback']);

Route::group(['prefix' => 'auth', 'as' => 'auth.', 'namespace' => 'Auth'], function () {

    Route::group(['middleware' => 'guest:api'], function () {

        Route::get('login/{provider}', ['as' => 'login', 'uses' => 'LoginController@redirectToProvider']);

        Route::post('login'     , ['as' => 'login'      , 'uses' => 'LoginController@login']);
        Route::post('register'  , ['as' => 'register'   , 'uses' => 'RegisterController@register']);

    });

    Route::group(['middleware' => 'auth:api'], function () {

        Route::post('refresh'   , ['as' => 'refresh'    , 'uses' => 'LoginController@refresh']);
        Route::post('logout'    , ['as' => 'logout'     , 'uses' => 'LoginController@logout']);
    });
});