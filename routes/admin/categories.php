<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Categories admin routes
|
*/

Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {

    Route::get('index/{any?}'       , ['as' => 'index'          , 'uses' => 'CategoriesController@index'])->where('any', '.*');
    Route::get('all'                , ['as' => 'all'            , 'uses' => 'CategoriesController@all']);
    Route::get('auto-complete'      , ['as' => 'autocomplete'   , 'uses' => 'CategoriesController@autocomplete']);
    Route::get('data-table'         , ['as' => 'dataTable'      , 'uses' => 'CategoriesController@dataTable']);
    Route::get('create'             , ['as' => 'create'         , 'uses' => 'CategoriesController@create']);
    Route::get('edit/{category}'    , ['as' => 'edit'           , 'uses' => 'CategoriesController@edit']);
    Route::get('destroy/{category}' , ['as' => 'destroy'        , 'uses' => 'CategoriesController@destroy']);

    Route::post('store'             , ['as' => 'store'          , 'uses' => 'CategoriesController@store']);
    Route::post('update/{category}' , ['as' => 'update'         , 'uses' => 'CategoriesController@update']);
});