<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Attributes admin routes
|
*/

Route::group(['prefix' => 'attributes', 'as' => 'attributes.'], function () {

    Route::get('index/{any?}'       , ['as' => 'index'      , 'uses' => 'AttributesController@index'])->where('any', '.*');
    Route::get('all'                , ['as' => 'all'        , 'uses' => 'AttributesController@all']);
    Route::get('data-table'         , ['as' => 'dataTable'  , 'uses' => 'AttributesController@dataTable']);
    Route::get('destroy/{attribute}', ['as' => 'destroy'    , 'uses' => 'AttributesController@destroy']);

    Route::post('store'             , ['as' => 'store'      , 'uses' => 'AttributesController@store']);
});