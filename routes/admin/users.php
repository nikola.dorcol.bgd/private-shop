<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Users admin routes
|
*/

Route::group(['prefix' => 'users', 'as' => 'users.'], function () {

    Route::get('index/{any?}'   , ['as' => 'index'          , 'uses' => 'UsersController@index'])->where('any', '.*');
    Route::get('data-table'     , ['as' => 'dataTable'      , 'uses' => 'UsersController@dataTable']);
    Route::get('edit/{user}'    , ['as' => 'edit'           , 'uses' => 'UsersController@edit']);
    Route::get('destroy/{user}' , ['as' => 'destroy'        , 'uses' => 'UsersController@destroy']);
    Route::get('auto-complete'  , ['as' => 'autocomplete'   , 'uses' => 'UsersController@autocomplete']);

    Route::post('update/{user}' , ['as' => 'update'     , 'uses' => 'UsersController@update']);
    Route::post('store'         , ['as' => 'store'      , 'uses' => 'UsersController@store']);
});