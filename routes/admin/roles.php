<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Roles admin routes
|
*/

Route::group(['prefix' => 'role', 'as' => 'roles.'], function () {

    Route::get('all', ['as' => 'all', 'uses' => 'RolesController@all']);
});