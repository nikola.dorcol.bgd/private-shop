<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Brands admin routes
|
*/

Route::group(['prefix' => 'brands', 'as' => 'brands.'], function () {

    Route::get('index/{any?}'       , ['as' => 'index'      , 'uses' => 'BrandsController@index'])->where('any', '.*');
    Route::get('all'                , ['as' => 'all'        , 'uses' => 'BrandsController@all']);
    Route::get('data-table'         , ['as' => 'dataTable'  , 'uses' => 'BrandsController@dataTable']);
    Route::get('create'             , ['as' => 'create'     , 'uses' => 'BrandsController@create']);
    Route::get('edit/{brand}'       , ['as' => 'edit'       , 'uses' => 'BrandsController@edit']);
    Route::get('destroy/{brand}'    , ['as' => 'destroy'     , 'uses' => 'BrandsController@destroy']);

    Route::post('store'             , ['as' => 'store'      , 'uses' => 'BrandsController@store']);
    Route::post('update/{brand}'    , ['as' => 'update'     , 'uses' => 'BrandsController@update']);
});