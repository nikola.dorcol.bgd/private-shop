<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Warehouses admin routes
|
*/

Route::group(['prefix' => 'warehouses', 'as' => 'warehouses.'], function () {

    Route::get('index/{any?}'                       , ['as' => 'index'                  , 'uses' => 'WarehousesController@index'])->where('any', '.*');
    Route::get('all'                                , ['as' => 'all'                    , 'uses' => 'WarehousesController@all']);
    Route::get('data-table'                         , ['as' => 'dataTable'              , 'uses' => 'WarehousesController@dataTable']);
    Route::get('inventory/data-table/{warehouse}'   , ['as' => 'inventory.dataTable'    , 'uses' => 'WarehousesController@inventoryDataTable']);
    Route::get('create'                             , ['as' => 'create'                 , 'uses' => 'WarehousesController@create']);
    Route::get('show/{warehouse}'                   , ['as' => 'show'                   , 'uses' => 'WarehousesController@show']);
    Route::get('edit/{warehouse}'                   , ['as' => 'edit'                   , 'uses' => 'WarehousesController@edit']);
    Route::get('destroy/{warehouse}'                , ['as' => 'destroy'                , 'uses' => 'WarehousesController@destroy']);

    Route::post('store'                             , ['as' => 'store'                  , 'uses' => 'WarehousesController@store']);
    Route::post('update/{warehouse}'                , ['as' => 'update'                 , 'uses' => 'WarehousesController@update']);
});