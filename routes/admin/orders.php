<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Orders admin routes
|
*/

Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {

    Route::get('index/{any?}'       , ['as' => 'index'      , 'uses' => 'OrdersController@index'])->where('any', '.*');
    Route::get('data-table'         , ['as' => 'dataTable'  , 'uses' => 'OrdersController@dataTable']);
    Route::get('create'             , ['as' => 'create'     , 'uses' => 'OrdersController@create']);
    Route::get('show/{order}'       , ['as' => 'show'       , 'uses' => 'OrdersController@show']);
    Route::get('edit/{order}'       , ['as' => 'edit'       , 'uses' => 'OrdersController@edit']);
    Route::get('destroy/{order}'    , ['as' => 'destroy'    , 'uses' => 'OrdersController@destroy']);

    Route::post('store'             , ['as' => 'store'      , 'uses' => 'OrdersController@store']);
    Route::post('update/{order}'    , ['as' => 'update'     , 'uses' => 'OrdersController@update']);
    Route::post('finalize/{order}'  , ['as' => 'finalize'   , 'uses' => 'OrdersController@finalize']);
});