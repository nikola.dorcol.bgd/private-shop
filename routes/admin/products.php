<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Products admin routes
|
*/

Route::group(['prefix' => 'products', 'as' => 'products.'], function () {

    Route::get('index/{any?}'                   , ['as' => 'index'              , 'uses' => 'ProductsController@index'])->where('any', '.*');
    Route::get('data-table'                     , ['as' => 'dataTable'          , 'uses' => 'ProductsController@dataTable']);
    Route::get('create'                         , ['as' => 'create'             , 'uses' => 'ProductsController@create']);
    Route::get('edit/{product}'                 , ['as' => 'edit'               , 'uses' => 'ProductsController@edit']);
    Route::get('destroy/{product}'              , ['as' => 'destroy'            , 'uses' => 'ProductsController@destroy']);
    Route::get('destroy/{product}/{image}'      , ['as' => 'destroy.image'      , 'uses' => 'ProductsController@destroyImage']);
    Route::get('auto-complete'                  , ['as' => 'autocomplete'       , 'uses' => 'ProductsController@autocomplete']);
    Route::get('order-auto-complete'            , ['as' => 'order.autocomplete' , 'uses' => 'ProductsController@orderAutocomplete']);
    Route::get('update/{product}/main/{image}'  , ['as' => 'update.image.main'  , 'uses' => 'ProductsController@updateMainImage']);

    Route::post('store'                         , ['as' => 'store'              , 'uses' => 'ProductsController@store']);
    Route::post('update/{product}'              , ['as' => 'update'             , 'uses' => 'ProductsController@update']);
    Route::post('update/{product}/active'       , ['as' => 'update.active'      , 'uses' => 'ProductsController@updateActiveStatus']);


    Route::group(['prefix' => 'recommends', 'as' => 'recommends.'], function () {

        Route::get('all'                , ['as' => 'all'        , 'uses' => 'RecommendedProductsController@all']);
        Route::get('add/{product}'      , ['as' => 'add'        , 'uses' => 'RecommendedProductsController@add']);
        Route::get('remove/{product}'   , ['as' => 'remove'     , 'uses' => 'RecommendedProductsController@remove']);
        Route::post('reorder'           , ['as' => 'reorder'    , 'uses' => 'RecommendedProductsController@reorder']);
    });
});