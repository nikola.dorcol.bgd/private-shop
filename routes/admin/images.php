<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Categories admin routes
|
*/

Route::group(['prefix' => 'images', 'as' => 'images.'], function () {

    Route::get('destroy/{image}' , ['as' => 'destroy', 'uses' => 'ImagesController@destroy']);
});