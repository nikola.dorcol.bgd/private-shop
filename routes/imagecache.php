<?php

/*
 * Override ImageCache intervention default route.
 */

Route::group(['prefix' => 'image-cache', 'as' => 'images.', 'namespace' => '\Intervention\Image'], function () {

    Route::get('{template}/{filename}', ['as' => 'cache', 'uses' => 'ImageCacheController@getResponse'])->where('filename', '(.*)');
});