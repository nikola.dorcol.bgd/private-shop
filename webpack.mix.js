const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/admin/admin.js', 'public/js/admin')
    .js('resources/js/admin/products/products.js', 'public/js/admin/products')
    .js('resources/js/admin/attributes/attributes.js', 'public/js/admin/attributes')
    .js('resources/js/admin/brands/brands.js', 'public/js/admin/brands')
    .js('resources/js/admin/categories/categories.js', 'public/js/admin/categories')
    .js('resources/js/admin/warehouses/warehouses.js', 'public/js/admin/warehouses')
    .js('resources/js/admin/orders/orders.js', 'public/js/admin/orders')
    .js('resources/js/admin/users/users.js', 'public/js/admin/users')
    .js('resources/js/client/client.js', 'public/js/client')
    .js('resources/js/client/product/product.js', 'public/js/client')
    .js('resources/js/client/search/search.js', 'public/js/client')
    .js('resources/js/client/summary/summary.js', 'public/js/client')
    .js('resources/js/client/order/order.js', 'public/js/client')
    .version();

mix.sass('resources/sass/client.scss', 'public/css/client').version();

// // TODO autoload, gloabl path define as @,...
//
// const path = require('path');
//
// mix.webpackConfig({
//     resolve: {
//         extensions: ['.js', '.vue', '.json'],
//         alias: {
//             '@mixins': path.join(__dirname, '/resources/js/admin/mixins')
//         },
//     }
// });

