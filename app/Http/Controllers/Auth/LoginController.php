<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Roles\Role;
use App\Src\Services\Models\Create\Cart\CreateCartFromSessionService;
use App\Src\Users\Requests\LoginUserRequest;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('client.security.login');
    }

    /**
     * Login user.
     *
     * @param LoginUserRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function login(LoginUserRequest $request)
    {
        if (!$user = auth()->guard('web')->attempt($request->validated())) {

            return redirect()->route('login')->withErrors(['email' => 'Neispravni Kredencijali!']);
        }

        (new CreateCartFromSessionService)->create();

        return redirect()->route(auth()->user()->role->name == 'admin' ? 'admin.products.index' : 'client.home');
    }


    /**
     * Log the user out.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        auth()->guard('web')->logout();

        return redirect()->route('client.home');
    }
}
