<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Mails\WelcomeMail;
use App\Src\Services\Auth\RegisterEmailUser;
use App\Src\Users\Requests\UserRegisterRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{

    public function showRegistrationForm()
    {
        return view('client.security.register');
    }


    public function register(UserRegisterRequest $request, RegisterEmailUser $registerEmailUser)
    {
        try {

            DB::beginTransaction();

            $registerEmailUser->register($request->validated());

        } catch (\Exception $exception) {

            DB::rollBack();

            return redirect()->back()->withErrors(['message' => $exception->getMessage()]);
        }

        DB::commit();

        return redirect()->route('login')->with('status', 'Uspesno ste se registrovali, verifikacioni email je poslat!');
    }
}