<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Src\Mails\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('client.contact');
    }


    public function send(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required|email',
            'message'   => 'required'
        ]);

        Mail::to('harcstefan@gmail.com')
            ->queue(new ContactMail($request->only('name', 'email', 'message')));

        return redirect()->back()->with(['status' => 'Poruka je uspesno poslata!']);
    }
}