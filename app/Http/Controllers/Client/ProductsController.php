<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Brands\Contracts\BrandRepositoryInterface;
use App\Src\Categories\Category;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Products\Product;
use App\Src\Products\Resources\ProductSearchResource;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\View\View;

class ProductsController extends Controller
{
    /**
     * Show product data.
     *
     * @param Product $product
     * @param AttributeRepositoryInterface $attributeRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Product $product, AttributeRepositoryInterface $attributeRepository)
    {
        $product->load('brand', 'variants.productAttributeValues.attributeValue.productAttribute', 'variants.warehouseInventories.warehouse');

        $attributes = $attributeRepository->getProductAttributes($product);

        return view('client.product', compact('product', 'attributes'));
    }


    /**
     * Search products table.
     *
     * @param Category $category
     * @param Request $request
     * @param BrandRepositoryInterface $brandRepository
     * @param ProductRepositoryInterface $productRepository
     * @return AnonymousResourceCollection|Factory|View
     */
    public function search(Category $category, Request $request, BrandRepositoryInterface $brandRepository, ProductRepositoryInterface $productRepository)
    {
        $products = $productRepository->searchProducts($request->all(), $category);

        $products->appends($request->all());

        $brands         = $brandRepository->all();
        $subCategories  = $category->children;

        return view('client.products', compact('products', 'brands', 'category', 'subCategories'));
    }


    /**
     * Search products table.
     *
     * @param Category $category
     * @param Request $request
     * @param BrandRepositoryInterface $brandRepository
     * @param ProductRepositoryInterface $productRepository
     * @return AnonymousResourceCollection|Factory|View
     */
    public function searchJson(Category $category, Request $request, BrandRepositoryInterface $brandRepository, ProductRepositoryInterface $productRepository)
    {
        $products = $productRepository->searchProducts($request->all(), $category);

        $products->appends($request->all());

        return ProductSearchResource::collection($products);
    }
}