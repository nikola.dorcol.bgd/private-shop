<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Src\Services\Home\GetHomeDataService;

class HomeController extends Controller
{
    /**
     * Index page
     *
     * @param GetHomeDataService $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetHomeDataService $service)
    {
        return view('client.home')->with($service->getData());
    }


    /**
     * About us page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function aboutUs()
    {
        return view('client.aboutUs');
    }


    /**
     * Address/Contact page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return view('client.address');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        return view('client.security.register');
    }

    /**
     * Products show/search page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function products()
    {
        return view('client.products');
    }
}