<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Orders\Order;
use App\Src\Orders\Queries\LoadShowOrderHelper;
use App\Src\Orders\Requests\ClientStoreOrderRequest;
use App\Src\Services\Models\Create\Order\ClientCreateOrderService;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Show order info.
     *
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Order $order)
    {
        if ($order->user_id !== auth()->user()->id) {

            abort(404);
        }

        $order->load(LoadShowOrderHelper::loadArray());

        return view('client.order', compact('order'));
    }


    /**
     * Store order.
     *
     * @param ClientStoreOrderRequest $request
     * @param ClientCreateOrderService $clientCreateOrderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ClientStoreOrderRequest $request, ClientCreateOrderService $clientCreateOrderService)
    {
        try {

            DB::beginTransaction();

            event(new CreateModelEvent($clientCreateOrderService, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json($exception->getMessage(), 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }
}