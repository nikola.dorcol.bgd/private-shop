<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Base\Exceptions\ModelsNotUpdatedException;
use App\Src\Carts\Cart;
use App\Src\Carts\Contracts\CartRepositoryInterface;
use App\Src\Carts\Requests\StoreCartRequest;
use App\Src\Carts\Requests\UpdateCartQuantityRequest;
use App\Src\Carts\Resources\CartSummaryResource;
use App\Src\Products\Product;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use App\Src\Products\Variants\ProductVariant;
use App\Src\Services\Contracts\CreateCartInterface;
use App\Src\Services\Models\Destroy\Cart\DestroyCartService;
use App\Src\Services\Models\Edit\Cart\EditManyCartQuantities;
use App\Src\Services\Models\Get\Cart\GetCartSeessionService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Preview cart for unauth users.
     *
     * @param GetCartSeessionService $cartSeessionService
     * @param ProductVariantRepositoryInterface $productVariantRepository
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function preview(GetCartSeessionService $cartSeessionService, ProductVariantRepositoryInterface $productVariantRepository)
    {
        $variants = $productVariantRepository->findByIds(array_column($cartSeessionService->get(), 'product_variant'));

        return view('client.cart-preview', compact('variants'));
    }


    /**
     * Show cart.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('client.summary');
    }


    /**
     * Store cart for selected product variant.
     *
     * @param StoreCartRequest $request
     * @param Product $product
     * @param ProductVariant $productVariant
     * @param CreateCartInterface $createCart
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCartRequest $request, Product $product, ProductVariant $productVariant, CreateCartInterface $createCart)
    {
        if (! $createCart->create($productVariant, $request->validated())) {

            return response()->json(['message' => 'Error'], 400);
        }

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Get cart summary
     *
     * @param CartRepositoryInterface $cartRepository
     * @return AnonymousResourceCollection
     */
    public function all(CartRepositoryInterface $cartRepository): AnonymousResourceCollection
    {
        return CartSummaryResource::collection($cartRepository->getUserCart(auth()->user()));
    }


    /**
     * Destroy cart.
     *
     * @param Cart $cart
     * @param DestroyCartService $destroyCartService
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Cart $cart, DestroyCartService $destroyCartService)
    {
        try {

            DB::beginTransaction();

            event(new DestroyModelEvent($destroyCartService, $cart));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Update items quantity in cart.
     *
     * @param UpdateCartQuantityRequest $request
     * @param EditManyCartQuantities $editManyCartQuantities
     * @return \Illuminate\Http\JsonResponse
     */
    public function quantityUpdate(UpdateCartQuantityRequest $request, EditManyCartQuantities $editManyCartQuantities)
    {
        try {

            DB::beginTransaction();

            if (! $editManyCartQuantities->edit($request->validated())) {

                throw new ModelsNotUpdatedException(Errors::MODElS_NOT_UPDATED['error_message']);
            }

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }
}
