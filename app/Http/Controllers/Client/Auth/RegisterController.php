<?php

namespace App\Http\Controllers\Client\Auth;

use App\Src\Base\Errors;
use App\Src\Mails\WelcomeMail;
use App\Src\Users\Events\RegisterUserEvent;
use App\Src\Users\Requests\RegisterUserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{

    /**
     * Register user.
     *
     * @param RegisterUserRequest $request
     * @return JsonResponse
     */
    public function register(RegisterUserRequest $request): JsonResponse
    {

        DB::beginTransaction();

        try {

            event(new RegisterUserEvent($request->validated(), 'email'));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::USER_NOT_REGISTERED, 400);
        }

        DB::commit();

        return response()->json(auth()->guard('api')->user(), 200);
    }
}
