<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Users\Events\RegisterUserEvent;
use App\Src\Users\Requests\LoginUserRequest;
use App\Src\Users\Requests\OauthLoginUserRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /**
     * Login user.
     *
     * @param LoginUserRequest $request
     * @return JsonResponse
     */
    public function login(LoginUserRequest $request): JsonResponse
    {
        if (! $token = auth()->guard('api')->attempt($request->validated())) {

            return response()->json(Errors::USER_UNAUTHORISED, 401);
        }

        return response()->json([
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth()->guard('api')->factory()->getTTL() * 60
        ]);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        auth()->guard('api')->logout();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        return response()->json([
            'access_token'  => auth()->guard('api')->refresh(),
            'token_type'    => 'bearer',
            'expires_in'    => auth()->guard('api')->factory()->getTTL() * 60
        ]);
    }


    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @param $provider
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }


    /**
     * Obtain the user information from GitHub.
     *
     * @param OauthLoginUserRequest $request
     * @param $provider
     * @return JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback(OauthLoginUserRequest $request, $provider)
    {
        try {

            $user = Socialite::driver($provider)->user();

        } catch (\Exception $exception) {

            return redirect()->route('client.login');
        }

        $data = [
            'provider'          => $provider,
            'provider_user_id'  => $user->getId(),
            'name'              => $user->getName() ? $user->getName() : '',
            'email'             => $user->getEmail() ? $user->getEmail() : ''
        ];

        DB::beginTransaction();

        try {

            event(new RegisterUserEvent($data, 'oauth'));

        } catch (\Exception $exception) {

            DB::rollBack();

            return redirect()->route('client.login');
        }

        DB::commit();

        return redirect()->route('client.home');
    }
}
