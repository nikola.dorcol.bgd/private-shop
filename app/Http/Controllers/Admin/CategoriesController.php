<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Base\Events\EditModelEvent;
use App\Src\Base\Requests\AutocompleteRequest;
use App\Src\Base\Requests\DataTableRequest;
use App\Src\Categories\Category;
use App\Src\Categories\Contracts\CategoryRepositoryInterface;
use App\Src\Categories\Requests\StoreCategoryRequest;
use App\Src\Categories\Requests\UpdateCategoryRequest;
use App\Src\Categories\Resources\AutocompleteCategoryResource;
use App\Src\Categories\Resources\DataTableCategoryResource;
use App\Src\Categories\Resources\CategoryResource;
use App\Src\Categories\Validations\DestroyCategoryValidation;
use App\Src\Services\Models\Create\CreateCategoryService;
use App\Src\Services\Models\Destroy\DestroyCategoryService;
use App\Src\Services\Models\Edit\EditCategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    /**
     * Render categories admin page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.categories.index');
    }


    /**
     * Display a listing of the resource.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     * @return AnonymousResourceCollection
     */
    public function all(CategoryRepositoryInterface $categoryRepository): AnonymousResourceCollection
    {
        return CategoryResource::collection($categoryRepository->getAll());
    }


    /**
     * Get results for given criteria.
     *
     * @param AutocompleteRequest $request
     * @param CategoryRepositoryInterface $categoryRepository
     * @return AnonymousResourceCollection
     * @internal param ProductRepositoryInterface $productRepository
     */
    public function autocomplete(AutocompleteRequest $request, CategoryRepositoryInterface $categoryRepository): AnonymousResourceCollection
    {
        return AutocompleteCategoryResource::collection($categoryRepository->autocomplete($request->get('query')));
    }


    /**
     * Display a listing of the resource.
     *
     * @param DataTableRequest $request
     * @param CategoryRepositoryInterface $categoryRepository
     * @return AnonymousResourceCollection
     */
    public function dataTable(DataTableRequest $request, CategoryRepositoryInterface $categoryRepository): AnonymousResourceCollection
    {
        return DataTableCategoryResource::collection($categoryRepository->dataTable($request->all()));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCategoryRequest $request
     * @param CreateCategoryService $createCategoryService
     * @return JsonResponse
     */
    public function store(StoreCategoryRequest $request, CreateCategoryService $createCategoryService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new CreateModelEvent($createCategoryService, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_CREATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return JsonResponse
     */
    public function show(Category $category): JsonResponse
    {
        return response()->json($category, 200);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return CategoryResource
     */
    public function edit(Category $category): CategoryResource
    {
        return new CategoryResource($category);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCategoryRequest $request
     * @param Category $category
     * @param EditCategoryService $editCategoryService
     * @return JsonResponse
     */
    public function update(UpdateCategoryRequest $request, Category $category, EditCategoryService $editCategoryService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new EditModelEvent($editCategoryService, $category, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_UPDATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @param DestroyCategoryService $destroyCategoryService
     * @param DestroyCategoryValidation $destroyCategoryValidation
     * @return JsonResponse
     */
    public function destroy(Category $category, DestroyCategoryService $destroyCategoryService, DestroyCategoryValidation $destroyCategoryValidation): JsonResponse
    {
        if ($destroyCategoryValidation->validate($category)) {

            return response()->json(Errors::MODEL_HAS_RELATIONSHIP_DATA, 400);
        }

        try {

            DB::beginTransaction();

            event(new DestroyModelEvent($destroyCategoryService, $category));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }
}
