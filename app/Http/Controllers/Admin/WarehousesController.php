<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Base\Events\EditModelEvent;
use App\Src\Base\Requests\DataTableRequest;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Products\Resources\ProductDataTableResource;
use App\Src\Services\Models\Create\Warehouse\CreateWarehouseService;
use App\Src\Services\Models\Destroy\Warehouse\DestroyWarehouseService;
use App\Src\Services\Models\Edit\Warehouse\EditWarehouseService;
use App\Src\Warehouses\Contracts\WarehouseRepositoryInterface;
use App\Src\Warehouses\Requests\StoreWarehouseRequest;
use App\Src\Warehouses\Requests\UpdateWarehouseRequest;
use App\Src\Warehouses\Resources\WarehouseDataTableResource;
use App\Src\Warehouses\Resources\WarehouseResource;
use App\Src\Warehouses\Validations\DestroyWarehouseValidation;
use App\Src\Warehouses\Warehouse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class WarehousesController extends Controller
{
    /**
     * Render warehouses admin page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.warehouses.index');
    }


    /**
     * Display a listing of the resource.
     *
     * @param WarehouseRepositoryInterface $warehouseRepository
     * @return AnonymousResourceCollection
     */
    public function all(WarehouseRepositoryInterface $warehouseRepository): AnonymousResourceCollection
    {
        return WarehouseResource::collection($warehouseRepository->getAll());
    }


    /**
     * Display a listing of the resource.
     *
     * @param DataTableRequest $request
     * @param WarehouseRepositoryInterface $warehouseRepository
     * @return AnonymousResourceCollection
     */
    public function dataTable(DataTableRequest $request, WarehouseRepositoryInterface $warehouseRepository): AnonymousResourceCollection
    {
        return WarehouseDataTableResource::collection($warehouseRepository->dataTable($request->all()));
    }


    /**
     * Get products that belongs to warehouse.
     *
     * @param DataTableRequest $request
     * @param Warehouse $warehouse
     * @param ProductRepositoryInterface $productRepository
     * @return AnonymousResourceCollection
     */
    public function inventoryDataTable(DataTableRequest $request, Warehouse $warehouse, ProductRepositoryInterface $productRepository): AnonymousResourceCollection
    {
        return ProductDataTableResource::collection($productRepository->getProductInventory($warehouse, $request->all()));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreWarehouseRequest $request
     * @param CreateWarehouseService $createWarehouseService
     * @return JsonResponse
     */
    public function store(StoreWarehouseRequest $request, CreateWarehouseService $createWarehouseService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new CreateModelEvent($createWarehouseService, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_CREATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param Warehouse $warehouse
     * @return WarehouseResource
     */
    public function show(Warehouse $warehouse): WarehouseResource
    {
        return new WarehouseResource($warehouse);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Warehouse $warehouse
     * @return WarehouseResource
     */
    public function edit(Warehouse $warehouse): WarehouseResource
    {
        return new WarehouseResource($warehouse);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateWarehouseRequest $request
     * @param Warehouse $warehouse
     * @param EditWarehouseService $editWarehouseService
     * @return JsonResponse
     */
    public function update(UpdateWarehouseRequest $request, Warehouse $warehouse, EditWarehouseService $editWarehouseService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new EditModelEvent($editWarehouseService, $warehouse, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_UPDATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Warehouse $warehouse
     * @param DestroyWarehouseService $destroyWarehouseService
     * @param DestroyWarehouseValidation $destroyWarehouseValidation
     * @return JsonResponse
     */
    public function destroy(Warehouse $warehouse, DestroyWarehouseService $destroyWarehouseService, DestroyWarehouseValidation $destroyWarehouseValidation): JsonResponse
    {
        if ($destroyWarehouseValidation->validate($warehouse)) {

            return response()->json(Errors::MODEL_HAS_RELATIONSHIP_DATA, 400);
        }

        try {

            DB::beginTransaction();

            event(new DestroyModelEvent($destroyWarehouseService, $warehouse));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }
}
