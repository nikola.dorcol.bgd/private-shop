<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Base\Events\EditModelEvent;
use App\Src\Base\Requests\AutocompleteRequest;
use App\Src\Base\Requests\DataTableRequest;
use App\Src\Services\Models\Create\User\AdminCreateUserService;
use App\Src\Services\Models\Destroy\User\DestroyUserService;
use App\Src\Services\Models\Edit\User\EditUserService;
use App\Src\Users\Contracts\UserRepositoryInterface;
use App\Src\Users\Requests\StoreUserRequest;
use App\Src\Users\Requests\UpdateUserRequest;
use App\Src\Users\Resources\AutocompleteUserResource;
use App\Src\Users\Resources\EditUserResource;
use App\Src\Users\Resources\UserDataTableResource;
use App\Src\Users\User;
use App\Src\Users\Validations\DestroyUserValidation;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.users.index');
    }


    /**
     * Display a listing of the resource.
     *
     * @param DataTableRequest $request
     * @param UserRepositoryInterface $userRepository
     * @return AnonymousResourceCollection
     */
    public function dataTable(DataTableRequest $request, UserRepositoryInterface $userRepository): AnonymousResourceCollection
    {
        return UserDataTableResource::collection($userRepository->dataTable($request->all()));
    }


    /**
     * Get results for given criteria.
     *
     * @param AutocompleteRequest $request
     * @param UserRepositoryInterface $userRepository
     * @return AnonymousResourceCollection
     */
    public function autocomplete(AutocompleteRequest $request, UserRepositoryInterface $userRepository): AnonymousResourceCollection
    {
        return AutocompleteUserResource::collection($userRepository->autocomplete($request->get('query')));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @param AdminCreateUserService $adminCreateUserService
     * @return JsonResponse
     */
    public function store(StoreUserRequest $request, AdminCreateUserService $adminCreateUserService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new CreateModelEvent($adminCreateUserService, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json($exception, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return EditUserResource
     */
    public function edit(User $user): EditUserResource
    {
        return new EditUserResource($user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @param EditUserService $editUserService
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request, User $user, EditUserService $editUserService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new EditModelEvent($editUserService, $user, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_UPDATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @param DestroyUserValidation $destroyUserValidation
     * @param DestroyUserService $destroyUserService
     * @return JsonResponse
     */
    public function destroy(User $user, DestroyUserValidation $destroyUserValidation, DestroyUserService $destroyUserService): JsonResponse
    {
        if (! $destroyUserValidation->validate($user)) {

            return response()->json(Errors::MODEL_HAS_RELATIONSHIP_DATA, 400);
        }

        try {

            DB::beginTransaction();

            event(new DestroyModelEvent($destroyUserService, $user));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }
}
