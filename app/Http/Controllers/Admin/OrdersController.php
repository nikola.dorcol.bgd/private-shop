<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Base\Events\EditModelEvent;
use App\Src\Base\Requests\DataTableRequest;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use App\Src\Orders\Order;
use App\Src\Orders\ProductVariants\Resources\ShowOrderResource;
use App\Src\Orders\Queries\LoadEditOrderHelper;
use App\Src\Orders\Queries\LoadShowOrderHelper;
use App\Src\Orders\Requests\AdminStoreOrderRequest;
use App\Src\Orders\Requests\AdminUpdateOrderRequest;
use App\Src\Orders\Requests\FinalizeOrderRequest;
use App\Src\Orders\Resources\EditOrderResource;
use App\Src\Orders\Resources\OrderDataTableResource;
use App\Src\Orders\Validations\HandleOrderValidation;
use App\Src\Services\Models\Create\Order\AdminCreateOrderService;
use App\Src\Services\Models\Destroy\Order\DestroyOrderService;
use App\Src\Services\Models\Edit\Order\EditOrderService;
use App\Src\Services\Models\Edit\Order\FinalizeOrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.orders.index');
    }


    /**
     * Display a listing of the resource.
     *
     * @param DataTableRequest $request
     * @param OrderRepositoryInterface $orderRepository
     * @return AnonymousResourceCollection
     */
    public function dataTable(DataTableRequest $request, OrderRepositoryInterface $orderRepository): AnonymousResourceCollection
    {
        return OrderDataTableResource::collection($orderRepository->dataTable($request->all()));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param AdminStoreOrderRequest $request
     * @param AdminCreateOrderService $adminCreateOrderService
     * @return JsonResponse
     */
    public function store(AdminStoreOrderRequest $request, AdminCreateOrderService $adminCreateOrderService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new CreateModelEvent($adminCreateOrderService, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json($exception->getMessage(), 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param Order $order
     * @return ShowOrderResource
     */
    public function show(Order $order): ShowOrderResource
    {
        return new ShowOrderResource($order->load(LoadShowOrderHelper::loadArray()));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Order $order
     * @param HandleOrderValidation $handleOrderValidation
     * @return EditOrderResource
     */
    public function edit(Order $order, HandleOrderValidation $handleOrderValidation): EditOrderResource
    {
        if ($handleOrderValidation->validate($order)) {

            return response()->json(Errors::ORDER_IS_FINAL, 400);
        }
        
        return new EditOrderResource($order->load(LoadEditOrderHelper::loadArray()));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param AdminUpdateOrderRequest $request
     * @param Order $order
     * @param HandleOrderValidation $handleOrderValidation
     * @param EditOrderService $editOrderService
     * @return \Illuminate\Http\Response
     */
    public function update(AdminUpdateOrderRequest $request, Order $order, HandleOrderValidation $handleOrderValidation, EditOrderService $editOrderService)
    {
        if ($handleOrderValidation->validate($order)) {

            return response()->json(Errors::ORDER_IS_FINAL, 400);
        }

        try {

            DB::beginTransaction();

            event(new EditModelEvent($editOrderService, $order, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_UPDATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Finalize order.
     *
     * @param FinalizeOrderRequest $request
     * @param Order $order
     * @param HandleOrderValidation $handleOrderValidation
     * @param FinalizeOrderService $finalizeOrderService
     * @return JsonResponse
     */
    public function finalize(FinalizeOrderRequest $request, Order $order, HandleOrderValidation $handleOrderValidation, FinalizeOrderService $finalizeOrderService)
    {
        if ($handleOrderValidation->validate($order)) {

            return response()->json(Errors::ORDER_IS_FINAL, 400);
        }

        try {

            DB::beginTransaction();

            event(new EditModelEvent($finalizeOrderService, $order, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_UPDATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Order $order
     * @param DestroyOrderService $destroyOrderService
     * @param HandleOrderValidation $handleOrderValidation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order, DestroyOrderService $destroyOrderService, HandleOrderValidation $handleOrderValidation)
    {
        if ($handleOrderValidation->validate($order)) {

            return response()->json(Errors::ORDER_IS_FINAL, 400);
        }

        try {

            DB::beginTransaction();

            event(new DestroyModelEvent($destroyOrderService, $order));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }
}
