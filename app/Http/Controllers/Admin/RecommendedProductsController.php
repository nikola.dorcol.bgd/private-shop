<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Products\Product;
use App\Src\Products\Recommended\RecommendedProduct;
use App\Src\Products\Recommended\Requests\ReorderRecommendedProductsRequest;
use App\Src\Products\Resources\ProductResource;
use App\Src\Services\Models\Create\Product\CreateRecommendedProductService;
use App\Src\Services\Models\Destroy\Product\DestroyRecommendedProductService;
use App\Src\Services\Models\Edit\Product\ReorderRecommendedProductsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class RecommendedProductsController extends Controller
{
    /**
     * Get all recommended products.
     *
     * @param ProductRepositoryInterface $productRepository
     * @return AnonymousResourceCollection
     */
    public function all(ProductRepositoryInterface $productRepository): AnonymousResourceCollection
    {
        return ProductResource::collection($productRepository->getRecommendedProducts());
    }


    /**
     * Add product to recommendations.
     *
     * @param Product $product
     * @param CreateRecommendedProductService $createRecommendedProductService
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Product $product, CreateRecommendedProductService $createRecommendedProductService)
    {
        try {

            DB::beginTransaction();

            event(new CreateModelEvent($createRecommendedProductService, ['product_id' => $product->id]));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_CREATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Remove product from recommendations.
     *
     * @param Product $product
     * @param DestroyRecommendedProductService $destroyRecommendedProductService
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Product $product, DestroyRecommendedProductService $destroyRecommendedProductService)
    {
        try {

            DB::beginTransaction();

            event(new DestroyModelEvent($destroyRecommendedProductService, $product->recommended));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Reorder recommended products
     *
     * @param ReorderRecommendedProductsRequest $request
     * @param ReorderRecommendedProductsService $reorderService
     * @return JsonResponse
     */
    public function reorder(ReorderRecommendedProductsRequest $request, ReorderRecommendedProductsService $reorderService): JsonResponse
    {
        return response()->json($reorderService->reorder($request->recommended_products), 200);
    }
}