<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Base\Events\EditModelEvent;
use App\Src\Base\Requests\DataTableRequest;
use App\Src\Brands\Brand;
use App\Src\Brands\Contracts\BrandRepositoryInterface;
use App\Src\Brands\Requests\StoreBrandRequest;
use App\Src\Brands\Requests\UpdateBrandRequest;
use App\Src\Brands\Resources\BrandDataTableResource;
use App\Src\Brands\Resources\BrandResource;
use App\Src\Brands\Resources\EditBrandResource;
use App\Src\Brands\Validations\DestroyBrandValidation;
use App\Src\Services\Models\Create\CreateBrandService;
use App\Src\Services\Models\Destroy\DestroyBrandService;
use App\Src\Services\Models\Edit\EditBrandService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class BrandsController extends Controller
{
    /**
     * Render brands admin page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.brands.index');
    }


    /**
     * Display a listing of the resource.
     *
     * @param BrandRepositoryInterface $brandRepository
     * @return AnonymousResourceCollection
     */
    public function all(BrandRepositoryInterface $brandRepository): AnonymousResourceCollection
    {
        return BrandResource::collection($brandRepository->getAll());
    }


    /**
     * Display a listing of the resource.
     *
     * @param DataTableRequest $request
     * @param BrandRepositoryInterface $brandRepository
     * @return AnonymousResourceCollection
     */
    public function dataTable(DataTableRequest $request, BrandRepositoryInterface $brandRepository): AnonymousResourceCollection
    {
        return BrandDataTableResource::collection($brandRepository->dataTable($request->all()));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBrandRequest $request
     * @param CreateBrandService $createBrandService
     * @return JsonResponse
     */
    public function store(StoreBrandRequest $request, CreateBrandService $createBrandService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new CreateModelEvent($createBrandService, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_CREATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param Brand $brand
     * @return JsonResponse
     */
    public function show(Brand $brand): JsonResponse
    {
        return response()->json($brand, 200);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Brand $brand
     * @return EditBrandResource
     */
    public function edit(Brand $brand): EditBrandResource
    {
        return new EditBrandResource($brand);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBrandRequest $request
     * @param Brand $brand
     * @param EditBrandService $editBrandService
     * @return JsonResponse
     */
    public function update(UpdateBrandRequest $request, Brand $brand, EditBrandService $editBrandService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new EditModelEvent($editBrandService, $brand, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_UPDATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $brand
     * @param DestroyBrandService $destroyBrandService
     * @param DestroyBrandValidation $destroyBrandValidation
     * @return JsonResponse
     */
    public function destroy(Brand $brand, DestroyBrandService $destroyBrandService, DestroyBrandValidation $destroyBrandValidation): JsonResponse
    {
        if ($destroyBrandValidation->validate($brand)) {

            return response()->json(Errors::MODEL_HAS_RELATIONSHIP_DATA, 400);
        }

        try {

            DB::beginTransaction();

            event(new DestroyModelEvent($destroyBrandService, $brand));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }
}
