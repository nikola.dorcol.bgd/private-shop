<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Src\Products\Variants\ProductVariant;

class DashboardController extends Controller
{

    public function index()
    {
        return view('admin.pages.dashboard.index');
    }
}