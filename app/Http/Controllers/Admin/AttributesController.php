<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Src\Attributes\Attribute;
use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Attributes\Requests\StoreAttributeRequest;
use App\Src\Attributes\Resources\AttributeDataTableResource;
use App\Src\Attributes\Resources\AttributeResource;
use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Requests\DataTableRequest;
use App\Src\Services\Models\Create\Attribute\CreateAttributeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;

class AttributesController extends Controller
{

    /**
     * Render attributes admin page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.attributes.index');
    }


    /**
     * Display a listing of the resource.
     *
     * @param AttributeRepositoryInterface $attributeRepository
     * @return AnonymousResourceCollection
     */
    public function all(AttributeRepositoryInterface $attributeRepository): AnonymousResourceCollection
    {
        return AttributeResource::collection($attributeRepository->getAll());
    }


    /**
     * Display a listing of the resource.
     *
     * @param DataTableRequest $request
     * @param AttributeRepositoryInterface $attributeRepository
     * @return AnonymousResourceCollection
     */
    public function dataTable(DataTableRequest $request, AttributeRepositoryInterface $attributeRepository): AnonymousResourceCollection
    {
        return AttributeDataTableResource::collection($attributeRepository->dataTable($request->all()));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAttributeRequest $request
     * @param CreateAttributeService $createAttributeService
     * @return JsonResponse
     */
    public function store(StoreAttributeRequest $request, CreateAttributeService $createAttributeService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new CreateModelEvent($createAttributeService, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json($exception->getMessage(), 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        //
    }
}
