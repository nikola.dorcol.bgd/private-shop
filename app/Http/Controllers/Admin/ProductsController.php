<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Base\Events\EditModelEvent;
use App\Src\Base\Requests\AutocompleteRequest;
use App\Src\Base\Requests\DataTableRequest;
use App\Src\Images\Image;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Products\Product;
use App\Src\Products\Requests\UpdateActiveStatusRequest;
use App\Src\Products\Requests\UpdateProductRequest;
use App\Src\Products\Resources\ProductAutocompleteResource;
use App\Src\Products\Resources\EditProductResource;
use App\Src\Products\Resources\ProductDataTableResource;
use App\Src\Products\Requests\StoreProductRequest;
use App\Src\Products\Resources\ProductOrderAutocompleteResource;
use App\Src\Products\Validations\DestroyProductImageValidation;
use App\Src\Products\Validations\DestroyProductValidation;
use App\Src\Services\Models\Create\Product\CreateProductService;
use App\Src\Services\Models\Destroy\Image\DestroyProductImageService;
use App\Src\Services\Models\Destroy\Product\DestroyProductService;
use App\Src\Services\Models\Edit\Product\EditProductActiveStatusService;
use App\Src\Services\Models\Edit\Product\EditProductMainImage;
use App\Src\Services\Models\Edit\Product\EditProductService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ProductsController extends Controller
{
    /**
     * Render products admin page.
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.pages.products.index');
    }


    /**
     * Display a listing of the resource.
     *
     * @param DataTableRequest $request
     * @param ProductRepositoryInterface $productRepository
     * @return AnonymousResourceCollection
     */
    public function dataTable(DataTableRequest $request, ProductRepositoryInterface $productRepository): AnonymousResourceCollection
    {
        return ProductDataTableResource::collection($productRepository->dataTable($request->all()));
    }


    /**
     * Get results for given criteria.
     *
     * @param AutocompleteRequest $request
     * @param ProductRepositoryInterface $productRepository
     * @return AnonymousResourceCollection
     */
    public function autocomplete(AutocompleteRequest $request, ProductRepositoryInterface $productRepository): AnonymousResourceCollection
    {
        return ProductAutocompleteResource::collection($productRepository->autocomplete($request->get('query')));
    }


    /**
     * Get results for given criteria when adding products to order.
     *
     * @param AutocompleteRequest $request
     * @param ProductRepositoryInterface $productRepository
     * @return AnonymousResourceCollection
     */
    public function orderAutocomplete(AutocompleteRequest $request, ProductRepositoryInterface $productRepository): AnonymousResourceCollection
    {
        return ProductOrderAutocompleteResource::collection($productRepository->searchAvailableProducts($request->get('query'), (int) $request->get('status')));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProductRequest $request
     * @param CreateProductService $createProductService
     * @return JsonResponse
     */
    public function store(StoreProductRequest $request, CreateProductService $createProductService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new CreateModelEvent($createProductService, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_CREATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return JsonResponse
     */
    public function show(Product $product): JsonResponse
    {
        return response()->json($product);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return EditProductResource
     */
    public function edit(Product $product): EditProductResource
    {
        return new EditProductResource($product->load(
            'brand',
            'images',
            'categories',
            'variants.productAttributeValues.attributeValue.productAttribute',
            'variants.warehouseInventories',
            'relatedProducts'
        ));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @param Product $product
     * @param EditProductService $editProductService
     * @return JsonResponse
     */
    public function update(UpdateProductRequest $request, Product $product, EditProductService $editProductService): JsonResponse
    {
        try {

            DB::beginTransaction();

            event(new EditModelEvent($editProductService, $product, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_UPDATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Update the specified resource's active status in storage.
     *
     * @param UpdateActiveStatusRequest $request
     * @param Product $product
     * @param EditProductActiveStatusService $editProductActiveStatusService
     * @return JsonResponse
     */
    public function updateActiveStatus(UpdateActiveStatusRequest $request, Product $product, EditProductActiveStatusService $editProductActiveStatusService)
    {
        try {

            DB::beginTransaction();

            event(new EditModelEvent($editProductActiveStatusService, $product, $request->validated()));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_UPDATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Set product new main image.
     *
     * @param Product $product
     * @param Image $image
     * @param EditProductMainImage $editProductMainImage
     * @return JsonResponse
     */
    public function updateMainImage(Product $product, Image $image, EditProductMainImage $editProductMainImage)
    {
        if ($image->imageable_id != $product->id) {

            return response()->json(Errors::MISMATCH_DATA, 400);
        }

        try {

            DB::beginTransaction();

            event(new EditModelEvent($editProductMainImage, $product, ['image' => $image]));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_UPDATED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @param DestroyProductService $destroyProductService
     * @param DestroyProductValidation $destroyProductValidation
     * @return JsonResponse
     */
    public function destroy(Product $product, DestroyProductService $destroyProductService, DestroyProductValidation $destroyProductValidation): JsonResponse
    {
        if ($destroyProductValidation->validate($product)) {

            return response()->json(Errors::MODEL_HAS_RELATIONSHIP_DATA, 400);
        }

        try {

            DB::beginTransaction();

            event(new DestroyModelEvent($destroyProductService, $product));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }


    /**
     * Destroy product image.
     *
     * @param Product $product
     * @param Image $image
     * @param DestroyProductImageValidation $destroyProductImageValidation
     * @param DestroyProductImageService $destroyProductImageService
     * @return JsonResponse
     */
    public function destroyImage(Product $product, Image $image, DestroyProductImageValidation $destroyProductImageValidation, DestroyProductImageService $destroyProductImageService) {

        if (! $destroyProductImageValidation->validate($product, ['image' => $image])) {

            return response()->json(Errors::MODEL_NOT_FOUND, 400);
        }

        try {

            DB::beginTransaction();

            event(new DestroyModelEvent($destroyProductImageService, $image));

        } catch (\Exception $exception) {

            DB::rollBack();

            return response()->json(Errors::MODEL_NOT_DELETED, 400);
        }

        DB::commit();

        return response()->json(['message' => 'Success'], 200);
    }
}
