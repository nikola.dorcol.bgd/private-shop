<?php

namespace App\Providers;

use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Base\Events\EditModelEvent;
use App\Src\Base\Listeners\DeleteModelListener;
use App\Src\Base\Listeners\StoreModelListener;
use App\Src\Base\Listeners\UpdateModelListener;
use App\Src\Users\Events\RegisterUserEvent;
use App\Src\Users\Events\UserRegisteredEvent;
use App\Src\Users\Listeners\RegisterUserListener;
use App\Src\Users\Listeners\WelcomeUserListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RegisterUserEvent::class => [
            RegisterUserListener::class
        ],

        UserRegisteredEvent::class => [
            WelcomeUserListener::class
        ],

        CreateModelEvent::class => [
            StoreModelListener::class
        ],

        EditModelEvent::class => [
            UpdateModelListener::class
        ],

        DestroyModelEvent::class => [
            DeleteModelListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
