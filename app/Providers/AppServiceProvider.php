<?php

namespace App\Providers;

use App\Src\Services\Auth\RegisterEmailUser;
use App\Src\Services\Auth\RegisterOauthUser;
use App\Src\Services\Contracts\CreateCartInterface;
use App\Src\Services\Contracts\CreateModelImageServiceInterface;
use App\Src\Services\Contracts\DeleteInterface;
use App\Src\Services\Contracts\DeleteManyInterface;
use App\Src\Services\Contracts\GetCartInterface;
use App\Src\Services\Contracts\InsertInterface;
use App\Src\Services\Contracts\RegisterUserInterface;
use App\Src\Services\Contracts\RemoveImageServiceInterface;
use App\Src\Services\Contracts\StoreInterface;
use App\Src\Services\Contracts\SyncInterface;
use App\Src\Services\Contracts\UpdateInterface;
use App\Src\Services\Contracts\UploadImageServiceInterface;
use App\Src\Services\Images\RemoveImageService;
use App\Src\Services\Images\UploadImageService;
use App\Src\Services\Models\Create\Cart\CreateCartDataBaseService;
use App\Src\Services\Models\Create\Cart\CreateCartSessionService;
use App\Src\Services\Models\CreateModelImageService;
use App\Src\Services\Models\DeleteManyModelService;
use App\Src\Services\Models\DeleteModelService;
use App\Src\Services\Models\Get\Cart\GetCartDataBaseService;
use App\Src\Services\Models\Get\Cart\GetCartSeessionService;
use App\Src\Services\Models\InsertModelService;
use App\Src\Services\Models\StoreModelService;
use App\Src\Services\Models\SyncModelService;
use App\Src\Services\Models\UpdateModelService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    public $bindings = [
        UploadImageServiceInterface::class      => UploadImageService::class,
        RemoveImageServiceInterface::class      => RemoveImageService::class,
        CreateModelImageServiceInterface::class => CreateModelImageService::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(StoreInterface::class, StoreModelService::class);
        $this->app->singleton(InsertInterface::class, InsertModelService::class);
        $this->app->singleton(UpdateInterface::class, UpdateModelService::class);
        $this->app->singleton(DeleteInterface::class, DeleteModelService::class);
        $this->app->singleton(DeleteManyInterface::class, DeleteManyModelService::class);
        $this->app->singleton(SyncInterface::class, SyncModelService::class);

        $this->app->bind(RegisterUserInterface::class, function ($app, $data) {

            return $app->make($data['type'] == 'oauth' ? RegisterOauthUser::class : RegisterEmailUser::class);
        });

        $this->app->bind(CreateCartInterface::class, function ($app, $data) {

            return $app->make(auth()->check() ? CreateCartDataBaseService::class : CreateCartSessionService::class);
        });

        $this->app->bind(GetCartInterface::class, function ($app, $data) {

            return $app->make(auth()->check() ? GetCartDataBaseService::class : GetCartSeessionService::class);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
