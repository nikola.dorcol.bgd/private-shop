<?php

namespace App\Providers;

use App\Src\Brands\Brand;
use App\Src\Brands\BrandObserver;
use App\Src\Carts\Cart;
use App\Src\Carts\CartObserver;
use App\Src\Categories\Category;
use App\Src\Categories\CategoryObserver;
use App\Src\Orders\Order;
use App\Src\Orders\OrderObserver;
use App\Src\Orders\ProductVariants\OrderProductVariant;
use App\Src\Orders\ProductVariants\OrderProductVariantObserver;
use App\Src\Permissions\Permission;
use App\Src\Permissions\PermissionObserver;
use App\Src\Attributes\Attribute;
use App\Src\Attributes\AttributeObserver;
use App\Src\Attributes\Values\AttributeValue;
use App\Src\Attributes\Values\AttributeValueObserver;
use App\Src\Products\AttributeValue\ProductAttributeValue;
use App\Src\Products\AttributeValue\ProductAttributeValueObserver;
use App\Src\Products\Product;
use App\Src\Products\ProductObserver;
use App\Src\Products\Recommended\RecommendedProduct;
use App\Src\Products\Recommended\RecommendedProductObserver;
use App\Src\Products\Variants\ProductVariant;
use App\Src\Products\Variants\ProductVariantObserver;
use App\Src\Roles\Role;
use App\Src\Roles\RoleObserver;
use App\Src\Users\ShippingInformation\ShippingInformation;
use App\Src\Users\ShippingInformation\ShippingInformationObserver;
use App\Src\Users\UserObserver;
use App\Src\Users\User;
use App\Src\Warehouses\Inventory\WarehouseInventory;
use App\Src\Warehouses\Inventory\WarehouseInventoryObserver;
use App\Src\Warehouses\Log\WarehouseLog;
use App\Src\Warehouses\Log\WarehouseLogObserver;
use App\Src\Warehouses\Warehouse;
use App\Src\Warehouses\WarehouseObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        ShippingInformation::observe(ShippingInformationObserver::class);

        Role::observe(RoleObserver::class);
        Permission::observe(PermissionObserver::class);

        Category::observe(CategoryObserver::class);

        Brand::observe(BrandObserver::class);

        Warehouse::observe(WarehouseObserver::class);
        WarehouseInventory::observe(WarehouseInventoryObserver::class);
        WarehouseLog::observe(WarehouseLogObserver::class);

        Attribute::observe(AttributeObserver::class);
        AttributeValue::observe(AttributeValueObserver::class);

        Product::observe(ProductObserver::class);
        ProductVariant::observe(ProductVariantObserver::class);
        ProductAttributeValue::observe(ProductAttributeValueObserver::class);
        RecommendedProduct::observe(RecommendedProductObserver::class);

        Cart::observe(CartObserver::class);

        Order::observe(OrderObserver::class);
        OrderProductVariant::observe(OrderProductVariantObserver::class);
    }
}
