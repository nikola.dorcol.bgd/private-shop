<?php

namespace App\Providers;

use App\Src\Categories\Contracts\CategoryRepositoryInterface;
use App\Src\Services\Contracts\GetCartInterface;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('client.*', function ($view) {

            $view->with('categories', app()->make(CategoryRepositoryInterface::class)->getMainCategories());
            $view->with('productsInCart', count(app()->make(GetCartInterface::class)->get()));
        });
    }
}
