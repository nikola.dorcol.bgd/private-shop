<?php

namespace App\Providers;

use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Base\Errors;
use App\Src\Brands\Contracts\BrandRepositoryInterface;
use App\Src\Carts\Contracts\CartRepositoryInterface;
use App\Src\Categories\Contracts\CategoryRepositoryInterface;
use App\Src\Images\Contracts\ImageRepositoryInterface;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use App\Src\Orders\Order;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use App\Src\Warehouses\Contracts\WarehouseRepositoryInterface;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::bind('provider', function ($value) {

            return in_array($value, config('services.availableOauthProviders'))
                ? $value
                : abort(response()->json(Errors::PROVIDER_BAD_REQUEST, 400));
        });

        Route::bind('category', function ($value) {

            return bindRouteParam((int) $value, CategoryRepositoryInterface::class);
        });

        Route::bind('brand', function ($value) {

            return bindRouteParam((int) $value, BrandRepositoryInterface::class);
        });

        Route::bind('warehouse', function ($value) {

            return bindRouteParam((int) $value, WarehouseRepositoryInterface::class);
        });

        Route::bind('product', function ($value) {

            return bindRouteParam((int) $value, ProductRepositoryInterface::class);
        });

        Route::bind('activeProduct', function ($value) {

            $model = bindRouteParam((int) $value, ProductRepositoryInterface::class);

            return $model->active ? $model : abort(404);
        });

        Route::bind('productVariant', function ($value) {

            return bindRouteParam((int) $value, ProductVariantRepositoryInterface::class);
        });

        Route::bind('attribute', function ($value) {

            return bindRouteParam((int) $value, AttributeRepositoryInterface::class);
        });

        Route::bind('image', function ($value) {

            return bindRouteParam((int) $value, ImageRepositoryInterface::class);
        });

        Route::bind('order', function ($value) {

            return bindRouteParam((int) $value, OrderRepositoryInterface::class);
        });

        Route::bind('cart', function ($value) {

            return bindRouteParam((int) $value, CartRepositoryInterface::class);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapImageCacheRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }


    /**
     * Define the "imagecache" routes for the application
     *
     * @return void
     */
    protected function mapImageCacheRoutes()
    {
        Route::middleware('api')
            ->group(base_path('routes/imagecache.php'));
    }
}
