<?php

namespace App\Providers;

use App\Src\Brands\Contracts\BrandRepositoryInterface;
use App\Src\Brands\Repositories\BrandEloquentRepository;
use App\Src\Carts\Contracts\CartRepositoryInterface;
use App\Src\Carts\Repositories\CartEloquentRepository;
use App\Src\Categories\Contracts\CategoryRepositoryInterface;
use App\Src\Categories\Repositories\CategoryEloquentRepository;
use App\Src\Images\Contracts\ImageRepositoryInterface;
use App\Src\Images\Repositories\ImageEloquentRepository;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use App\Src\Orders\ProductVariants\Contracts\OrderProductVariantRepositoryInterface;
use App\Src\Orders\ProductVariants\OrderProductVariantEloquentRepository;
use App\Src\Orders\Repositories\OrderEloquentRepository;
use App\Src\Permissions\Contracts\PermissionRepositoryInterface;
use App\Src\Permissions\Repositories\PermissionEloquentRepository;
use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Attributes\Repositories\AttributeEloquentRepository;
use App\Src\Attributes\Values\Contracts\AttributeValueRepositoryInterface;
use App\Src\Attributes\Values\Repositories\AttributeValueEloquentRepository;
use App\Src\Products\AttributeValue\Contracts\ProductAttributeValueRepositoryInterface;
use App\Src\Products\AttributeValue\Repositories\ProductAttributeValueEloquentRepository;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Products\Recommended\Contracts\RecommendedProductRepositoryInterface;
use App\Src\Products\Recommended\Repositories\RecommendedProductEloquentRepository;
use App\Src\Products\Repositories\ProductEloquentRepository;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use App\Src\Products\Variants\Repositories\ProductVariantEloquentRepository;
use App\Src\Roles\Contracts\RoleRepositoryInterface;
use App\Src\Roles\Repositories\RoleEloquentRepository;
use App\Src\Users\Contracts\UserRepositoryInterface;
use App\Src\Users\Oauth\Contracts\OauthRepositoryInterface;
use App\Src\Users\Oauth\Repositories\OauthEloquentRepository;
use App\Src\Users\Repositories\UserEloquentRepository;
use App\Src\Users\ShippingInformation\Contracts\UserShippingInformationRepositoryInterface;
use App\Src\Users\ShippingInformation\Repositories\UserShippingInformationEloquentRepository;
use App\Src\Warehouses\Contracts\WarehouseRepositoryInterface;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use App\Src\Warehouses\Inventory\Repositories\WarehouseInventoryEloquentRepository;
use App\Src\Warehouses\Repositories\WarehouseEloquentRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    public $bindings = [
        UserRepositoryInterface::class                      => UserEloquentRepository::class,
        UserShippingInformationRepositoryInterface::class   => UserShippingInformationEloquentRepository::class,
        OauthRepositoryInterface::class                     => OauthEloquentRepository::class,

        RoleRepositoryInterface::class                      => RoleEloquentRepository::class,
        PermissionRepositoryInterface::class                => PermissionEloquentRepository::class,

        CategoryRepositoryInterface::class                  => CategoryEloquentRepository::class,

        ImageRepositoryInterface::class                     => ImageEloquentRepository::class,

        BrandRepositoryInterface::class                     => BrandEloquentRepository::class,

        WarehouseRepositoryInterface::class                 => WarehouseEloquentRepository::class,
        WarehouseInventoryRepositoryInterface::class        => WarehouseInventoryEloquentRepository::class,

        AttributeRepositoryInterface::class                 => AttributeEloquentRepository::class,
        AttributeValueRepositoryInterface::class            => AttributeValueEloquentRepository::class,

        ProductRepositoryInterface::class                   => ProductEloquentRepository::class,
        ProductVariantRepositoryInterface::class            => ProductVariantEloquentRepository::class,
        ProductAttributeValueRepositoryInterface::class     => ProductAttributeValueEloquentRepository::class,
        RecommendedProductRepositoryInterface::class        => RecommendedProductEloquentRepository::class,

        CartRepositoryInterface::class                      => CartEloquentRepository::class,

        OrderRepositoryInterface::class                     => OrderEloquentRepository::class,
        OrderProductVariantRepositoryInterface::class       => OrderProductVariantEloquentRepository::class
    ];


    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
