<?php

namespace App\Providers;

use App\Src\Builders\Contracts\ImageBuilderInterface;
use App\Src\Builders\Directors\ImageDirector;
use App\Src\Builders\Images\ImageBuilder;
use Illuminate\Support\ServiceProvider;

class BuilderServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    public $bindings = [
        ImageBuilderInterface::class => ImageBuilder::class
    ];


    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ImageDirector::class, function ($app, $data) {

            $data['type']   = isset($data['type']) ? $data['type'] : '';
            $data['resize'] = isset($data['resize']) ? $data['resize'] : false;

            return new ImageDirector($app->make(ImageBuilderInterface::class), $data['file'], $data['path'], $data['type'], $data['resize']);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
