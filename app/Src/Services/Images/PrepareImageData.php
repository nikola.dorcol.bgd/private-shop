<?php

namespace App\Src\Services\Images;

use App\Src\Builders\Directors\ImageDirector;
use Illuminate\Http\UploadedFile;

abstract class PrepareImageData
{
    /**
     * Prepare data for App\Src\Services\Models\Create\CreateImageService class.
     *
     * @param int $id
     * @param string $className
     * @param string $type
     * @param string $folder
     * @param UploadedFile $file
     * @return array
     */
    public static function prepare(int $id, string $className, string $type, string $folder, UploadedFile $file): array
    {
        $imageDirector = app()->makeWith(ImageDirector::class, [
            'file' => $file,
            'path' => $folder,
        ]);

        return [
            'imageable_id'      => $id,
            'imageable_type'    => $className,
            'type'              => constant('App\Src\Images\Image::TYPE_' . strtoupper($type)),
            'image'             => $imageDirector->build()
        ];
    }
}