<?php

namespace App\Src\Services\Images;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\FileNotUploadedException;
use App\Src\Images\Image;
use App\Src\Services\Contracts\RemoveImageServiceInterface;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class RemoveImageService implements RemoveImageServiceInterface
{

    public function remove(Image $image): bool
    {
        if (! Storage::disk($image->storage)->exists($image->src)) {

            Log::info("Image (id: {$image->id}) did not exist on {$image->storage}, delete action failed!");

            return true;
        }

        if (! Storage::disk($image->storage)->delete($image->src)) {

            throw new FileNotUploadedException(Errors::FILE_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}