<?php

namespace App\Src\Services\Images;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\FileNotUploadedException;
use App\Src\Images\ImageObject;
use App\Src\Services\Contracts\UploadImageServiceInterface;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UploadImageService implements UploadImageServiceInterface
{

    public function upload(ImageObject $imageObject): array
    {
        $src  = 'images/' . ltrim(rtrim($imageObject->getPath(), '/'), '/') . '/' . date('Y') . '/' . date('M') . '/' . date('D') . '/' . Str::random(5) . time() . '.' . $imageObject->getExtension();

        $image = Image::make($imageObject->getFile())
            ->stream()
            ->__toString();

        if (! Storage::disk($imageObject->getStorage())->put($src, $image)) {

            throw new FileNotUploadedException(Errors::FILE_NOT_UPLOADED['error_message'], 400);
        }

        return ['src' => $src, 'storage' => $imageObject->getStorage()];
    }
}