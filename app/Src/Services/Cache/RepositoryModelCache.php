<?php

namespace App\Src\Services\Cache;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;

class RepositoryModelCache
{

    /**
     * Check if cache exists.
     *
     * @param array $tags
     * @param string $hash
     * @return bool
     */
    public function hasCache(array $tags, string $hash): bool
    {
        return Cache::store(config('cache.modelDefault'))
            ->tags($tags)
            ->has($hash);
    }


    /**
     * Clear cache for given tags.
     *
     * @param array $tags
     * @return bool
     */
    public function clearCache(array $tags): bool
    {
        return Cache::store(config('cache.modelDefault'))
            ->tags($tags)
            ->flush();
    }


    /**
     * Set/Get query data to/from cache.
     *
     * @param Builder $query
     * @param array $columns
     * @param array $tags
     * @return Collection
     */
    public function get(Builder $query, array $columns, array $tags): Collection
    {
        $hash = md5($query->toSql());

        if ($this->hasCache($tags, $hash)) {

            return Cache::store(config('cache.modelDefault'))
                ->tags($tags)
                ->get($hash);
        }

        return Cache::store(config('cache.modelDefault'))
            ->tags($tags)
            ->rememberForever(md5($query->toSql()), function () use($query, $columns) {
                return $query->get($columns);
            });
    }


    /**
     * @param Builder $query
     * @param int $perPage
     * @param array $columns
     * @param array $tags
     */
    public function paginate(Builder $query, int $perPage, array $columns, array $tags)
    {
        $hash = md5($query->toSql());

        if ($this->hasCache($tags, $hash)) {

            return Cache::store(config('cache.modelDefault'))
                ->tags($tags)
                ->get($hash);
        }

        return Cache::store(config('cache.modelDefault'))
            ->tags($tags)
            ->rememberForever($hash, function () use($query, $perPage, $columns) {
                return $query->paginate($perPage, $columns);
            });
    }
}