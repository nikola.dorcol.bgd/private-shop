<?php

namespace App\Src\Services\Contracts;

interface EditManyServiceInterface
{
    /**
     * Edit many rows.
     *
     * @param array $data
     * @return bool
     */
    public function edit(array $data): bool;
}