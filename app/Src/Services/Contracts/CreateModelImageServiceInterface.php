<?php

namespace App\Src\Services\Contracts;

use App\Src\Images\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

interface CreateModelImageServiceInterface
{
    /**
     * Create image for given model.
     *
     * @param Model $model
     * @param string $type
     * @param string $folder
     * @param UploadedFile $file
     * @return Image
     */
    public function create(Model $model, string $type, string $folder, UploadedFile $file): Image;
}