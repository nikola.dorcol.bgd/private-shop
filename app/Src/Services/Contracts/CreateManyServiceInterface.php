<?php

namespace App\Src\Services\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface CreateManyServiceInterface
{
    /**
     * Create many rows in database.
     *
     * @param array $data
     * @return Collection
     */
    public function create(array $data): Collection;
}