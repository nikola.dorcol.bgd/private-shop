<?php

namespace App\Src\Services\Contracts;

use App\Src\Images\ImageObject;

interface UploadImageServiceInterface
{
    /**
     * Upload image to specific storage and path with resizing and quality change.
     *
     * @param ImageObject $imageObject
     * @return array
     */
    public function upload(ImageObject $imageObject): array;
}