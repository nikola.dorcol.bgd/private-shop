<?php

namespace App\Src\Services\Contracts;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

interface StoreInterface
{
    /**
     * Store model record.
     *
     * @param AbstractRepositoryInterface $repository
     * @param array $data
     * @return Model|null
     */
    public function store(AbstractRepositoryInterface $repository, array $data): ?Model;
}