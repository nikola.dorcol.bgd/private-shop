<?php

namespace App\Src\Services\Contracts;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

interface UpdateInterface
{
    /**
     * Update given model.
     *
     * @param AbstractRepositoryInterface $repository
     * @param Model $model
     * @param array $data
     * @return bool
     */
    public function update(AbstractRepositoryInterface $repository, Model $model, array $data): bool;
}