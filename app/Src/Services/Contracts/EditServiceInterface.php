<?php

namespace App\Src\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface EditServiceInterface
{
    /**
     * Edit model.
     *
     * @param Model $model
     * @param array $data
     * @return Model
     */
    public function edit(Model $model, array $data): Model;
}