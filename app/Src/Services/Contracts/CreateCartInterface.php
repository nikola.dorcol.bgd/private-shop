<?php

namespace App\Src\Services\Contracts;


use App\Src\Products\Variants\ProductVariant;

/**
 * Interface for creating rows in carts table or session.
 *
 * Interface CreateCartInterface
 * @package App\Src\Services\Contracts
 */
interface CreateCartInterface {

    /**
     * @param ProductVariant $productVariant
     * @param array $data
     * @return bool
     */
    public function create(ProductVariant $productVariant, array $data): bool;
}