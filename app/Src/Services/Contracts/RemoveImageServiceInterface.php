<?php

namespace App\Src\Services\Contracts;

use App\Src\Images\Image;

interface RemoveImageServiceInterface
{
    /**
     * Remove image from storage.
     *
     * @param Image $image
     * @return bool
     */
    public function remove(Image $image): bool;
}