<?php

namespace App\Src\Services\Contracts;

interface GetCartInterface
{
    /**
     * @return array
     */
    public function get(): array;
}