<?php

namespace App\Src\Services\Contracts;

interface DestroyManyServiceInterface
{
    /**
     * Destroy given model with additional actions with given data if need.
     *
     * @param array $data
     * @return bool
     */
    public function destroyMany(array $data): bool;
}