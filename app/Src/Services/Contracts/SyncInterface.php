<?php

namespace App\Src\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface SyncInterface
{
    /**
     * Wrapper around sync eloquent method.
     *
     * @param Model $model
     * @param string $relation
     * @param array $ids
     * @return array
     */
    public function sync(Model $model, string $relation, array $ids): array;
}