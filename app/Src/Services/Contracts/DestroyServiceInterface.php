<?php

namespace App\Src\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface DestroyServiceInterface
{
    /**
     * Destroy given model with additional actions with given data if need.
     *
     * @param Model $model
     * @param array|null|null $data
     * @return bool
     */
    public function destroy(Model $model, ?array $data = null): bool;
}