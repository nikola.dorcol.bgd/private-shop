<?php

namespace App\Src\Services\Contracts;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

interface DeleteInterface
{
    /**
     * Delete Row from database.
     *
     * @param AbstractRepositoryInterface $repository
     * @param Model $model
     * @return bool
     */
    public function delete(AbstractRepositoryInterface $repository, Model $model): bool;
}