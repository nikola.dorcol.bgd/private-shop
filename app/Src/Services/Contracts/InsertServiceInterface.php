<?php

namespace App\Src\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface InsertServiceInterface
{
    /**
     * Insert multiple rows in db for their parent.
     *
     * @param array $data
     * @param Model|null $relationshipModel
     * @return bool
     */
    public function insert(array $data, ?Model $relationshipModel = null): bool;
}