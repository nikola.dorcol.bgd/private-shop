<?php

namespace App\Src\Services\Contracts;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

interface DeleteManyInterface
{
    /**
     * Delete Row from database.
     *
     * @param AbstractRepositoryInterface $repository
     * @param array $ids
     * @return bool
     */
    public function deleteMany(AbstractRepositoryInterface $repository, array $ids): bool;
}