<?php

namespace App\Src\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface CreateServiceInterface
{
    /**
     * Create model.
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model;
}