<?php

namespace App\Src\Services\Contracts;

use App\Src\Base\Contracts\AbstractRepositoryInterface;

interface InsertInterface
{
    /**
     * Prepare and insert data to db.
     *
     * @param AbstractRepositoryInterface $repository
     * @param array $data
     * @return bool
     */
    public function insert(AbstractRepositoryInterface $repository, array $data): bool;
}