<?php

namespace App\Src\Services\Contracts;

use App\Src\Users\User;

interface RegisterUserInterface
{
    /**
     * Register user.
     *
     * @param array $data
     * @return User
     */
    public function register(array $data): User;

    /**
     * Fire event after user is registered if needed.
     *
     * @param User $user
     * @return void
     */
    public function fireRegisterEvent(User $user): void;
}