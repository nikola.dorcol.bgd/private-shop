<?php

namespace App\Src\Services\Auth;

use App\Src\Roles\Contracts\RoleRepositoryInterface;
use App\Src\Services\Contracts\RegisterUserInterface;
use App\Src\Services\Contracts\StoreInterface;
use App\Src\Users\Contracts\UserRepositoryInterface;
use App\Src\Users\Events\UserRegisteredEvent;
use App\Src\Users\User;

abstract class AbstractRegisterUser implements RegisterUserInterface
{
    /**
     * @var StoreInterface
     */
    protected $storeService;

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var RoleRepositoryInterface
     */
    protected $roleRepository;


    /**
     * RegisterOauthUser constructor.
     */
    public function __construct()
    {
        $this->storeService     = app()->make(StoreInterface::class);
        $this->userRepository   = app()->make(UserRepositoryInterface::class);
        $this->roleRepository   = app()->make(RoleRepositoryInterface::class);
    }


    public function fireRegisterEvent(User $user): void
    {
        event(new UserRegisteredEvent($user));
    }
}