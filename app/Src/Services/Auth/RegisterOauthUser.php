<?php

namespace App\Src\Services\Auth;

use App\Src\Users\Oauth\Contracts\OauthRepositoryInterface;
use App\Src\Users\User;
use Carbon\Carbon;

class RegisterOauthUser extends AbstractRegisterUser
{
    /**
     * @var OauthRepositoryInterface
     */
    protected $oauthRepository;


    /**
     * RegisterOauthUser constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->oauthRepository  = app()->make(OauthRepositoryInterface::class);
    }


    public function register(array $data): User
    {
        $user = $this->userRepository->getByProviderId($data['provider_user_id'], $data['provider']);

        if (! $user) {

            $data['role_id']            = $this->roleRepository->findByRoleName('client')->id;
            $data['email_verified_at']  = Carbon::now()->format('Y-m-d H:i:s');
            $user                       = $this->storeService->store($this->userRepository, $data);
            $data['user_id']            = $user->id;

            $this->storeService->store($this->oauthRepository, $data);

        // Involve edger loading with role.permissions in auth user.
            $user = $user->fresh();

            $this->fireRegisterEvent($user);
        }

        auth()->guard('web')->login($user);

        return $user;
    }
}