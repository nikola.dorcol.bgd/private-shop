<?php

namespace App\Src\Services\Auth;

use App\Src\Mails\WelcomeMail;
use App\Src\Users\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class RegisterEmailUser extends AbstractRegisterUser
{

    public function register(array $data): User
    {
        $data['role_id']    = $this->roleRepository->findByRoleName('client')->id;
        $user               = $this->storeService->store($this->userRepository, $data);

    // Involve edger loading with role.permissions in auth user.
        $user = $user->fresh();

//        auth()->guard('web')->login($user);

//        $this->fireRegisterEvent($user);

        $token = URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            ['id' => $user->getKey()]);

        Mail::to($user->email)->queue(new WelcomeMail($user->name, $token));

        return $user;
    }
}