<?php

namespace App\Src\Services\Models\Create;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Brands\Contracts\BrandRepositoryInterface;
use App\Src\Services\Contracts\CreateModelImageServiceInterface;
use Illuminate\Database\Eloquent\Model;

class CreateBrandService extends AbstractCreateService
{
    /**
     * @var BrandRepositoryInterface
     */
    protected $brandRepository;

    /**
     * @var CreateModelImageServiceInterface
     */
    protected $createModelImageService;


    /**
     * CreateBrandService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->brandRepository          = app()->make(BrandRepositoryInterface::class);
        $this->createModelImageService  = app()->make(CreateModelImageServiceInterface::class);
    }


    public function create(array $data): Model
    {
        if (! $brand = $this->storeService->store($this->brandRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        if (! $this->createModelImageService->create($brand, 'logo', 'brands', $data['logo'])) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $brand;
    }
}