<?php

namespace App\Src\Services\Models\Create;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Images\Contracts\ImageRepositoryInterface;
use App\Src\Services\Contracts\UploadImageServiceInterface;
use Illuminate\Database\Eloquent\Model;

class CreateImageService extends AbstractCreateService
{
    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var UploadImageServiceInterface
     */
    protected $uploadImageService;


    /**
     * CreateImageService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->imageRepository      = app()->make(ImageRepositoryInterface::class);
        $this->uploadImageService   = app()->make(UploadImageServiceInterface::class);
    }


    public function create(array $data): Model
    {
        $uploadData = $this->uploadImageService->upload($data['image']);

        unset($data['image']);

        if (! $image = $this->storeService->store($this->imageRepository, array_merge($data, $uploadData))) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $image;
    }
}