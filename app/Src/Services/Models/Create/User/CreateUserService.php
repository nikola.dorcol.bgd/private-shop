<?php

namespace App\Src\Services\Models\Create\User;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Users\Contracts\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CreateUserService extends AbstractCreateService
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;


    /**
     * CreateUserService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->userRepository = app()->make(UserRepositoryInterface::class);
    }


    public function create(array $data): Model
    {
        if (! $user = $this->storeService->store($this->userRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $user;
    }
}