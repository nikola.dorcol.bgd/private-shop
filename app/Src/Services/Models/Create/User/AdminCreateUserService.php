<?php

namespace App\Src\Services\Models\Create\User;

use App\Src\Users\Notifications\WelcomeNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class AdminCreateUserService extends CreateUserService
{
    /**
     * When admin creates user, assign random password.
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        $data['password'] = bcrypt(Str::random(10));

        $user = parent::create($data);

        $user->notify(new WelcomeNotification());

        return $user;
    }
}