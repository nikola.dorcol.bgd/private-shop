<?php

namespace App\Src\Services\Models\Create\User;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Mails\WelcomeMail;
use App\Src\Roles\Contracts\RoleRepositoryInterface;
use App\Src\Users\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class AdminCreateCustomerService extends AbstractCreateService
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var RoleRepositoryInterface
     */
    protected $roleRepository;

    /**
     * @var AdminCreateUserService
     */
    protected $adminCreateUserService;

    /**
     * @var CreateUserShippingInformationService
     */
    protected $createShippingInfoService;


    /**
     * AdminCreateCustomerService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->userRepository               = app()->make(UserRepositoryInterface::class);
        $this->roleRepository               = app()->make(RoleRepositoryInterface::class);
        $this->adminCreateUserService       = app()->make(AdminCreateUserService::class);
        $this->createShippingInfoService    = app()->make(CreateUserShippingInformationService::class);
    }


    /**
     * Used to create customer when storing order without existing user.
     *
     * @param array $billingInfo
     * @return Model
     * @throws ModelNotCreatedException
     */
    public function create(array $billingInfo): Model
    {
        $userData = [
            'name'              => $billingInfo['billing_name'],
            'email'             => $billingInfo['billing_email'],
            'phone_number'      => $billingInfo['billing_phone_number'],
            'role_id'           => $this->roleRepository->findByRoleName('client')->id,
            'email_verified_at' => Carbon::now()->format('Y-m-d H:i:s')
        ];

        if (! $user = $this->adminCreateUserService->create($userData)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

//        $billingInfo['user_id'] = $user->id;
//
//        if (! $this->createShippingInfoService->create($this->removeBillingPrefix($billingInfo))) {
//
//            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
//        }

        Mail::to($userData['email'])->queue(new WelcomeMail($userData['email'], null));

        return $user;
    }


    /**
     * Because all fields that came to this class has "billing_" prefix.
     *
     * @param array $billingInfo
     * @return array
     */
    private function removeBillingPrefix(array $billingInfo): array
    {
        $array = [];

        foreach ($billingInfo as $key => $value) {

            $array[str_replace('billing_', '', $key)] = $value;
        }

        return $array;
    }
}