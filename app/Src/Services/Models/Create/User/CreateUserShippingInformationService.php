<?php

namespace App\Src\Services\Models\Create\User;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Users\ShippingInformation\Contracts\UserShippingInformationRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CreateUserShippingInformationService extends AbstractCreateService
{
    /**
     * @var UserShippingInformationRepositoryInterface
     */
    protected $userShippingInformationRepository;


    /**
     * CreateUserShippingInformationService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->userShippingInformationRepository = app()->make(UserShippingInformationRepositoryInterface::class);
    }


    public function create(array $data): Model
    {
        if (! $info = $this->storeService->store($this->userShippingInformationRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $info;
    }
}