<?php

namespace App\Src\Services\Models\Create\Order;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Base\Exceptions\ModelsNotDeletedException;
use App\Src\Mails\NewOrderForAdminMail;
use App\Src\Mails\OrderMail;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use App\Src\Orders\Order;
use App\Src\Services\Models\Destroy\Cart\DestroyManyCartsService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class ClientCreateOrderService extends AbstractCreateService
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var CreateOrderProductVariantService
     */
    protected $createOrderProductVariantService;

    /**
     * @var
     */
    protected $destroyManyCartsService;


    /**
     * ClientCreateOrderService constructor.
     */
    public function __construct()
    {
        $this->orderRepository                  = app()->make(OrderRepositoryInterface::class);
        $this->createOrderProductVariantService = app()->make(CreateOrderProductVariantService::class);
        $this->destroyManyCartsService          = app()->make(DestroyManyCartsService::class);

        parent::__construct();
    }


    public function create(array $data): Model
    {
        $data['billing_info']['user_id']    = auth()->user()->id;
        $data['billing_info']['admin_id']   = null;
        $data['billing_info']['status']     = Order::STATUS_PENDING;
        $data['billing_info']['final']      = 0;
        $data['billing_info']['total']      = 0;


        if (! $order = $this->storeService->store($this->orderRepository, $data['billing_info'])) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message']);
        }


        foreach ($data['products'] as $product) {

            $product['order_id']    = $order->id;
            $product['status']      = $order->status;

            if (!$this->createOrderProductVariantService->create($product)) {

                throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
            }
        }


        if (! $this->destroyManyCartsService->destroyMany(auth()->user()->carts->pluck('id')->toArray())) {

            throw new ModelsNotDeletedException(Errors::MODELS_NOT_DELETED['error_message']);
        }


        Mail::to(auth()->user()->email)->queue(new OrderMail($order->id));
        Mail::to('harcstefan@gmail.com')->queue(new NewOrderForAdminMail($order->load('productVariants.product', 'productVariants.productAttributeValues.attribute', 'productVariants.productAttributeValues.attributeValue')));

        return $order;
    }
}
