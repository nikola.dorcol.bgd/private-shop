<?php

namespace App\Src\Services\Models\Create\Order;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Mails\OrderAcceptedMail;
use App\Src\Mails\OrderDeclinedMail;
use App\Src\Mails\OrderMail;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use App\Src\Orders\Events\OrderCreatedEvent;
use App\Src\Orders\Order;
use App\Src\Roles\Contracts\RoleRepositoryInterface;
use App\Src\Services\Models\Create\User\AdminCreateCustomerService;
use App\Src\Services\Models\Create\User\AdminCreateUserService;
use App\Src\Services\Models\Create\User\CreateUserShippingInformationService;
use App\Src\Services\Models\Helpers\PrepareBillingInfoData;
use App\Src\Users\Contracts\UserRepositoryInterface;
use App\Src\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class AdminCreateOrderService extends AbstractCreateService
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var CreateOrderProductVariantService
     */
    protected $createOrderProductVariantService;

    /**
     * @var AdminCreateCustomerService
     */
    protected $adminCreateCustomerService;

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;


    /**
     * AdminCreateOrder constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->orderRepository                  = app()->make(OrderRepositoryInterface::class);
        $this->createOrderProductVariantService = app()->make(CreateOrderProductVariantService::class);
        $this->adminCreateCustomerService       = app()->make(AdminCreateCustomerService::class);
        $this->userRepository                   = app()->make(UserRepositoryInterface::class);
    }


    /**
     * Used when admin manually creating orders.
     *
     * @param array $data
     * @return Model
     * @throws ModelNotCreatedException
     */
    public function create(array $data): Model
    {
        $billingInfoData = PrepareBillingInfoData::prepare($data);


        if (! isset($billingInfoData['user_id']) || is_null($billingInfoData['user_id'])) {

            $billingInfoData['user_id'] = $this->adminCreateCustomerService->create($billingInfoData)->id;
        }


        if (! $order = $this->storeService->store($this->orderRepository, $billingInfoData)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message']);
        }


        foreach ($data['products'] as $product) {

            $product['order_id']    = $order->id;
            $product['status']      = $order->status;

            if (!$this->createOrderProductVariantService->create($product)) {

                throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
            }
        }


        // TODO put in event
        if ($billingInfoData['final'] == 1) {

            if ($billingInfoData['status'] == Order::STATUS_ACCEPTED) {

                Mail::to($order->user->email)->queue(new OrderAcceptedMail($order));
            }

            if ($billingInfoData['status'] == Order::STATUS_DECLINED) {

                Mail::to($order->user->email)->queue(new OrderDeclinedMail($order));
            }

        } else {

            Mail::to($this->userRepository->find($billingInfoData['user_id'])->email)->queue(new OrderMail($order->id, true));
        }

        return $order;
    }
}