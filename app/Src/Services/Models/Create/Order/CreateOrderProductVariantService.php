<?php

namespace App\Src\Services\Models\Create\Order;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Orders\Order;
use App\Src\Orders\ProductVariants\Contracts\OrderProductVariantRepositoryInterface;
use App\Src\Services\Models\Edit\Warehouse\EditWarehouseInventoryService;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CreateOrderProductVariantService extends AbstractCreateService
{
    /**
     * @var OrderProductVariantRepositoryInterface
     */
    protected $orderProductVariantRepository;

    /**
     * @var WarehouseInventoryRepositoryInterface
     */
    protected $warehouseInventoryRepository;

    /**
     * @var EditWarehouseInventoryService
     */
    protected $editWarehouseInventoryService;


    /**
     * CreateOrderProductVariantService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->orderProductVariantRepository    = app()->make(OrderProductVariantRepositoryInterface::class);
        $this->warehouseInventoryRepository     = app()->make(WarehouseInventoryRepositoryInterface::class);
        $this->editWarehouseInventoryService    = app()->make(EditWarehouseInventoryService::class);
    }


    /**
     * Create order product variant and update warehouse if order is Accepted.
     *
     * @param array $data
     * @return Model
     * @throws ModelNotUpdatedException
     */
    public function create(array $data): Model
    {
        if (! $orderProductVariant = $this->storeService->store($this->orderProductVariantRepository, $data)) {

            throw new ModelNotFoundException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        if ($data['status'] == Order::STATUS_ACCEPTED) {

            if (! $warehouseInventory = $this->warehouseInventoryRepository->getProductVariantWarehouseInventory($data['product_variant_id'], $data['warehouse_id'])) {

                throw new ModelNotFoundException(Errors::MODEL_NOT_FOUND['error_message'], 400);
            }


        // Update inventory value by product variant original price because original passed $data['price'] can be different
        // because of a variety of reasons (discounts, custom prise,...).
            $data['price'] = $orderProductVariant->productVariant->price;


            if (! $this->editWarehouseInventoryService->edit($warehouseInventory, $data)) {

                throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
            }
        }

        return $orderProductVariant;
    }
}