<?php

namespace App\Src\Services\Models\Create\Cart;


use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;

class CreateCartFromSessionService
{
    /**
     * @var CreateCartDataBaseService
     */
    protected $createCartDataBaseService;

    /**
     * @var ProductVariantRepositoryInterface
     */
    protected $productVariantRepository;


    /**
     * CreateCartFromSessionService constructor.
     */
    public function __construct()
    {
        $this->createCartDataBaseService    = app()->make(CreateCartDataBaseService::class);
        $this->productVariantRepository     = app()->make(ProductVariantRepositoryInterface::class);
    }


    /**
     * If there is session for cart store them in DB.
     *
     * @return bool
     */
    public function create(): bool
    {
        if (session()->exists('carts')) {

            $carts = session()->get('carts');

            foreach ($carts as $cart) {

                $productVariant = $this->productVariantRepository->find($cart['product_variant']);

                if ($productVariant || isset($cart['quantity'])) {

                    $this->createCartDataBaseService->create($productVariant, ['quantity' => $cart['quantity']]);
                }
            }

            session()->forget('carts');
        }

        return true;
    }
}