<?php

namespace App\Src\Services\Models\Create\Cart;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Carts\Contracts\CartRepositoryInterface;
use App\Src\Products\Variants\ProductVariant;
use App\Src\Services\Contracts\CreateCartInterface;
use App\Src\Services\Contracts\StoreInterface;
use App\Src\Services\Models\Edit\Cart\EditCartService;

class CreateCartDataBaseService implements CreateCartInterface
{
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @var StoreInterface
     */
    protected $storeService;

    /**
     * @var EditCartService
     */
    protected $editCartService;


    /**
     * CreateCartDataBaseService constructor.
     */
    public function __construct()
    {
        $this->cartRepository   = app()->make(CartRepositoryInterface::class);
        $this->storeService     = app()->make(StoreInterface::class);
        $this->editCartService  = app()->make(EditCartService::class);
    }


    /**
     * @param ProductVariant $productVariant
     * @param array $data
     * @return bool
     * @throws ModelNotCreatedException
     */
    public function create(ProductVariant $productVariant, array $data): bool
    {
        $data['product_variant_id'] = $productVariant->id;
        $data['user_id']            = auth()->user()->id;

        if ($cart = $this->cartRepository->findUserProductVariant(auth()->user(), $productVariant)) {

            $this->editCartService->edit($cart, ['quantity' => $data['quantity'] + $cart->quantity]);

            return true;
        }

        if (! $cart = $this->storeService->store($this->cartRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message']);
        }

        return true;
    }
}