<?php

namespace App\Src\Services\Models\Create\Cart;

use App\Src\Products\Variants\ProductVariant;
use App\Src\Services\Contracts\CreateCartInterface;

class CreateCartSessionService implements CreateCartInterface
{
    /**
     * Create cart in session.
     *
     * @param ProductVariant $productVariant
     * @param array $data
     * @return bool
     */
    public function create(ProductVariant $productVariant, array $data): bool
    {
        if (! session()->exists('carts')) {

            session()->put('carts', []);
        }

        session()->push('carts', [

            'product_variant'   => $productVariant->id,
            'quantity'          => $data['quantity']
        ]);

        return true;
    }
}