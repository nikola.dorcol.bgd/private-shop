<?php

namespace App\Src\Services\Models\Create;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Categories\Contracts\CategoryRepositoryInterface;
use App\Src\Services\Contracts\CreateModelImageServiceInterface;
use Illuminate\Database\Eloquent\Model;

class CreateCategoryService extends AbstractCreateService
{
    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var CreateModelImageServiceInterface
     */
    protected $createModelImageService;


    /**
     * CreateCategoryService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->categoryRepository       = app()->make(CategoryRepositoryInterface::class);
        $this->createModelImageService  = app()->make(CreateModelImageServiceInterface::class);
    }


    public function create(array $data): Model
    {
        if (! $category = $this->storeService->store($this->categoryRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        if (isset($data['icon'])) {

            if (! $this->createModelImageService->create($category, 'icon', 'categories', $data['icon'])) {

                throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
            }
        }

        return $category;
    }
}