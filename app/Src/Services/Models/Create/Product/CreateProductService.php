<?php

namespace App\Src\Services\Models\Create\Product;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Base\Exceptions\ModelRelationNotSynced;
use App\Src\Base\Exceptions\ModelsNotInsertedException;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Products\Product;
use App\Src\Services\Contracts\CreateModelImageServiceInterface;
use App\Src\Services\Contracts\SyncInterface;
use App\Src\Services\Models\Helpers\Product\ProductServiceHelper;
use Illuminate\Database\Eloquent\Model;

class CreateProductService extends AbstractCreateService
{
    use ProductServiceHelper;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var SyncInterface
     */
    protected $syncService;


    /**
     * CreateProductService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->productRepository                = app()->make(ProductRepositoryInterface::class);
        $this->createManyProductVariantsService = app()->make(CreateManyProductVariantsService::class);
        $this->createModelImageService          = app()->make(CreateModelImageServiceInterface::class);
        $this->syncService                      = app()->make(SyncInterface::class);
    }


    /**
     * Required data: App\Src\Products\StoreProductRequest
     *
     * @param array $data
     * @return Model
     * @throws ModelNotCreatedException
     * @throws ModelRelationNotSynced
     * @throws ModelsNotInsertedException
     */
    public function create(array $data): Model
    {
        if (! $product = $this->storeService->store($this->productRepository, $data['base_info'])) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        if (empty($this->syncService->sync($product, 'categories', $data['categories']))) {

            throw new ModelRelationNotSynced(Errors::MODEL_RELATION_NOT_SYNCED['error_message'], 400);
        }

        if (isset($data['related_products'])) {

            if (empty($this->syncService->sync($product, 'relatedProducts', $data['related_products']))) {

                throw new ModelRelationNotSynced(Errors::MODEL_RELATION_NOT_SYNCED['error_message'], 400);
            }
        }

        $this->createManyProductVariantsHelper($product, $data['variants']);

        if (! empty($data['images'])) {

            $this->createProductImagesHelper($product, $data['images']);
        }

        return $product;
    }
}
