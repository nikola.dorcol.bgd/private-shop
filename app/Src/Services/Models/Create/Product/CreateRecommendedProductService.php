<?php

namespace App\Src\Services\Models\Create\Product;

use App\Src\Base\AbstractCreateService;
use App\Src\Products\Recommended\Contracts\RecommendedProductRepositoryInterface;
use App\Src\Services\Contracts\StoreInterface;
use Illuminate\Database\Eloquent\Model;

class CreateRecommendedProductService extends AbstractCreateService
{
    /**
     * @var RecommendedProductRepositoryInterface
     */
    protected $recommendedProductRepository;


    /**
     * CreateProductVariantService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->recommendedProductRepository = app()->make(RecommendedProductRepositoryInterface::class);
        $this->storeService                 = app()->make(StoreInterface::class);
    }


    /**
     * Required data: product_id, sku, price
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        $data['position'] = $this->recommendedProductRepository->all()->count() + 1;

        return $this->storeService->store($this->recommendedProductRepository, $data);
    }
}