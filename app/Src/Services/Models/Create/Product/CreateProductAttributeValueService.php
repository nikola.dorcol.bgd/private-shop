<?php

namespace App\Src\Services\Models\Create\Product;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Products\AttributeValue\Contracts\ProductAttributeValueRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CreateProductAttributeValueService extends AbstractCreateService
{
    /**
     * @var ProductAttributeValueRepositoryInterface
     */
    protected $productAttributeValueRepository;


    /**
     * CreateProductAttributeValueService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->productAttributeValueRepository = app()->make(ProductAttributeValueRepositoryInterface::class);
    }


    /**
     * Required data: product_variant_id, attribute_id, attribute_value_id
     *
     * @param array $data
     * @return Model
     * @throws ModelNotCreatedException
     */
    public function create(array $data): Model
    {
        if (! $productAttributeValue = $this->storeService->store($this->productAttributeValueRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $productAttributeValue;
    }
}