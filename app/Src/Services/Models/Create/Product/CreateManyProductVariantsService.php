<?php

namespace App\Src\Services\Models\Create\Product;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Attributes\Values\Contracts\AttributeValueRepositoryInterface;
use App\Src\Attributes\Values\AttributeValue;
use App\Src\Products\AttributeValue\ProductAttributeValue;
use App\Src\Products\Variants\ProductVariant;
use App\Src\Services\Contracts\CreateManyServiceInterface;
use App\Src\Services\Models\Create\Attribute\CreateAttributeService;
use App\Src\Services\Models\Create\Attribute\CreateAttributeValueService;
use App\Src\Services\Models\Create\Warehouse\CreateWarehouseInventoryService;
use App\Src\Warehouses\Inventory\WarehouseInventory;
use Illuminate\Database\Eloquent\Collection;

class CreateManyProductVariantsService implements CreateManyServiceInterface
{
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var AttributeValueRepositoryInterface
     */
    protected $attributeValueRepository;

    /**
     * @var CreateProductVariantService
     */
    protected $createProductVariantService;

    /**
     * @var CreateAttributeService
     */
    protected $createAttributeService;

    /**
     * @var CreateAttributeValueService
     */
    protected $createAttributeValueService;

    /**
     * @var CreateProductAttributeValueService
     */
    protected $createProductAttributeValueService;

    /**
     * @var CreateWarehouseInventoryService
     */
    protected $createWarehouseInventoryService;


    /**
     * CreateProductVariants constructor.
     */
    public function __construct()
    {
        $this->attributeRepository                  = app()->make(AttributeRepositoryInterface::class);
        $this->attributeValueRepository             = app()->make(AttributeValueRepositoryInterface::class);
        $this->createAttributeService               = app()->make(CreateAttributeService::class);
        $this->createAttributeValueService          = app()->make(CreateAttributeValueService::class);
        $this->createProductVariantService          = app()->make(CreateProductVariantService::class);
        $this->createProductAttributeValueService   = app()->make(CreateProductAttributeValueService::class);
        $this->createWarehouseInventoryService      = app()->make(CreateWarehouseInventoryService::class);
    }


    /**
     * Required data: App\Src\Products\StoreProductRequest - key: variants (required), product_id (required)
     *
     * @param array $data
     * @return Collection
     * @throws ModelNotCreatedException
     */
    public function create(array $data): Collection
    {
        $productId  = $data['product_id'];
        $variants   = new Collection();

        foreach ($data['variants'] as $variant) {

            $productVariant = $this->createProductVariantHelper($productId, $variant['sku'], $variant['price']);

            foreach ($variant['attributes-values'] as $key => $value) {

                $attributeName      = sanitizeString($key);
                $attributeValueName = sanitizeString($value);

                if (! $attribute = $this->attributeRepository->findByName($attributeName)) {

                    if (! $attribute = $this->createAttributeService->create(['name' => $attributeName])) {

                        throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
                    }

                    $attributeValue = $this->createAttributeValueHelper($attribute->id, $attributeValueName);

                } else {

                    if (! $attributeValue = $this->attributeValueRepository->checkIfAttributeHasValue($attribute, $attributeValueName)) {

                        $attributeValue = $this->createAttributeValueHelper($attribute->id, $attributeValueName);
                    }
                }

                $this->createProductAttributeValueHelper($productVariant->id, $attribute->id, $attributeValue->id);
            }

            foreach ($variant['warehouses'] as $warehouse) {

                $this->createWarehouseInventoryHelper($productVariant->id, $warehouse['id'], $warehouse['quantity'], $productVariant->price);
            }

            $variants->push($productVariant);
        }

        return $variants;
    }


    /**
     * Create row in product_variant db table helper method.
     *
     * @param int $productId
     * @param string $sku
     * @param string $price
     * @return ProductVariant
     * @throws ModelNotCreatedException
     */
    private function createProductVariantHelper(int $productId, string $sku, string $price): ProductVariant
    {
        $productVariantData = [
            'product_id'    => $productId,
            'sku'           => $sku,
            'price'         => $price
        ];

        if (! $productVariant = $this->createProductVariantService->create($productVariantData)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $productVariant;
    }


    /**
     * Create row in attribute_value db table helper method.
     *
     * @param int $attributeId
     * @param string $name
     * @return AttributeValue
     * @throws ModelNotCreatedException
     */
    private function createAttributeValueHelper(int $attributeId, string $name): AttributeValue
    {
        $attributeValueData = [
            'attribute_id'  => $attributeId,
            'value'         => $name
        ];

        if (! $attributeValue = $this->createAttributeValueService->create($attributeValueData)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $attributeValue;
    }


    /**
     * Create row in product_attribute_value db table helper method.
     *
     * @param int $productVariantId
     * @param int $attributeId
     * @param int $attributeValueId
     * @return ProductAttributeValue
     * @throws ModelNotCreatedException
     */
    private function createProductAttributeValueHelper(int $productVariantId, int $attributeId, int $attributeValueId): ProductAttributeValue
    {
        $productAttributeValueData = [
            'product_variant_id'    => $productVariantId,
            'attribute_id'          => $attributeId,
            'attribute_value_id'    => $attributeValueId
        ];

        if (! $productAttributeValue = $this->createProductAttributeValueService->create($productAttributeValueData)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $productAttributeValue;
    }


    /**
     * Create row in warehouse_inventory db table helper method.
     *
     * @param int $variantId
     * @param int $warehouseId
     * @param int $quantity
     * @param float $price
     * @return WarehouseInventory
     * @throws ModelNotCreatedException
     */
    private function createWarehouseInventoryHelper(int $variantId, int $warehouseId, int $quantity, float $price): WareHouseInventory
    {
        $warehouseInventoryData = [
            'product_variant_id'    => $variantId,
            'warehouse_id'          => $warehouseId,
            'quantity'              => $quantity,
            'price'                 => $price
        ];

        if (! $warehouseInventory = $this->createWarehouseInventoryService->create($warehouseInventoryData)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $warehouseInventory;
    }
}