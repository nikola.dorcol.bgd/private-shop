<?php

namespace App\Src\Services\Models\Create\Product;

use App\Src\Base\AbstractCreateService;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use App\Src\Services\Contracts\StoreInterface;
use Illuminate\Database\Eloquent\Model;

class CreateProductVariantService extends AbstractCreateService
{
    /**
     * @var ProductVariantRepositoryInterface
     */
    protected $productVariantRepository;


    /**
     * CreateProductVariantService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->productVariantRepository = app()->make(ProductVariantRepositoryInterface::class);
        $this->storeService             = app()->make(StoreInterface::class);
    }


    /**
     * Required data: product_id, sku, price
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this->storeService->store($this->productVariantRepository, $data);
    }
}