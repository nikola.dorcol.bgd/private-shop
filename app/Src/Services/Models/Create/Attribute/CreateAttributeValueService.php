<?php

namespace App\Src\Services\Models\Create\Attribute;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Attributes\Values\Contracts\AttributeValueRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CreateAttributeValueService extends AbstractCreateService
{
    /**
     * @var AttributeValueRepositoryInterface
     */
    protected $attributeValueRepository;


    /**
     * CreateAttributeValue constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->attributeValueRepository = app()->make(AttributeValueRepositoryInterface::class);
    }


    /**
     * Required data: name
     *
     * @param array $data
     * @return Model
     * @throws ModelNotCreatedException
     */
    public function create(array $data): Model
    {
        if (! $productAttributeValue = $this->storeService->store($this->attributeValueRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $productAttributeValue;
    }
}