<?php

namespace App\Src\Services\Models\Create\Attribute;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Base\Exceptions\ModelsNotInsertedException;
use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Services\Models\Insert\Attribute\InsertAttributeValuesService;
use Illuminate\Database\Eloquent\Model;

class CreateAttributeService extends AbstractCreateService
{
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var InsertAttributeValuesService
     */
    protected $insertAttributeValuesService;


    /**
     * CreateAttributeService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->attributeRepository          = app()->make(AttributeRepositoryInterface::class);
        $this->insertAttributeValuesService = app()->make(InsertAttributeValuesService::class);
    }


    /**
     * Required data: name (required), values (optional)
     *
     * @param array $data
     * @return Model
     * @throws ModelNotCreatedException
     * @throws ModelsNotInsertedException
     */
    public function create(array $data): Model
    {
        if (! $attribute = $this->storeService->store($this->attributeRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        if (isset($data['values'])) {

            if (! $this->insertAttributeValuesService->insert($data['values'], $attribute)) {

                throw new ModelsNotInsertedException(Errors::MODELS_NOT_INSERTED['error_message'], 400);
            }
        }

        return $attribute;
    }
}