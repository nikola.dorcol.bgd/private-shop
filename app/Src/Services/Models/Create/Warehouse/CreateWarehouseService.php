<?php

namespace App\Src\Services\Models\Create\Warehouse;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Services\Contracts\StoreInterface;
use App\Src\Warehouses\Contracts\WarehouseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class CreateWarehouseService extends AbstractCreateService
{
    /**
     * @var WarehouseRepositoryInterface
     */
    protected $warehouseRepository;


    /**
     * CreateWarehouseService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->warehouseRepository  = app()->make(WarehouseRepositoryInterface::class);
        $this->storeService         = app()->make(StoreInterface::class);
    }


    public function create(array $data): Model
    {
        if (! $warehouse = $this->storeService->store($this->warehouseRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED, 400);
        }

        return $warehouse;
    }
}