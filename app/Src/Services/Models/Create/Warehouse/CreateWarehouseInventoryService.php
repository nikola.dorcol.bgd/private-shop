<?php

namespace App\Src\Services\Models\Create\Warehouse;

use App\Src\Base\AbstractCreateService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Services\Contracts\CreateServiceInterface;
use App\Src\Services\Contracts\StoreInterface;
use App\Src\Services\Models\Edit\Warehouse\EditWarehouseService;
use App\Src\Warehouses\Contracts\WarehouseRepositoryInterface;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use App\Src\Warehouses\Warehouse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CreateWarehouseInventoryService extends AbstractCreateService
{
    /**
     * @var WarehouseInventoryRepositoryInterface
     */
    protected $warehouseInventoryRepository;

    /**
     * @var WarehouseRepositoryInterface
     */
    protected $warehouseRepository;

    /**
     * @var EditWarehouseService
     */
    protected $editWarehouseService;


    /**
     * CreateWarehouseInventoryService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->warehouseInventoryRepository = app()->make(WarehouseInventoryRepositoryInterface::class);
        $this->warehouseRepository          = app()->make(WarehouseRepositoryInterface::class);
        $this->editWarehouseService         = app()->make(EditWarehouseService::class);
    }


    /**
     * Required data: warehouse_id, product_variant_id, quantity, price.
     *
     * @param array $data
     * @return Model
     * @throws ModelNotCreatedException
     */
    public function create(array $data): Model
    {
        $data['value'] = $data['quantity'] * $data['price'];

        if (! $warehouseInventory = $this->storeService->store($this->warehouseInventoryRepository, $data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        if (! $warehouse = $this->warehouseRepository->find($warehouseInventory->warehouse_id)) {

            throw new ModelNotFoundException(Errors::MODEL_NOT_FOUND['error_message'], 400);
        }

        $this->editWarehouseHelper($warehouse, $data['quantity'], $data['value']);

        return $warehouseInventory;
    }


    /**
     * Edit row in warehouses db table helper method.
     *
     * @param Warehouse $warehouse
     * @param $quantity
     * @param $value
     * @return bool
     * @throws ModelNotUpdatedException
     */
    private function editWarehouseHelper(Warehouse $warehouse, $quantity, $value): bool
    {
        $warehouseData = [
            'quantity'  => $warehouse->quantity + $quantity,
            'value'     => $warehouse->value + $value
        ];

        if (! $this->editWarehouseService->edit($warehouse, $warehouseData)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        return true;
    }
}