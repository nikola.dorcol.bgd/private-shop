<?php

namespace App\Src\Services\Models;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use App\Src\Services\Contracts\UpdateInterface;
use App\Src\Services\Models\Helpers\ModelAttributeMapper;
use Illuminate\Database\Eloquent\Model;

class UpdateModelService implements UpdateInterface
{

    public function update(AbstractRepositoryInterface $repository, Model $model, array $data): bool
    {
        $mappedData = ModelAttributeMapper::map($model, $data, 'fillable');

        return $repository->update($model, $mappedData);
    }
}