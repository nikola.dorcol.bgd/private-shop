<?php

namespace App\Src\Services\Models\Helpers;

use App\Src\Orders\Order;
use App\Src\Users\User;

class PrepareBillingInfoData
{
    /**
     * Prepare billing info array for creating or editing order.
     *
     * @param array $data
     * @return array
     */
    public static function prepare(array $data): array
    {
        $billingInfoData                = $data['billing_info'];
        $billingInfoData['admin_id']    = auth()->user()->id;
        $billingInfoData['final']       = in_array($billingInfoData['status'], Order::FINAL_STATUSES) ? 1 : 0;
        $billingInfoData['total']       = array_sum(array_map(function ($item) {

            return $item['price'];

        }, $data['products']));

        return $billingInfoData;
    }
}