<?php

namespace App\Src\Services\Models\Helpers\Product;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Base\Exceptions\ModelsNotInsertedException;
use App\Src\Images\Image;
use App\Src\Products\Product;
use App\Src\Services\Contracts\CreateModelImageServiceInterface;
use App\Src\Services\Models\Create\Product\CreateManyProductVariantsService;

/**
 * Methods that are required for Create/Edit product services.
 *
 * Class ProductServiceHelper
 * @package App\Src\Services\Models\Helpers\Product
 */
trait ProductServiceHelper
{
    /**
     * @var CreateModelImageServiceInterface
     */
    protected $createModelImageService;

    /**
     * @var CreateManyProductVariantsService
     */
    protected $createManyProductVariantsService;


    /**
     * @param Product $product
     * @param array $images
     * @return void
     * @throws ModelNotCreatedException
     */
    private function createProductImagesHelper(Product $product, array $images): void
    {
        $mainImage = null;

        foreach ($images as $image) {

            $type = $image['main'] == 1 ? 'product_main' : 'product';

            if (! $image = $this->createModelImageService->create($product, $type, 'products', $image['file'])) {

                throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
            }

            if ($image['main'] == 1) {

                $mainImage = $image;
            }
        }

        if (isset($this->imageRepository) && $mainImage) {

            $this->imageRepository->changeUnnecessaryProductMainTypes($mainImage->id, $product->id);
        }
    }


    /**
     * @param Product $product
     * @param array $variants
     * @return void
     * @throws ModelsNotInsertedException
     */
    private function createManyProductVariantsHelper(Product $product, array $variants): void
    {
        $productVariantsData = [
            'variants'      => $variants,
            'product_id'    => $product->id
        ];

        if (empty($this->createManyProductVariantsService->create($productVariantsData))) {

            throw new ModelsNotInsertedException(Errors::MODELS_NOT_INSERTED['error_message'], 400);
        }
    }
}