<?php

namespace App\Src\Services\Models\Helpers;

use Illuminate\Database\Eloquent\Model;

abstract class ModelAttributeMapper
{
    /**
     * Map given data to specific model attribute data array.
     *
     * @param Model $model
     * @param array $data
     * @param string $attribute
     * @return array
     */
    public static function map(Model $model, array $data, string $attribute = 'fillable'): array
    {
        $mappedAttributes = [];

        foreach ($model->{'get' . ucfirst($attribute)}() as $attribute)
        {
            if (array_key_exists($attribute, $data)) {

                $mappedAttributes[$attribute] = $data[$attribute];
            }
        }

        return $mappedAttributes;
    }
}