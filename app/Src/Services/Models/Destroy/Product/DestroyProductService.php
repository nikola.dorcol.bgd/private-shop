<?php

namespace App\Src\Services\Models\Destroy\Product;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Base\Exceptions\ModelsNotDeletedException;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Services\Models\Destroy\Cart\AdminDestroyManyCarts;
use App\Src\Services\Models\Destroy\Image\DestroyImageService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DestroyProductService extends AbstractDestroyService
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var DestroyProductVariantService
     */
    protected $destroyProductVariantService;

    /**
     * @var DestroyImageService
     */
    protected $destroyImageService;

    /**
     * @var AdminDestroyManyCarts
     */
    protected $adminDestroyManyCarts;


    /**
     * DestroyProductService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->productRepository            = app()->make(ProductRepositoryInterface::class);
        $this->destroyProductVariantService = app()->make(DestroyProductVariantService::class);
        $this->destroyImageService          = app()->make(DestroyImageService::class);
        $this->adminDestroyManyCarts        = app()->make(AdminDestroyManyCarts::class);
    }


    public function destroy(Model $product, ?array $data = null): bool
    {
        foreach ($product->variants as $variant) {

            if (! $this->destroyProductVariantService->destroy($variant)) {

                throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
            }
        }

        foreach ($product->images as $image) {

            if (! $this->destroyImageService->destroy($image)) {

                throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
            }
        }

        $product->categories()->detach();

        $product->relatedProducts()->detach();

        DB::table('related_products')->where('related_product_id', $product->id)->delete();

        if (! $this->adminDestroyManyCarts->destroyMany($product)) {

            throw new ModelsNotDeletedException(Errors::MODELS_NOT_DELETED['error_message']);
        }

        if (! $this->deleteService->delete($this->productRepository, $product)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}