<?php

namespace App\Src\Services\Models\Destroy\Product;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Products\AttributeValue\Contracts\ProductAttributeValueRepositoryInterface;
use App\Src\Services\Contracts\DeleteInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyProductAttributeValueService extends AbstractDestroyService
{
    /**
     * @var ProductAttributeValueRepositoryInterface
     */
    protected $productAttributeValueRepository;


    /**
     * DestroyProductAttributeValueService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->productAttributeValueRepository = app()->make(ProductAttributeValueRepositoryInterface::class);
    }


    public function destroy(Model $productAttributeValue, ?array $data = null): bool
    {
        if (! $this->deleteService->delete($this->productAttributeValueRepository, $productAttributeValue)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}