<?php

namespace App\Src\Services\Models\Destroy\Product;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use App\Src\Services\Models\Destroy\DestroyCartService;
use App\Src\Services\Models\Destroy\Warehouse\DestroyWarehouseInventoryService;
use Illuminate\Database\Eloquent\Model;

class DestroyProductVariantService extends AbstractDestroyService
{
    /**
     * @var ProductVariantRepositoryInterface
     */
    protected $productVariantRepository;

    /**
     * @var DestroyProductAttributeValueService
     */
    protected $destroyProductAttributeValueService;

    /**
     * @var DestroyCartService
     */
    protected $destroyCartService;

    /**
     * @var DestroyWarehouseInventoryService
     */
    protected $destroyWarehouseInventoryService;


    /**
     * DestroyProductVariantService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->productVariantRepository             = app()->make(ProductVariantRepositoryInterface::class);
        $this->destroyProductAttributeValueService  = app()->make(DestroyProductAttributeValueService::class);
        $this->destroyCartService                   = app()->make(DestroyCartService::class);
        $this->destroyWarehouseInventoryService     = app()->make(DestroyWarehouseInventoryService::class);
    }


    public function destroy(Model $productVariant, ?array $data = null): bool
    {
        foreach ($productVariant->productAttributeValues as $productAttributeValue) {

            if (! $this->destroyProductAttributeValueService->destroy($productAttributeValue)) {

                throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
            }
        }

        foreach ($productVariant->carts as $cart) {

            if (! $this->destroyCartService->destroy($cart)) {

                throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
            }
        }

        foreach ($productVariant->warehouseInventories as $warehouseInventory) {

            if (! $this->destroyWarehouseInventoryService->destroy($warehouseInventory)) {

                throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
            }
        }

        if (! $this->deleteService->delete($this->productVariantRepository, $productVariant)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}