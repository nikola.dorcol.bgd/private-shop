<?php

namespace App\Src\Services\Models\Destroy\Product;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Base\Exceptions\ModelsNotUpdatedException;
use App\Src\Products\Recommended\Contracts\RecommendedProductRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyRecommendedProductService extends AbstractDestroyService
{
    /**
     * @var RecommendedProductRepositoryInterface
     */
    protected $recommendedProductRepository;


    /**
     * DestroyRecommendedProductService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->recommendedProductRepository = app()->make(RecommendedProductRepositoryInterface::class);
    }


    /**
     * Destroy given model with additional actions with given data if need.
     *
     * @param Model $recommendedProduct
     * @param array|null|null $data
     * @return bool
     * @throws ModelNotDeletedException
     * @throws ModelsNotUpdatedException
     */
    public function destroy(Model $recommendedProduct, ?array $data = null): bool
    {
        if (! $this->deleteService->delete($this->recommendedProductRepository, $recommendedProduct)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message']);
        }

        if (!$this->recommendedProductRepository->reorderAfterRemove($recommendedProduct)) {

            throw new ModelsNotUpdatedException(Errors::MODElS_NOT_UPDATED['errors_message']);
        }

        return true;
    }
}