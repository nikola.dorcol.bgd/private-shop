<?php

namespace App\Src\Services\Models\Destroy\Image;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\FileNotDeletedException;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Images\Contracts\ImageRepositoryInterface;
use App\Src\Services\Contracts\RemoveImageServiceInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyImageService extends AbstractDestroyService
{
    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var RemoveImageServiceInterface
     */
    protected $removeImageService;


    /**
     * DestroyImageService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->imageRepository      = app()->make(ImageRepositoryInterface::class);
        $this->removeImageService   = app()->make(RemoveImageServiceInterface::class);
    }


    public function destroy(Model $image, ?array $data = null): bool
    {
        if (! $this->deleteService->delete($this->imageRepository, $image)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        if (! $this->removeImageService->remove($image)) {

            throw new FileNotDeletedException(Errors::FILE_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}