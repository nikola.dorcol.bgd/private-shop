<?php

namespace App\Src\Services\Models\Destroy\Image;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Images\Image;
use App\Src\Services\Models\Edit\EditImageService;
use Illuminate\Database\Eloquent\Model;

class DestroyProductImageService extends AbstractDestroyService
{
    /**
     * @var DestroyImageService
     */
    protected $destroyImageService;

    /**
     * @var EditImageService
     */
    protected $editImageService;


    /**
     * DestroyProductImageService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->destroyImageService  = app()->make(DestroyImageService::class);
        $this->editImageService     = app()->make(EditImageService::class);
    }


    public function destroy(Model $image, ?array $data = null): bool
    {
        if ($image->type == Image::TYPE_PRODUCT_MAIN) {

            $nextMainImage = $image->imageable->images()->where('type', Image::TYPE_PRODUCT)->first();

            if ($nextMainImage) {

                if (! $this->editImageService->edit($nextMainImage, ['type' => Image::TYPE_PRODUCT_MAIN])) {

                    throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
                }
            }
        }

        if (! $this->destroyImageService->destroy($image)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}