<?php

namespace App\Src\Services\Models\Destroy;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Categories\Contracts\CategoryRepositoryInterface;
use App\Src\Services\Models\Destroy\Image\DestroyImageService;
use Illuminate\Database\Eloquent\Model;

class DestroyCategoryService extends AbstractDestroyService
{
    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var DestroyImageService
     */
    protected $destroyImageService;


    /**
     * DestroyCategoryService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->categoryRepository   = app()->make(CategoryRepositoryInterface::class);
        $this->destroyImageService  = app()->make(DestroyImageService::class);
    }


    public function destroy(Model $category, ?array $data = null): bool
    {
    //Check if category has uploaded image.
        if ($categoryIcon = $category->icon) {

            if (! $this->destroyImageService->destroy($categoryIcon)) {

                throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
            }
        }

        if (! $this->deleteService->delete($this->categoryRepository, $category)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}