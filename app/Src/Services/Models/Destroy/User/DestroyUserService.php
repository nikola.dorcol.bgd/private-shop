<?php

namespace App\Src\Services\Models\Destroy\User;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Users\Contracts\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyUserService extends AbstractDestroyService
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var DestroyUserShippingInformationService
     */
    protected $destroyUserShippingInformationService;


    /**
     * DestroyUserService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->userRepository                           = app()->make(UserRepositoryInterface::class);
        $this->destroyUserShippingInformationService    = app()->make(DestroyUserShippingInformationService::class);
    }


    public function destroy(Model $user, ?array $data = null): bool
    {
        if ($user->shippingInformation->count()) {

            foreach ($user->shippingInformation as $shippingInformation) {

                if (! $this->destroyUserShippingInformationService->destroy($shippingInformation)) {

                    throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
                }
            }
        }

        if (! $this->deleteService->delete($this->userRepository, $user)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}