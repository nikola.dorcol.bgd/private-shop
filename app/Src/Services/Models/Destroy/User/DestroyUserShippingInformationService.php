<?php

namespace App\Src\Services\Models\Destroy\User;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Users\ShippingInformation\Contracts\UserShippingInformationRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyUserShippingInformationService extends AbstractDestroyService
{
    /**
     * @var UserShippingInformationRepositoryInterface
     */
    protected $userShippingInformationRepository;


    /**
     * DestroyUserShippingInformationService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->userShippingInformationRepository = app()->make(UserShippingInformationRepositoryInterface::class);
    }


    public function destroy(Model $shippingInformation, ?array $data = null): bool
    {
        if (! $this->deleteService->delete($this->userShippingInformationRepository, $shippingInformation)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}