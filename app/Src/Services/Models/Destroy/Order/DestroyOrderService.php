<?php

namespace App\Src\Services\Models\Destroy\Order;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Base\Exceptions\ModelsNotDeletedException;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyOrderService extends AbstractDestroyService
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var DestroyManyOrderProductVariantsService
     */
    protected $destroyManyOrderProductVariantService;


    /**
     * DestroyOrderService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->orderRepository                      = app()->make(OrderRepositoryInterface::class);
        $this->destroyManyOrderProductVariantService= app()->make(DestroyManyOrderProductVariantsService::class);
    }


    public function destroy(Model $order, ?array $data = null): bool
    {
        if (! $this->destroyManyOrderProductVariantService->destroyMany($order->orderProductVariant->pluck('id')->toArray())) {

            throw new ModelsNotDeletedException(Errors::MODELS_NOT_DELETED['error_message'], 400);
        }

        if (! $this->deleteService->delete($this->orderRepository, $order)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message']);
        }

        return true;
    }
}