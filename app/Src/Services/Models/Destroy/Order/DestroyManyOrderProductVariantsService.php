<?php

namespace App\Src\Services\Models\Destroy\Order;

use App\Src\Base\AbstractDestroyManyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelsNotDeletedException;
use App\Src\Orders\ProductVariants\Contracts\OrderProductVariantRepositoryInterface;

class DestroyManyOrderProductVariantsService extends AbstractDestroyManyService
{
    /**
     * @var OrderProductVariantRepositoryInterface
     */
    protected $orderProductVariantRepository;


    /**
     * DestroyManyOrderProductVariants constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->orderProductVariantRepository = app()->make(OrderProductVariantRepositoryInterface::class);
    }


    public function destroyMany(array $data): bool
    {
        if (! $this->deleteManyService->deleteMany($this->orderProductVariantRepository, $data)) {

            throw new ModelsNotDeletedException(Errors::MODELS_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}