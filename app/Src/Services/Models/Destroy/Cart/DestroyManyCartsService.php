<?php

namespace App\Src\Services\Models\Destroy\Cart;

use App\Src\Base\AbstractDestroyManyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelsNotDeletedException;
use App\Src\Carts\Contracts\CartRepositoryInterface;

class DestroyManyCartsService extends AbstractDestroyManyService
{
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;


    /**
     * DestroyManyCartService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->cartRepository = app()->make(CartRepositoryInterface::class);
    }


    public function destroyMany(array $data): bool
    {
        if (! $this->deleteManyService->deleteMany($this->cartRepository, $data)) {

            throw new ModelsNotDeletedException(Errors::MODELS_NOT_DELETED['error_message']);
        }

        return true;
    }
}