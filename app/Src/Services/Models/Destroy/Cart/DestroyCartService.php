<?php

namespace App\Src\Services\Models\Destroy\Cart;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Carts\Contracts\CartRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyCartService extends AbstractDestroyService
{
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;


    /**
     * DestroyCartService constructor.
     */
    public function __construct()
    {
        $this->cartRepository = app()->make(CartRepositoryInterface::class);

        parent::__construct();
    }


    public function destroy(Model $cart, ?array $data = null): bool
    {
        if (! $this->deleteService->delete($this->cartRepository, $cart)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message']);
        }

        return true;
    }
}