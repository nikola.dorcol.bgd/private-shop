<?php

namespace App\Src\Services\Models\Destroy\Cart;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelsNotDeletedException;
use App\Src\Products\Product;
use App\Src\Users\Contracts\UserRepositoryInterface;

class AdminDestroyManyCarts
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var DestroyManyCartsService
     */
    protected $destroyManyCartsService;


    /**
     * DestroyManyCartService constructor.
     */
    public function __construct()
    {
        $this->userRepository           = app()->make(UserRepositoryInterface::class);
        $this->destroyManyCartsService  = app()->make(DestroyManyCartsService::class);
    }


    /**
     * When admin delete product or mark it as unavailable,
     * delete all carts that have that product and notify its owners.
     *
     * @param Product $product
     * @return bool
     * @throws ModelsNotDeletedException
     */
    public function destroyMany(Product $product): bool
    {
        $users = $this->userRepository->getUsersThatHasProductInCart($product);

        foreach ($users as $user) {

            if (! $this->destroyManyCartsService->destroyMany($user->carts->pluck('id')->toArray())) {

                throw new ModelsNotDeletedException(Errors::MODELS_NOT_DELETED['error_message']);
            }

            // TODO send email about removed item.
//            $user->notify();
        }

        return true;
    }
}