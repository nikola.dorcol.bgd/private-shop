<?php

namespace App\Src\Services\Models\Destroy\Warehouse;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Warehouses\Contracts\WarehouseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyWarehouseService extends AbstractDestroyService
{
    /**
     * @var WarehouseRepositoryInterface
     */
    protected $warehouseRepository;

    /**
     * @var DestroyWarehouseInventoryService
     */
    protected $destroyWarehouseInventoryService;


    /**
     * DestroyWarehouseService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->warehouseRepository              = app()->make(WarehouseRepositoryInterface::class);
        $this->destroyWarehouseInventoryService = app()->make(DestroyWarehouseInventoryService::class);
    }


    public function destroy(Model $warehouse, ?array $data = null): bool
    {
        foreach($warehouse->inventory as $inventory) {

            if (! $this->destroyWarehouseInventoryService->destroy($inventory, ['updateWarehouse' => false])) {

                throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
            }
        }

        if (! $this->deleteService->delete($this->warehouseRepository, $warehouse)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}