<?php

namespace App\Src\Services\Models\Destroy\Warehouse;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Services\Models\Edit\Warehouse\EditWarehouseService;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyWarehouseInventoryService extends AbstractDestroyService
{
    /**
     * @var WarehouseInventoryRepositoryInterface
     */
    protected $warehouseInventoryRepository;

    /**
     * @var EditWarehouseService
     */
    protected $editWarehouseService;


    /**
     * DestroyWarehouseInventoryService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->warehouseInventoryRepository = app()->make(WarehouseInventoryRepositoryInterface::class);
        $this->editWarehouseService         = app()->make(EditWarehouseService::class);
    }


    public function destroy(Model $warehouseInventory, ?array $data = null): bool
    {
        $data['updateWarehouse'] = isset($data['updateWarehouse']) ? $data['updateWarehouse'] : false;

        if ($data['updateWarehouse'] !== false) {

            $warehouse      = $warehouseInventory->warehouse;
            $warehouseData  = [
                'value'     => $warehouse->value - $warehouseInventory->value,
                'quantity'  => $warehouse->quantity - $warehouseInventory->quantity
            ];

            if (! $this->editWarehouseService->edit($warehouse, $warehouseData)) {

                throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
            }
        }


        if (! $this->deleteService->delete($this->warehouseInventoryRepository, $warehouseInventory)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}