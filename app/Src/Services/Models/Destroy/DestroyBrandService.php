<?php

namespace App\Src\Services\Models\Destroy;

use App\Src\Base\AbstractDestroyService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotDeletedException;
use App\Src\Brands\Contracts\BrandRepositoryInterface;
use App\Src\Services\Models\Destroy\Image\DestroyImageService;
use Illuminate\Database\Eloquent\Model;

class DestroyBrandService extends AbstractDestroyService
{
    /**
     * @var BrandRepositoryInterface
     */
    protected $brandRepository;

    /**
     * @var DestroyImageService
     */
    protected $destroyImageService;


    /**
     * DestroyBrandCategory constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->brandRepository      = app()->make(BrandRepositoryInterface::class);
        $this->destroyImageService  = app()->make(DestroyImageService::class);
    }


    public function destroy(Model $brand, ?array $data = null): bool
    {
        if ($categoryIcon = $brand->logo) {

            if (! $this->destroyImageService->destroy($categoryIcon)) {

                throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
            }
        }

        if (! $this->deleteService->delete($this->brandRepository, $brand)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }

        return true;
    }
}