<?php

namespace App\Src\Services\Models;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use App\Src\Services\Contracts\InsertInterface;
use App\Src\Services\Models\Helpers\ModelAttributeMapper;
use Carbon\Carbon;

class InsertModelService implements InsertInterface
{

    public function insert(AbstractRepositoryInterface $repository, array $data): bool
    {
        $insertData = [];

        foreach ($data as $modelData) {

            $mappedData = ModelAttributeMapper::map($repository->model, $modelData, 'fillable');

            $mappedData['created_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $mappedData['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');

            array_push($insertData, $mappedData);
        }

        return $repository->insert($insertData);
    }
}