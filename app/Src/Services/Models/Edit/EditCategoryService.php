<?php

namespace App\Src\Services\Models\Edit;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Categories\Category;
use App\Src\Categories\Contracts\CategoryRepositoryInterface;
use App\Src\Services\Cache\RepositoryModelCache;
use App\Src\Services\Images\PrepareImageData;
use App\Src\Services\Models\Create\CreateImageService;
use Illuminate\Database\Eloquent\Model;

class EditCategoryService extends AbstractEditService
{
    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var CreateImageService
     */
    protected $createImageService;

    /**
     * @var EditImageService
     */
    protected $editImageService;


    /**
     * EditCategoryService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->categoryRepository   = app()->make(CategoryRepositoryInterface::class);
        $this->createImageService   = app()->make(CreateImageService::class);
        $this->editImageService     = app()->make(EditImageService::class);
    }


    public function edit(Model $category, array $data): Model
    {
        $data['parent_id'] = isset($data['parent_id']) ? $data['parent_id'] : null;

        if (! $this->updateService->update($this->categoryRepository, $category, $data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        if (isset($data['icon'])) {

            $imageData      = PrepareImageData::prepare($category->id, Category::class, 'icon', 'categories', $data['icon']);
            $categoryIcon   = $category->icon;

            if (! $categoryIcon) {

                if (! $this->createImageService->create($imageData)) {

                    throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
                }

            } else {

                if (! $this->editImageService->edit($categoryIcon, $imageData)) {

                    throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
                }
            }
        }

        if (is_null($data['parent_id'])) {

            app()->make(RepositoryModelCache::class)->clearCache(['categories']);
        }

        return $category->fresh();
    }
}