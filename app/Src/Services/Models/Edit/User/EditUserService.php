<?php

namespace App\Src\Services\Models\Edit\User;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Users\Contracts\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class EditUserService extends AbstractEditService
{
    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;


    /**
     * EditUserService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->userRepository = app()->make(UserRepositoryInterface::class);
    }


    public function edit(Model $user, array $data): Model
    {
        if (! $this->updateService->update($this->userRepository, $user, $data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        return $user->fresh();
    }
}