<?php

namespace App\Src\Services\Models\Edit\Warehouse;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use App\Src\Warehouses\Warehouse;
use Illuminate\Database\Eloquent\Model;

class EditWarehouseInventoryService extends AbstractEditService
{
    /**
     * @var WarehouseInventoryRepositoryInterface
     */
    protected $warehouseInventoryRepository;

    /**
     * @var EditWarehouseService
     */
    protected $editWarehouseService;


    /**
     * EditWarehouseInventoryService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->warehouseInventoryRepository = app()->make(WarehouseInventoryRepositoryInterface::class);
        $this->editWarehouseService         = app()->make(EditWarehouseService::class);
    }


    /**
     * Required data: quantity, price.
     *
     * @param Model $warehouseInventory
     * @param array $data
     * @return Model
     * @throws ModelNotUpdatedException
     */
    public function edit(Model $warehouseInventory, array $data): Model
    {
        $data['value']              = $data['quantity'] * $data['price'];
        $data['warehouse_id']       = $warehouseInventory->warehouse_id;
        $data['product_variant_id'] = $warehouseInventory->product_variant_id;

        $difference['value']        = $data['value'] - $warehouseInventory->value;
        $difference['quantity']     = $data['quantity'] - $warehouseInventory->quantity;

        if (! $this->updateService->update($this->warehouseInventoryRepository, $warehouseInventory, $data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        $this->editWarehouseHelper($warehouseInventory->warehouse, $difference['value'], $difference['quantity']);

        return $warehouseInventory->fresh();
    }


    /**
     * Edit row in warehouses db table helper method.
     *
     * @param Warehouse $warehouse
     * @param float $value
     * @param int $quantity
     * @return bool
     * @throws ModelNotUpdatedException
     */
    private function editWarehouseHelper(Warehouse $warehouse, float $value, int $quantity): bool
    {
        $data['value']      = $warehouse->value + $value;
        $data['quantity']   = $warehouse->quantity + $quantity;

        if (! $this->editWarehouseService->edit($warehouse, $data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        return true;
    }
}