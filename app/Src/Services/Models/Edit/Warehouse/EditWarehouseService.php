<?php

namespace App\Src\Services\Models\Edit\Warehouse;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Warehouses\Contracts\WarehouseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class EditWarehouseService extends AbstractEditService
{
    /**
     * @var WarehouseRepositoryInterface
     */
    protected $warehouseRepository;


    /**
     * EditWarehouseService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->warehouseRepository = app()->make(WarehouseRepositoryInterface::class);
    }


    public function edit(Model $warehouse, array $data): Model
    {
        if (! $this->updateService->update($this->warehouseRepository, $warehouse, $data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        return $warehouse->fresh();
    }
}