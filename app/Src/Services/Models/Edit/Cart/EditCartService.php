<?php

namespace App\Src\Services\Models\Edit\Cart;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelsNotUpdatedException;
use App\Src\Carts\Contracts\CartRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class EditCartService extends AbstractEditService
{
    /**
     * @var CartRepositoryInterface
     */
    public $cartRepository;


    /**
     * EditCartService constructor.
     */
    public function __construct()
    {
        $this->cartRepository = app()->make(CartRepositoryInterface::class);

        parent::__construct();
    }


    public function edit(Model $cart, array $data): Model
    {
        if (! $this->updateService->update($this->cartRepository, $cart, $data)) {

            throw new ModelsNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message']);
        }

        return $cart->fresh();
    }
}