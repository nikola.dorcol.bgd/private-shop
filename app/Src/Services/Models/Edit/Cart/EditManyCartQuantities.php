<?php

namespace App\Src\Services\Models\Edit\Cart;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Carts\Contracts\CartRepositoryInterface;
use App\Src\Services\Contracts\EditManyServiceInterface;

class EditManyCartQuantities implements EditManyServiceInterface
{
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;


    /**
     * EditManyCartQuantities constructor.
     */
    public function __construct()
    {
        $this->cartRepository = app()->make(CartRepositoryInterface::class);
    }


    public function edit(array $data): bool
    {
        foreach ($data['products'] as $cartItem) {

            if (! $this->cartRepository->queryUpdate($this->cartRepository->model->where('id', $cartItem['id']), ['quantity' => $cartItem['quantity']])) {

                throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message']);
            }
        }

        return true;
    }
}