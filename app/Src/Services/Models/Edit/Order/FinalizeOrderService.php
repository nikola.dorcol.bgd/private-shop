<?php

namespace App\Src\Services\Models\Edit\Order;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Mails\OrderAcceptedMail;
use App\Src\Mails\OrderDeclinedMail;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use App\Src\Orders\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class FinalizeOrderService extends AbstractEditService
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var EditOrderProductVariantInventory
     */
    protected $editOrderProductVariantInventory;


    /**
     * FinalizeOrderService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->orderRepository                  = app()->make(OrderRepositoryInterface::class);
        $this->editOrderProductVariantInventory = app()->make(EditOrderProductVariantInventory::class);
    }


    /**
     * Finalize order.
     *
     * @param Model $order
     * @param array $data
     * @return Model
     * @throws ModelNotUpdatedException
     */
    public function edit(Model $order, array $data): Model
    {
        $orderData = [
            'status'        => $data['status'],
            'admin_comment' => $data['admin_comment'],
            'final'         => 1
        ];

        if (! $this->updateService->update($this->orderRepository, $order, $orderData)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message']);
        }

        $order = $order->fresh('user');

        if ($orderData['status'] == Order::STATUS_ACCEPTED) {

            foreach ($data['cart'] as $cart) {

                if (! $this->editOrderProductVariantInventory->edit($cart['id'], $cart['warehouse_id'])) {

                    throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message']);
                }
            }

            Mail::to($order->user->email)->queue(new OrderAcceptedMail($order));
        }

        if ($orderData['status'] == Order::STATUS_DECLINED) {

            Mail::to($order->user->email)->queue(new OrderDeclinedMail($order));
        }

        return $order;
    }
}