<?php

namespace App\Src\Services\Models\Edit\Order;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Orders\Order;
use App\Src\Orders\ProductVariants\Contracts\OrderProductVariantRepositoryInterface;
use App\Src\Services\Models\Edit\Warehouse\EditWarehouseInventoryService;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EditOrderProductVariantService extends AbstractEditService
{
    /**
     * @var OrderProductVariantRepositoryInterface
     */
    protected $orderProductVariantRepository;

    /**
     * @var EditOrderProductVariantInventory
     */
    protected $editOrderProductVariantInventory;


    /**
     * CreateOrderProductVariantService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->orderProductVariantRepository    = app()->make(OrderProductVariantRepositoryInterface::class);
        $this->editOrderProductVariantInventory = app()->make(EditOrderProductVariantInventory::class);
    }


    public function edit(Model $orderProductVariant, array $data): Model
    {
        if (! $this->updateService->update($this->orderProductVariantRepository, $orderProductVariant, $data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        if ($data['status'] == Order::STATUS_ACCEPTED) {

            if (! $this->editOrderProductVariantInventory->edit($data['product_variant_id'], $data['warehouse_id'])) {

                throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message']);
            }
        }

        return $orderProductVariant;
    }
}