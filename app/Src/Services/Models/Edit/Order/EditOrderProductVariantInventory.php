<?php

namespace App\Src\Services\Models\Edit\Order;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Orders\ProductVariants\Contracts\OrderProductVariantRepositoryInterface;
use App\Src\Services\Models\Edit\Warehouse\EditWarehouseInventoryService;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EditOrderProductVariantInventory
{
    /**
     * @var OrderProductVariantRepositoryInterface
     */
    protected $orderProductVariantRepository;

    /**
     * @var WarehouseInventoryRepositoryInterface
     */
    protected $warehouseInventoryRepository;

    /**
     * @var EditWarehouseInventoryService
     */
    protected $editWarehouseInventoryService;


    /**
     * EditOrderProductVariantInventory constructor.
     */
    public function __construct()
    {
        $this->orderProductVariantRepository= app()->make(OrderProductVariantRepositoryInterface::class);
        $this->warehouseInventoryRepository = app()->make(WarehouseInventoryRepositoryInterface::class);
        $this->editWarehouseInventoryService= app()->make(EditWarehouseInventoryService::class);
    }


    /**
     * When order is finalized and accepted update warehouse inventory for given product.
     *
     * @param int $orderProductVariantId
     * @param int $warehouseId
     * @return bool
     * @throws ModelNotUpdatedException
     */
    public function edit(int $orderProductVariantId, int $warehouseId): bool
    {
        if (! $orderProductVariant = $this->orderProductVariantRepository->find($orderProductVariantId)) {

            throw new ModelNotFoundException(Errors::MODEL_NOT_FOUND['error_message']);
        }

        if (! $warehouseInventory = $this->warehouseInventoryRepository->getProductVariantWarehouseInventory($orderProductVariant->product_variant_id, $warehouseId)) {

            throw new ModelNotFoundException(Errors::MODEL_NOT_FOUND['error_message']);
        }

        $inventoryData = [
            'price'     => $orderProductVariant->productVariant->price,
            'quantity'  => $orderProductVariant->quantity,
        ];

        if (! $this->editWarehouseInventoryService->edit($warehouseInventory, $inventoryData)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message']);
        }

        return true;
    }
}