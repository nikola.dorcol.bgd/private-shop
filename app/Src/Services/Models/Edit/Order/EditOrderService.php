<?php

namespace App\Src\Services\Models\Edit\Order;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Base\Exceptions\ModelsNotDeletedException;
use App\Src\Mails\OrderAcceptedMail;
use App\Src\Mails\OrderDeclinedMail;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use App\Src\Orders\Order;
use App\Src\Orders\ProductVariants\Contracts\OrderProductVariantRepositoryInterface;
use App\Src\Services\Models\Create\Order\CreateOrderProductVariantService;
use App\Src\Services\Models\Create\User\AdminCreateCustomerService;
use App\Src\Services\Models\Helpers\PrepareBillingInfoData;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Mail;

class EditOrderService extends AbstractEditService
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var OrderProductVariantRepositoryInterface
     */
    protected $orderProductVariantRepository;

    /**
     * @var AdminCreateCustomerService
     */
    protected $adminCreateCustomerService;

    /**
     * @var CreateOrderProductVariantService
     */
    protected $createOrderProductVariantService;

    /**
     * @var EditOrderProductVariantService
     */
    protected $editOrderProductVariantService;


    /**
     * EditOrderService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->orderRepository                  = app()->make(OrderRepositoryInterface::class);
        $this->orderProductVariantRepository    = app()->make(OrderProductVariantRepositoryInterface::class);
        $this->adminCreateCustomerService       = app()->make(AdminCreateCustomerService::class);
        $this->createOrderProductVariantService = app()->make(CreateOrderProductVariantService::class);
        $this->editOrderProductVariantService   = app()->make(EditOrderProductVariantService::class);
    }


    public function edit(Model $order, array $data): Model
    {
        $billingInfoData = PrepareBillingInfoData::prepare($data);


        if (! isset($billingInfoData['user_id']) || is_null($billingInfoData['user_id'])) {

            $billingInfoData['user_id'] = $this->adminCreateCustomerService->create($billingInfoData)->id;
        }


        if (! $this->updateService->update($this->orderRepository, $order, $billingInfoData)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }


        $this->orderProductVariantRepository->deleteOrderRemovedProductVariants($order, array_column($data['products'], 'order_product_variant_id'));


        foreach ($data['products'] as $product) {

            $product['status'] = $billingInfoData['status'];

            if ($product['order_product_variant_id']) {

                if (! $orderProductVariant = $this->orderProductVariantRepository->find($product['order_product_variant_id'])) {

                    throw new ModelNotFoundException(Errors::MODEL_NOT_FOUND['error_message'], 400);
                }

                if (! $this->editOrderProductVariantService->edit($orderProductVariant, $product)) {

                    throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
                }

            } else {

                $product['order_id'] = $order->id;

                if (! $this->createOrderProductVariantService->create($product)) {

                    throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
                }
            }
        }

        $order = $order->fresh('user');

        if ($billingInfoData['final'] == 1) {

            if ($billingInfoData['status'] == Order::STATUS_ACCEPTED) {

                Mail::to($order->user->email)->queue(new OrderAcceptedMail($order));
            }

            if ($billingInfoData['status'] == Order::STATUS_DECLINED) {

                Mail::to($order->user->email)->queue(new OrderDeclinedMail($order));
            }
        }

        return $order;
    }
}