<?php

namespace App\Src\Services\Models\Edit;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Brands\Brand;
use App\Src\Brands\Contracts\BrandRepositoryInterface;
use App\Src\Services\Images\PrepareImageData;
use App\Src\Services\Models\Create\CreateImageService;
use Illuminate\Database\Eloquent\Model;

class EditBrandService extends AbstractEditService
{
    /**
     * @var BrandRepositoryInterface
     */
    protected $brandRepository;

    /**
     * @var CreateImageService
     */
    protected $createImageService;

    /**
     * @var EditImageService
     */
    protected $editImageService;


    /**
     * EditBrandService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->brandRepository      = app()->make(BrandRepositoryInterface::class);
        $this->editImageService     = app()->make(EditImageService::class);
        $this->createImageService   = app()->make(CreateImageService::class);
    }


    public function edit(Model $brand, array $data): Model
    {
        if (! $this->updateService->update($this->brandRepository, $brand, $data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        if (isset($data['logo'])) {

            $imageData      = PrepareImageData::prepare($brand->id, Brand::class, 'logo', 'brands', $data['logo']);
            $categoryIcon   = $brand->logo;

            if (! $categoryIcon) {

                if (! $this->createImageService->create($imageData)) {

                    throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
                }

            } else {

                if (! $this->editImageService->edit($categoryIcon, $imageData)) {

                    throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
                }
            }
        }


        return $brand->fresh();
    }
}