<?php

namespace App\Src\Services\Models\Edit;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\FileNotUploadedException;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Images\Contracts\ImageRepositoryInterface;
use App\Src\Images\ImageObject;
use App\Src\Services\Contracts\RemoveImageServiceInterface;
use App\Src\Services\Contracts\UploadImageServiceInterface;
use Illuminate\Database\Eloquent\Model;

class EditImageService extends AbstractEditService
{
    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var UploadImageServiceInterface
     */
    protected $uploadImageService;

    /**
     * @var RemoveImageServiceInterface
     */
    protected $removeImageService;


    /**
     * EditImageService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->imageRepository      = app()->make(ImageRepositoryInterface::class);
        $this->uploadImageService   = app()->make(UploadImageServiceInterface::class);
        $this->removeImageService   = app()->make(RemoveImageServiceInterface::class);
    }


    public function edit(Model $image, array $data): Model
    {
        if (isset($data['image']) && $data['image'] instanceof ImageObject) {

            if (! $newImage = $this->uploadImageService->upload($data['image'])) {

                throw new FileNotUploadedException(Errors::FILE_NOT_UPLOADED['error_message'], 400);
            }

            unset($data['image']);

            $data['src']        = $newImage['src'];
            $data['storage']    = $newImage['storage'];

        // Can not check if success because of potential loop of upload/delete checks, can allow one more unused image in storage.
            $this->removeImageService->remove($image);
        }

        if (! $this->updateService->update($this->imageRepository, $image, $data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        return $image->fresh();
    }
}