<?php

namespace App\Src\Services\Models\Edit\Product;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Base\Exceptions\ModelsNotDeletedException;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Services\Models\Destroy\Cart\AdminDestroyManyCarts;
use Illuminate\Database\Eloquent\Model;

class EditProductActiveStatusService extends AbstractEditService
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var AdminDestroyManyCarts
     */
    protected $adminDestroyManyCarts;


    /**
     * EditProductActiveStatusService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->productRepository        = app()->make(ProductRepositoryInterface::class);
        $this->adminDestroyManyCarts    = app()->make(AdminDestroyManyCarts::class);
    }


    /**
     * When product active status is changed, if it is inactive remove all users carts with given product.
     *
     * @param Model $product
     * @param array $data
     * @return Model
     * @throws ModelNotUpdatedException
     * @throws ModelsNotDeletedException
     */
    public function edit(Model $product, array $data): Model
    {
        if (! $this->updateService->update($this->productRepository, $product, ['active' => $data['active']])) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        if ($data['active'] == 0) {

            if (! $this->adminDestroyManyCarts->destroyMany($product)) {

                throw new ModelsNotDeletedException(Errors::MODELS_NOT_DELETED['error_message']);
            }
        }

        return $product;
    }
}