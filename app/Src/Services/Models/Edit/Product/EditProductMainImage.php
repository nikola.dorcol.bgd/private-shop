<?php

namespace App\Src\Services\Models\Edit\Product;


use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Images\Contracts\ImageRepositoryInterface;
use App\Src\Images\Image;
use App\Src\Services\Models\Edit\EditImageService;
use Illuminate\Database\Eloquent\Model;

class EditProductMainImage extends AbstractEditService
{
    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @var EditImageService
     */
    protected $editImageService;


    /**
     * EditProductMainImage constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->imageRepository  = app()->make(ImageRepositoryInterface::class);
        $this->editImageService = app()->make(EditImageService::class);
    }


    public function edit(Model $product, array $data): Model
    {
        if (! $this->editImageService->edit($data['image'], ['type' => Image::TYPE_PRODUCT_MAIN])) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        $this->imageRepository->changeUnnecessaryProductMainTypes($data['image']->id, $product->id);

        return $product;
    }
}