<?php

namespace App\Src\Services\Models\Edit\Product;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\MismatchDataException;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use App\Src\Products\Variants\ProductVariant;
use App\Src\Services\Models\Edit\Warehouse\EditWarehouseInventoryService;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use App\Src\Warehouses\Inventory\WarehouseInventory;
use Illuminate\Database\Eloquent\Model;

class EditProductVariantService extends AbstractEditService
{
    /**
     * @var ProductVariantRepositoryInterface
     */
    protected $productVariantRepository;

    /**
     * @var WarehouseInventoryRepositoryInterface
     */
    protected $warehouseInventoryRepository;

    /**
     * @var EditWarehouseInventoryService
     */
    protected $editWarehouseInventoryService;


    /**
     * EditProductVariantService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->productVariantRepository     = app()->make(ProductVariantRepositoryInterface::class);
        $this->warehouseInventoryRepository = app()->make(WarehouseInventoryRepositoryInterface::class);
        $this->editWarehouseInventoryService= app()->make(EditWarehouseInventoryService::class);
    }


    /**
     * Required data: sku, price, inventories
     *
     * @param Model $productVariant
     * @param array $data
     * @return Model
     * @throws ModelNotUpdatedException
     */
    public function edit(Model $productVariant, array $data): Model
    {
        if (! $this->updateService->update($this->productVariantRepository, $productVariant, $data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        foreach ($data['inventories'] as $inventory) {

            $this->editWarehouseInventoryHelper($productVariant, $inventory['entity'], $data['price'], $inventory['quantity']);
        }

        return $productVariant;
    }


    /**
     * Edit warehouse_inventory row in db table helper method.
     *
     * @param ProductVariant $productVariant
     * @param WarehouseInventory $warehouseInventory
     * @param float $price
     * @param int $quantity
     * @return bool
     * @throws MismatchDataException
     * @throws ModelNotUpdatedException
     */
    private function editWarehouseInventoryHelper(ProductVariant $productVariant, WarehouseInventory $warehouseInventory, float $price, int $quantity): bool
    {
        if ($warehouseInventory->product_variant_id != $productVariant->id) {

            throw new MismatchDataException(Errors::MISMATCH_DATA['error_message'], 400);
        }

        $warehouseInventoryData = [
            'price'     => $price,
            'quantity'  => $quantity
        ];

        if (! $this->editWarehouseInventoryService->edit($warehouseInventory, $warehouseInventoryData)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        return true;
    }
}