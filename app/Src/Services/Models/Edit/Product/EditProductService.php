<?php

namespace App\Src\Services\Models\Edit\Product;

use App\Src\Base\AbstractEditService;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\MismatchDataException;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Base\Exceptions\ModelRelationNotSynced;
use App\Src\Base\Exceptions\ModelsNotDeletedException;
use App\Src\Images\Contracts\ImageRepositoryInterface;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Products\Product;
use App\Src\Services\Cache\RepositoryModelCache;
use App\Src\Services\Contracts\CreateModelImageServiceInterface;
use App\Src\Services\Contracts\SyncInterface;
use App\Src\Services\Models\Create\Product\CreateManyProductVariantsService;
use App\Src\Services\Models\Create\Warehouse\CreateWarehouseInventoryService;

use App\Src\Services\Models\Destroy\Cart\AdminDestroyManyCarts;
use App\Src\Services\Models\Helpers\Product\ProductServiceHelper;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class EditProductService extends AbstractEditService
{
    use ProductServiceHelper;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var AdminDestroyManyCarts
     */
    protected $adminDestroyManyCarts;

    /**
     * @var SyncInterface
     */
    protected $syncService;

    /**
     * @var EditProductVariantService
     */
    protected $editProductVariantService;

    /**
     * @var CreateWarehouseInventoryService
     */
    protected $createWarehouseInventoryService;

    /**
     * @var WarehouseInventoryRepositoryInterface
     */
    protected $warehouseInventoryRepository;

    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;


    /**
     * EditProductService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->productRepository                = app()->make(ProductRepositoryInterface::class);
        $this->adminDestroyManyCarts            = app()->make(AdminDestroyManyCarts::class);
        $this->createManyProductVariantsService = app()->make(CreateManyProductVariantsService::class);
        $this->createModelImageService          = app()->make(CreateModelImageServiceInterface::class);
        $this->syncService                      = app()->make(SyncInterface::class);
        $this->editProductVariantService        = app()->make(EditProductVariantService::class);
        $this->createWarehouseInventoryService  = app()->make(CreateWarehouseInventoryService::class);
        $this->warehouseInventoryRepository     = app()->make(WarehouseInventoryRepositoryInterface::class);
        $this->imageRepository                  = app()->make(ImageRepositoryInterface::class);
    }


    /**
     * Required data: \App\Src\Products\Requests\UpdateProductRequest
     *
     * @param Model $product
     * @param array $data
     * @return Model
     * @throws ModelNotUpdatedException
     * @throws ModelRelationNotSynced
     * @throws ModelsNotDeletedException
     */
    public function edit(Model $product, array $data): Model
    {
        if (! $this->updateService->update($this->productRepository, $product, $data['base_info'])) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
        }

        if ($data['base_info']['active'] == 0) {

            if (! $this->adminDestroyManyCarts->destroyMany($product)) {

                throw new ModelsNotDeletedException(Errors::MODELS_NOT_DELETED['error_message']);
            }
        }

        if (empty($this->syncService->sync($product, 'categories', $data['categories']))) {

            throw new ModelRelationNotSynced(Errors::MODEL_RELATION_NOT_SYNCED['error_message'], 400);
        }

        if (isset($data['related_products'])) {

            if (empty($this->syncService->sync($product, 'relatedProducts', $data['related_products']))) {

                throw new ModelRelationNotSynced(Errors::MODEL_RELATION_NOT_SYNCED['error_message'], 400);
            }
        }

        $this->variantsHelper($product, $data['variants']);

        if (! empty($data['images'])) {

            $this->createProductImagesHelper($product, $data['images']);
        }

        if ($product->recommended) {

            app()->make(RepositoryModelCache::class)->clearCache(['recommended_products', 'products']);
        }

        return $product;
    }


    /**
     * Sort variants by new/old.
     *
     * @param Product $product
     * @param array $variants
     * @throws MismatchDataException
     * @throws ModelNotCreatedException
     * @throws ModelNotUpdatedException
     */
    private function variantsHelper(Product $product, array $variants): void
    {
        $oldVariants = [];
        $newVariants = [];

        foreach ($variants as $variant) {

            isset($variant['id']) ? array_push($oldVariants, $variant) : array_push($newVariants, $variant);
        }

        if (! empty($newVariants)) {

            $this->createManyProductVariantsHelper($product, $newVariants);
        }

        if (! empty($oldVariants)) {

            foreach ($oldVariants as $variant) {

                if (! $productVariant = $product->variants->where('id', $variant['id'])->first()) {

                    throw new MismatchDataException(Errors::MODEL_NOT_FOUND['error_message'], 400);
                }

                $variant['inventories'] = [];

                foreach ($variant['warehouses'] as $warehouse) {

                    if (! $inventory = $this->warehouseInventoryRepository->getProductVariantWarehouseInventory($productVariant->id, $warehouse['id'])) {

                        $warehouseInventoryData = [
                            'product_variant_id'    => $productVariant->id,
                            'warehouse_id'          => $warehouse['id'],
                            'quantity'              => $warehouse['quantity'],
                            'price'                 => $variant['price']
                        ];

                        if (! $warehouseInventory = $this->createWarehouseInventoryService->create($warehouseInventoryData)) {

                            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
                        }


                    } else {

                        array_push($variant['inventories'], [

                            'entity'    => $inventory,
                            'quantity'  => $warehouse['quantity']
                        ]);
                    }
                }

                if (! $this->editProductVariantService->edit($productVariant, $variant)) {

                    throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
                }
            }
        }
    }
}