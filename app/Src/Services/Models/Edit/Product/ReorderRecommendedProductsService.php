<?php

namespace App\Src\Services\Models\Edit\Product;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotUpdatedException;
use App\Src\Products\Recommended\Contracts\RecommendedProductRepositoryInterface;

final class ReorderRecommendedProductsService
{
    /**
     * @var RecommendedProductRepositoryInterface
     */
    protected $recommendedProductRepository;


    /**
     * ReorderRecommendedProducts constructor.
     */
    public function __construct()
    {
        $this->recommendedProductRepository = app()->make(RecommendedProductRepositoryInterface::class);
    }


    /**
     * @param array $recommendedProducts
     * @return bool
     * @throws ModelNotUpdatedException
     */
    public function reorder(array $recommendedProducts): bool
    {
        foreach ($recommendedProducts as $product) {

            if (! $this->recommendedProductRepository->updatePosition($product['id'], $product['position'])) {

                throw new ModelNotUpdatedException(Errors::MODEL_NOT_UPDATED['error_message'], 400);
            }
        }

        return true;
    }
}