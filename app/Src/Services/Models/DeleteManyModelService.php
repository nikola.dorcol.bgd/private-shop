<?php

namespace App\Src\Services\Models;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use App\Src\Services\Contracts\DeleteManyInterface;

class DeleteManyModelService implements DeleteManyInterface
{

    public function deleteMany(AbstractRepositoryInterface $repository, array $ids): bool
    {
        return $repository->destroy($ids);
    }
}