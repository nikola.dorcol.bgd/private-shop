<?php

namespace App\Src\Services\Models;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use App\Src\Services\Contracts\DeleteInterface;
use Illuminate\Database\Eloquent\Model;

class DeleteModelService implements DeleteInterface
{

    public function delete(AbstractRepositoryInterface $repository, Model $model): bool
    {
        return $repository->delete($model);
    }
}