<?php

namespace App\Src\Services\Models\Insert\Attribute;

use App\Src\Attributes\Values\Contracts\AttributeValueRepositoryInterface;
use App\Src\Base\AbstractInsertService;
use Illuminate\Database\Eloquent\Model;

class InsertAttributeValuesService extends AbstractInsertService
{
    /**
     * @var AttributeValueRepositoryInterface
     */
    protected $attributeValueRepository;


    /**
     * InsertAttributeValues constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->attributeValueRepository = app()->make(AttributeValueRepositoryInterface::class);
    }


    public function insert(array $data, ?Model $attribute = null): bool
    {
        $insertData = [];

        foreach ($data as $value) {

            $valueData['value']         = sanitizeString($value);
            $valueData['attribute_id']  = $attribute->id;

            array_push($insertData, $valueData);
        }

        return $this->insertService->insert($this->attributeValueRepository, $insertData);
    }
}