<?php

namespace App\Src\Services\Models\Get\Cart;

use App\Src\Services\Contracts\GetCartInterface;

class GetCartSeessionService implements GetCartInterface
{
    /**
     * @return array
     */
    public function get(): array
    {
        return session()->exists('carts') ? session()->get('carts') : [];
    }
}