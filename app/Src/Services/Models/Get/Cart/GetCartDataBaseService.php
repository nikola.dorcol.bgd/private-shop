<?php

namespace App\Src\Services\Models\Get\Cart;

use App\Src\Carts\Contracts\CartRepositoryInterface;
use App\Src\Services\Contracts\GetCartInterface;

class GetCartDataBaseService implements GetCartInterface
{
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;


    /**
     * GetCartDataBaseService constructor.
     */
    public function __construct()
    {
        $this->cartRepository = app()->make(CartRepositoryInterface::class);
    }


    /**
     * @return array
     */
    public function get(): array
    {
        return $this->cartRepository->getUserCart(auth()->user())->toArray();
    }
}