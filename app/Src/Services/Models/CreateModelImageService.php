<?php

namespace App\Src\Services\Models;

use App\Src\Base\Errors;
use App\Src\Base\Exceptions\ModelNotCreatedException;
use App\Src\Images\Image;
use App\Src\Services\Contracts\CreateModelImageServiceInterface;
use App\Src\Services\Images\PrepareImageData;
use App\Src\Services\Models\Create\CreateImageService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class CreateModelImageService implements CreateModelImageServiceInterface
{
    /**
     * @var CreateImageService
     */
    protected $createImageService;


    /**
     * CreateModelImageService constructor.
     */
    public function __construct()
    {
        $this->createImageService = app()->make(CreateImageService::class);
    }


    public function create(Model $model, string $type, string $folder, UploadedFile $file): Image
    {
        $imageData = PrepareImageData::prepare($model->id, get_class($model), $type, $folder, $file);

        if (! $image = $this->createImageService->create($imageData)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }

        return $image;
    }
}