<?php

namespace App\Src\Services\Models;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use App\Src\Services\Contracts\StoreInterface;
use App\Src\Services\Models\Helpers\ModelAttributeMapper;
use Illuminate\Database\Eloquent\Model;

class StoreModelService implements StoreInterface
{

    public function store(AbstractRepositoryInterface $repository, array $data): ?Model
    {
        $mappedData = ModelAttributeMapper::map($repository->model, $data, 'fillable');

        return $repository->store($mappedData);
    }
}