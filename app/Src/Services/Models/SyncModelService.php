<?php

namespace App\Src\Services\Models;

use App\Src\Services\Contracts\SyncInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SyncModelService implements SyncInterface
{

    public function sync(Model $model, string $relation, array $ids): array
    {
        $syncArray = [];

        foreach ($ids as $id) {

            $syncArray[$id] = [
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ];
        }

        return $model->{$relation}()->sync($syncArray);
    }
}