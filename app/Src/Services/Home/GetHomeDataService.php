<?php

namespace App\Src\Services\Home;

use App\Src\Products\Contracts\ProductRepositoryInterface;

class GetHomeDataService
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;


    /**
     * GetHomeDataService constructor.
     */
    public function __construct()
    {
        $this->productRepository = app()->make(ProductRepositoryInterface::class);
    }


    /**
     * Get data for Homepage.
     *
     * @return array
     */
    public function getData(): array
    {
        $data['recommended_products']   = $this->productRepository->getRecommendedProducts();
        $data['latest_products']        = $this->productRepository->getLatestProducts();

        return $data;
    }
}