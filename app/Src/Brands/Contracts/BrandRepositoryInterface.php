<?php

namespace App\Src\Brands\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface BrandRepositoryInterface
{
    /**
     * Get brands for data table.
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): LengthAwarePaginator;
}