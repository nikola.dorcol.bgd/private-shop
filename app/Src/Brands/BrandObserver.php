<?php

namespace App\Src\Brands;

use App\Src\Base\AbstractObserver;

class BrandObserver extends AbstractObserver
{
    /**
     * Handle the Brand "created" event.
     *
     * @param Brand $category
     * @return void
     */
    public function created(Brand $category): void
    {
        $this->clearCacheTags($category->getTable());
    }


    /**
     * Handle the Brand "updated" event.
     *
     * @param Brand $category
     * @return void
     */
    public function updated(Brand $category)
    {
        $this->clearCacheTags($category->getTable());
    }


    /**
     * Handle the Brand "deleted" event.
     *
     * @param Brand $category
     * @return void
     */
    public function deleted(Brand $category)
    {
        $this->clearCacheTags($category->getTable());
    }
}