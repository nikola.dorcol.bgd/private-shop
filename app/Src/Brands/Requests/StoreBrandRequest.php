<?php

namespace App\Src\Brands\Requests;

use App\Src\Base\AbstractFormRequest;

class StoreBrandRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'          => 'required',
            'description'   => 'present',
            'logo'          => 'required|file|mimes:jpeg,jpg,png|max:10000'
        ];
    }
}