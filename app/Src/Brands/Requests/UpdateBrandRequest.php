<?php

namespace App\Src\Brands\Requests;

use App\Src\Base\AbstractFormRequest;

class UpdateBrandRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'          => 'required',
            'description'   => 'nullable',
            'logo'          => 'nullable|file|mimes:jpeg,jpg,png|max:10000'
        ];
    }
}