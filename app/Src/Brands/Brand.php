<?php

namespace App\Src\Brands;

use App\Src\Images\Image;
use App\Src\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Nicolaslopezj\Searchable\SearchableTrait;

class Brand extends Model
{
    use SearchableTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'brands';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'logo'
    ];

    /**
     * Search rules.
     *
     * @var array
     */
    protected $searchable = [

        'columns' => [
            'brands.name' => 10
        ]
    ];


    /**
     * Get icon path for category.
     *
     * @return Image|null
     */
    public function getLogoAttribute(): ?Image
    {
        return $this->images()->logo()->first();
    }


    /**
     * Get all products that belong to product.
     *
     * @return HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(Product::class, 'brand_id');
    }


    /**
     * Get all of category's images.
     *
     * @return MorphMany
     */
    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}