<?php

namespace App\Src\Brands\Validations;

use App\Src\Base\Contracts\ValidationInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyBrandValidation implements ValidationInterface
{

    public function validate(Model $brand, ?array $data = null): bool
    {
        return $brand->products->count() > 0;
    }
}