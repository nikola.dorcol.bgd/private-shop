<?php

namespace App\Src\Brands\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Base\Contracts\DataTableRepositoryInterface;
use App\Src\Brands\Brand;
use App\Src\Brands\Contracts\BrandRepositoryInterface;
use App\Src\Brands\Queries\EloquentRepositoryQueries\BrandDataTableQuery;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class BrandEloquentRepository extends AbstractRepository implements BrandRepositoryInterface, DataTableRepositoryInterface
{

    public function getModelClass(): string
    {
        return Brand::class;
    }


    public function getAll(): LengthAwarePaginator
    {
        $query = $this->model->query();

        return $this->paginate($query);
    }


    public function dataTable(array $data): LengthAwarePaginator
    {
        return $this->paginate((new BrandDataTableQuery())->build($this->model, $data), $data['perPage']);
    }
}