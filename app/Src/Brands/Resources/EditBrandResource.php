<?php

namespace App\Src\Brands\Resources;

use App\Src\Images\Resources\ImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EditBrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'logo'          => new ImageResource($this->logo),
            'name'          => $this->name,
            'description'   => $this->description
        ];
    }
}