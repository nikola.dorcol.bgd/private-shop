<?php

namespace App\Src\Brands\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BrandDataTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'products_count'    => $this->products_count,
            'created_at'        => $this->created_at
        ];
    }
}