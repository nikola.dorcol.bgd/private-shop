<?php

namespace App\Src\Orders\Contracts;

use App\Src\Products\Product;
use Illuminate\Database\Eloquent\Collection;

interface OrderRepositoryInterface
{
    /**
     * Get all orders for given product.
     *
     * @param Product $product
     * @return Collection
     */
    public function getProductOrders(Product $product): Collection;
}