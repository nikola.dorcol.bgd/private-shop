<?php

namespace App\Src\Orders\Validations;

use App\Src\Base\Contracts\ValidationInterface;
use Illuminate\Database\Eloquent\Model;

class HandleOrderValidation implements ValidationInterface
{
    /**
     * Can not delete order that is final.
     *
     * @param Model $order
     * @param array|null|null $data
     * @return bool
     */
    public function validate(Model $order, ?array $data = null): bool
    {
        return $order->final;
    }
}