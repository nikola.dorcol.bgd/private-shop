<?php

namespace App\Src\Orders;

use App\Src\Base\AbstractObserver;

class OrderObserver extends AbstractObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param Order $order
     * @return void
     */
    public function created(Order $order): void
    {
        $this->clearCacheTags($order->getTable());
    }


    /**
     * Handle the Order "updated" event.
     *
     * @param Order $order
     * @return void
     */
    public function updated(Order $order)
    {
        $this->clearCacheTags($order->getTable());
    }


    /**
     * Handle the Order "deleted" event.
     *
     * @param Order $order
     * @return void
     */
    public function deleted(Order $order)
    {
        $this->clearCacheTags($order->getTable());
    }
}