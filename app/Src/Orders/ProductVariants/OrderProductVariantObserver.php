<?php

namespace App\Src\Orders\ProductVariants;

use App\Src\Base\AbstractObserver;

class OrderProductVariantObserver extends AbstractObserver
{
    /**
     * Handle the OrderProductVariant "created" event.
     *
     * @param OrderProductVariant $orderProductVariant
     * @return void
     */
    public function created(OrderProductVariant $orderProductVariant): void
    {
        $this->clearCacheTags($orderProductVariant->getTable());
    }


    /**
     * Handle the OrderProductVariant "updated" event.
     *
     * @param OrderProductVariant $orderProductVariant
     * @return void
     */
    public function updated(OrderProductVariant $orderProductVariant)
    {
        $this->clearCacheTags($orderProductVariant->getTable());
    }


    /**
     * Handle the OrderProductVariant "deleted" event.
     *
     * @param OrderProductVariant $orderProductVariant
     * @return void
     */
    public function deleted(OrderProductVariant $orderProductVariant)
    {
        $this->clearCacheTags($orderProductVariant->getTable());
    }
}