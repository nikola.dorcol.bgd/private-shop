<?php

namespace App\Src\Orders\ProductVariants;

use App\Src\Base\AbstractRepository;
use App\Src\Orders\Order;
use App\Src\Orders\ProductVariants\Contracts\OrderProductVariantRepositoryInterface;

class OrderProductVariantEloquentRepository extends AbstractRepository implements OrderProductVariantRepositoryInterface
{

    function getModelClass(): string
    {
        return OrderProductVariant::class;
    }


    public function deleteOrderRemovedProductVariants(Order $order, array $activeIds): bool
    {
        return $this->model->query()
            ->where('order_id', $order->id)
            ->whereNotIn('id', $activeIds)
            ->delete();
    }
}