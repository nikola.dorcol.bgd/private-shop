<?php

namespace App\Src\Orders\ProductVariants\Contracts;

use App\Src\Orders\Order;

interface OrderProductVariantRepositoryInterface
{
    /**
     * When editing order, remove all product variants that are no longer selected for order.
     *
     * @param Order $order
     * @param array $activeIds
     * @return bool
     */
    public function deleteOrderRemovedProductVariants(Order $order, array $activeIds): bool;
}