<?php

namespace App\Src\Orders\ProductVariants\Rules;

use App\Src\Orders\Order;
use App\Src\Orders\ProductVariants\Contracts\OrderProductVariantRepositoryInterface;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class CheckProductVariantBelongsToOrder implements Rule
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var OrderProductVariantRepositoryInterface
     */
    private $orderProductVariantRepository;


    /**
     * Create a new rule instance.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order                            = $order;
        $this->orderProductVariantRepository    = app()->make(OrderProductVariantRepositoryInterface::class);
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    // If value is 0, means this is new order product variant.
        if ($value == 0) {

            return true;
        }

        if (! $orderProductVariant = $this->orderProductVariantRepository->find($value)) {

            return false;
        }

        return $this->order->id == $orderProductVariant->order_id;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid order product variant.';
    }
}