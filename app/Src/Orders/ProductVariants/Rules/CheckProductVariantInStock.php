<?php

namespace App\Src\Orders\ProductVariants\Rules;

use App\Src\Orders\Order;
use App\Src\Orders\ProductVariants\Contracts\OrderProductVariantRepositoryInterface;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class CheckProductVariantInStock implements Rule
{
    /**
     * @var OrderProductVariantRepositoryInterface
     */
    private $orderProductVariantRepository;

    /**
     * @var WarehouseInventoryRepositoryInterface
     */
    private $warehouseInventoryRepository;


    /**
     * Create a new rule instance.
     */
    public function __construct()
    {
        $this->orderProductVariantRepository    = app()->make(OrderProductVariantRepositoryInterface::class);
        $this->warehouseInventoryRepository     = app()->make(WarehouseInventoryRepositoryInterface::class);
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (! $orderProductVariant = $this->orderProductVariantRepository->find($value['id'])) {

            return false;
        }

        if (! isset($value['warehouse_id'])) {

            return false;
        }

        if (! $warehouseInventory = $this->warehouseInventoryRepository->getProductVariantWarehouseInventory($orderProductVariant->product_variant_id, $value['warehouse_id'])) {

            return false;
        }

        return $warehouseInventory->quantity >= $orderProductVariant->quantity;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Not In Stock.';
    }
}