<?php

namespace App\Src\Orders\ProductVariants\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShowOrderProductVariantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'                    => $this->id,
            'product_variant'       => $this->productVariant,
            'product_id'            => $this->productVariant->product->id,
            'product_name'          => $this->productVariant->product->name,
            'product_main_image'    => $this->productVariant->product->main_image,
            'quantity'              => $this->quantity,
            'price'                 => $this->price
        ];
    }
}