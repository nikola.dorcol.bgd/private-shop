<?php

namespace App\Src\Orders\ProductVariants\Resources;

use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Attributes\Resources\AttributeResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EditOrderProductVariantResource extends JsonResource
{
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;


    /**
     * EditProductResource constructor.
     *
     * @param mixed $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);

        $this->attributeRepository = app()->make(AttributeRepositoryInterface::class);
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_product_variant_id'  => $this->id,
            'product_variant_id'        => $this->product_variant_id,
            'product_variant'           => $this->productVariant->product->variants->where('id', $this->product_variant_id)->first(),
            'product_id'                => $this->productVariant->product->id,
            'product_name'              => $this->productVariant->product->name,
            'product_main_image'        => $this->productVariant->product->main_image,
            'variants'                  => $this->productVariant->product->variants,
            'quantity'                  => $this->quantity,
            'price'                     => $this->price,
            'base_price'                => $this->productVariant->price,
            'attributes'                => AttributeResource::collection($this->attributeRepository->getProductAttributes($this->productVariant->product))
        ];
    }
}