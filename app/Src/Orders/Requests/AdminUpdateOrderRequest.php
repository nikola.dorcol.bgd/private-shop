<?php

namespace App\Src\Orders\Requests;

use App\Src\Orders\ProductVariants\Rules\CheckProductVariantBelongsToOrder;

class AdminUpdateOrderRequest extends AdminStoreOrderRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = parent::rules();

        $rules['products.*.order_product_variant_id'] = new CheckProductVariantBelongsToOrder($this->route('order'));

        return parent::rules();
    }
}