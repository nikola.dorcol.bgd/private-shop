<?php

namespace App\Src\Orders\Requests;

use App\Src\Base\AbstractFormRequest;
use App\Src\Base\Rules\ValidatePhoneNumberRule;
use App\Src\Orders\Order;
use App\Src\Orders\ProductVariants\Rules\CheckProductVariantBelongsToOrder;
use App\Src\Products\Variants\Rules\CanOrderProductVariantRule;
use Illuminate\Validation\Rule;

class AdminStoreOrderRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'billing_info'                          => 'required|array',
            'billing_info.user_id'                  => 'nullable|exists:users,id',
            'billing_info.status'                   => [
                'required',
                Rule::in(Order::STATUSES)
            ],
            'billing_info.admin_comment'            => 'nullable|string',
            'billing_info.user_comment'             => 'nullable|string',
            'billing_info.billing_name'             => 'required|string',
            'billing_info.billing_email'            => [
                'required_if:billing_info.user_id,null',
                isset($this->request->get('billing_info')['email']) ?
                    $this->request->get('billing_info')['email'] ?
                        'email' :
                        '' : ''
            ],
            'billing_info.billing_phone_number'     => [
                'required',
                'string',
//                new ValidatePhoneNumberRule()
            ],
            'billing_info.billing_address'          => 'required|string',
            'billing_info.billing_city'             => 'required|string',
            'billing_info.billing_state'            => 'present',
            'billing_info.billing_zip'              => 'required|string',
            'billing_info.billing_floor'            => 'required|int',
            'billing_info.billing_intercom'         => 'required|string',
            'billing_info.billing_flat'             => 'required|string',
            'billing_info.delivery_company'         => 'required|string',

            'products'                              => 'required|array',
            'products.*.product_variant_id'         => 'required|exists:product_variants,id',
            'products.*.quantity'                   => 'required|int|min:1',
            'products.*.warehouse_id'               => 'required_if:billing_info.status,' . Order::STATUS_ACCEPTED . '|exists:warehouses,id',
            'products.*.price'                      => 'required|between:0.00, 99999999.99',
            'products.*'                            => new CanOrderProductVariantRule((int) $this->request->get('billing_info.status'))
        ];
    }
}