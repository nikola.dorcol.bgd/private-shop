<?php

namespace App\Src\Orders\Requests;

use App\Src\Base\AbstractFormRequest;
use App\Src\Orders\Order;
use App\Src\Orders\ProductVariants\Rules\CheckProductVariantBelongsToOrder;
use App\Src\Orders\ProductVariants\Rules\CheckProductVariantInStock;
use Illuminate\Validation\Rule;

class FinalizeOrderRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'status'                => Rule::in(Order::FINAL_STATUSES),
            'admin_comment'         => 'present|string',

            'cart'                  => 'required|array',
            'cart.*.id'             => [
                'required',
                'exists:orders_product_variants,id',
                new CheckProductVariantBelongsToOrder($this->route('order'))
            ],
            'cart.*.warehouse_id'   => [
                'required',
                'exists:warehouses,id'
            ],
            'cart.*' => $this->request->get('status') == Order::STATUS_ACCEPTED ? new CheckProductVariantInStock() : ''
        ];
    }
}