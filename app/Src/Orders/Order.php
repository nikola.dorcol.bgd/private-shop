<?php

namespace App\Src\Orders;

use App\Src\Orders\ProductVariants\OrderProductVariant;
use App\Src\Products\Variants\ProductVariant;
use App\Src\Users\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Nicolaslopezj\Searchable\SearchableTrait;

class Order extends Model
{
    use SearchableTrait;

    const STATUS_PENDING = 0;

    const STATUS_ACCEPTED = 1;

    const STATUS_DECLINED = 2;

    const STATUSES = [
        self::STATUS_PENDING,
        self::STATUS_ACCEPTED,
        self::STATUS_DECLINED
    ];

    const FINAL_STATUSES = [
        self::STATUS_ACCEPTED,
        self::STATUS_DECLINED
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'admin_id',
        'label',
        'total',
        'final',
        'status',
        'admin_comment',
        'user_comment',
        'billing_name',
        'billing_phone_number',
        'billing_address',
        'billing_city',
        'billing_state',
        'billing_zip',
        'billing_floor',
        'billing_flat',
        'billing_intercom',
        'delivery_company'
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'orders.billing_name'           => 10,
            'orders.billing_phone_number'   => 10,
            'orders.billing_address'        => 10,
            'orders.billing_city'           => 10,
            'orders.billing_state'          => 10,
            'orders.billing_zip'            => 10,
            'orders.status'                 => 5
        ]
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'final' => 'bool'
    ];


    /**
     * Avoid null insert into db.
     *
     * @param null|string $comment
     */
    public function setAdminCommentAttribute(?string $comment)
    {
        $this->attributes['admin_comment'] = $comment ? $comment : '';
    }


    /**
     * Avoid null insert into db.
     *
     * @param null|string $comment
     */
    public function setUserCommentAttribute(?string $comment)
    {
        $this->attributes['user_comment'] = $comment ? $comment : '';
    }


    /**
     * Scope query to include only final orders.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeFinal(Builder $query): Builder
    {
        return $query->where('final', 1);
    }


    /**
     * Get all product variants that belongs to order.
     *
     * @return BelongsToMany
     */
    public function productVariants(): BelongsToMany
    {
        return $this->belongsToMany(ProductVariant::class, 'orders_product_variants')
            ->withPivot('quantity', 'price');
    }


    /**
     * Get customer that owns order.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    /**
     * Get all product variants that belongs to order.
     *
     * @return HasMany
     */
    public function orderProductVariant(): HasMany
    {
        return $this->hasMany(OrderProductVariant::class, 'order_id');
    }
}
