<?php

namespace App\Src\Orders\Events;

use App\Src\Orders\Order;

class OrderCreatedEvent
{
    /**
     * @var Order
     */
    public $order;


    /**
     * OrderCreatedEvent constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}