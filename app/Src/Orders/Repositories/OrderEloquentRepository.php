<?php

namespace App\Src\Orders\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Base\Contracts\DataTableRepositoryInterface;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use App\Src\Orders\Order;
use App\Src\Orders\Queries\EloquentRepositoryQueries\OrderDataTableQuery;
use App\Src\Products\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class OrderEloquentRepository extends AbstractRepository implements OrderRepositoryInterface, DataTableRepositoryInterface
{

    public function getModelClass(): string
    {
        return Order::class;
    }


    public function getProductOrders(Product $product): Collection
    {
        $query = $this->model->whereHas('productVariants', function ($q) use ($product) {

            $q->where('product_id', $product->id);
        });

        return $this->get($query);
    }


    public function dataTable(array $data): LengthAwarePaginator
    {
        return $this->paginate((new OrderDataTableQuery())->build($this->model, $data), $data['perPage']);
    }
}