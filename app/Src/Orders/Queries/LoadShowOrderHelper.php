<?php

namespace App\Src\Orders\Queries;

class LoadShowOrderHelper
{
    /**
     * Helper load array for eadger load all required entities for edit order.
     *
     * @return array
     */
    static function loadArray()
    {
        return [

            'user',

            'orderProductVariant' => function ($q) {

                $q->with([

                    'productVariant' => function ($q) {

                        $q->with([
                            'product',
                            'warehouseInventories' => function ($q) {

                                $q->where('quantity', '>', 0)
                                    ->with('warehouse');
                            },
                            'productAttributeValues.attribute',
                            'productAttributeValues.attributeValue',
                        ]);
                    }
                ]);
            }
        ];
    }
}