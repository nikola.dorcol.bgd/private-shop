<?php

namespace App\Src\Orders\Queries;

class LoadEditOrderHelper
{
    /**
     * Helper load array for eadger load all required entities for edit order.
     *
     * @return array
     */
    static function loadArray()
    {
        return [
            'user',

            'orderProductVariant' => function ($q) {

                $q->with([

                    'productVariant' => function ($q) {

                        $q->with([

                            'productAttributeValues',

                            'product' => function ($q) {

                                $q->with([
                                    'variants' => function ($q) {

                                        $q->with(['warehouseInventories' => function ($q) {

                                            $q->where('quantity', '>', 0)
                                                ->with('warehouse');
                                        }]);
                                    },

                                    'variants.productAttributeValues.attributeValue.productAttribute'
                                ]);
                            }
                        ]);
                    }
                ]);
            }
        ];
    }
}