<?php

namespace App\Src\Orders\Queries\EloquentRepositoryQueries;

use App\Src\Base\Contracts\QueryInterface;
use App\Src\Traits\ParametersValidityTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class OrderDataTableQuery implements QueryInterface
{
    use ParametersValidityTrait;

    public function build(Model $order, array $data): Builder
    {
        $query = $order->query()->with('user');

        /**
         * Method from trait.
         */
        if ($this->checkIsParameterValid($data, 'billing')) {

            $query->search($data['billing']);
        }

        if ($this->checkIsParameterValid($data, 'final')) {

            $query->where('final', $data['final']);
        }

        /**
         * Check only for sortParam because in DataTableRequest sortType is required if there is sortParam.
         */
        if ($this->checkIsParameterValid($data, 'sortParam')) {

            $query->orderBy($data['sortParam'], $data['sortType']);

        } else {

            $query->orderBy('status', 'ASC');
            $query->orderBy('created_at', 'DESC');
        }

        return $query;
    }
}