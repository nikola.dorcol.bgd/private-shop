<?php

namespace App\Src\Orders\ProductVariants\Resources;

use App\Src\Users\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ShowOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [

            'customer'  => new UserResource($this->user),

            'billing'   => [
                'shipping_user'         => new UserResource($this->user),
                'billing_name'          => $this->billing_name,
                'billing_phone_number'  => $this->billing_phone_number,
                'billing_address'       => $this->billing_address,
                'billing_city'          => $this->billing_city,
                'billing_state'         => $this->billing_state,
                'billing_zip'           => $this->billing_zip,
                'billing_floor'         => $this->billing_floor,
                'billing_flat'          => $this->billing_flat,
                'billing_intercom'      => $this->billing_intercom,
            ],

            'cart'      => [
                'total'                 => $this->total,
                'products'              => ShowOrderProductVariantResource::collection($this->orderProductVariant)
            ],

            'final'     => [
                'final'                 => $this->final,
                'admin_comment'         => $this->admin_comment,
                'status'                => $this->status
            ]
        ];
    }
}