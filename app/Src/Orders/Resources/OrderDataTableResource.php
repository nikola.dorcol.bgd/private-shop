<?php

namespace App\Src\Orders\Resources;

use App\Src\Brands\Resources\BrandResource;
use App\Src\Users\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDataTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'            => $this->id,
            'user'          => new UserResource($this->user),
            'total'         => $this->total,
            'status'        => $this->status,
            'final'         => $this->final,
            'created_at'    => $this->created_at
        ];
    }
}