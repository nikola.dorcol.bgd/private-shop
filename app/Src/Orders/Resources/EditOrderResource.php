<?php

namespace App\Src\Orders\Resources;

use App\Src\Orders\ProductVariants\Resources\EditOrderProductVariantResource;
use App\Src\Users\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EditOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'billing_info' => [
                'id'                    => $this->id,
                'user_id'               => $this->user_id,
                'user'                  => new UserResource($this->user),
                'total'                 => $this->total,
                'status'                => $this->status,
                'final'                 => $this->final,
                'admin_comment'         => $this->admin_comment,
                'user_comment'          => $this->user_comment,
                'billing_name'          => $this->billing_name,
                'billing_phone_number'  => $this->billing_phone_number,
                'billing_address'       => $this->billing_address,
                'billing_city'          => $this->billing_city,
                'billing_state'         => $this->billing_state,
                'billing_zip'           => $this->billing_zip,
                'billing_floor'         => $this->billing_floor,
                'billing_flat'          => $this->billing_flat,
                'billing_intercom'      => $this->billing_intercom,
            ],
            'products'      => EditOrderProductVariantResource::collection($this->orderProductVariant)
        ];
    }
}