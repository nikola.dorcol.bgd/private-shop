<?php

namespace App\Src\Products\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Base\Contracts\AutocompleteRepositoryInterface;
use App\Src\Base\Contracts\DataTableRepositoryInterface;
use App\Src\Categories\Category;
use App\Src\Orders\Order;
use App\Src\Products\Contracts\ProductRepositoryInterface;
use App\Src\Products\Product;
use App\Src\Products\Queries\EloquentRepositoryQueries\AvailableProductsQuery;
use App\Src\Products\Queries\EloquentRepositoryQueries\ProductDataTableQuery;
use App\Src\Products\Queries\EloquentRepositoryQueries\ProductWarehouseInventoryDataTableQuery;
use App\Src\Traits\ParametersValidityTrait;
use App\Src\Traits\RepositoryAutocompleteTrait;
use App\Src\Warehouses\Warehouse;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class ProductEloquentRepository extends AbstractRepository implements ProductRepositoryInterface, AutocompleteRepositoryInterface, DataTableRepositoryInterface
{
    use RepositoryAutocompleteTrait, ParametersValidityTrait;

    public function getModelClass(): string
    {
        return Product::class;
    }


    public function dataTable(array $data): LengthAwarePaginator
    {
        return $this->paginate((new ProductDataTableQuery())->build($this->model, $data), $data['perPage']);
    }


    public function searchAvailableProducts(string $query, int $orderStatus): Collection
    {
        if ($orderStatus == Order::STATUS_ACCEPTED) {

            return $this->get((new AvailableProductsQuery())->build($this->model, [])->search($query));

        } else {

            return $this->autocomplete($query, [
                'variants.productAttributeValues.attributeValue.productAttribute',
                'variants.warehouseInventories.warehouse'
            ]);
        }
    }


    public function getProductInventory(Warehouse $warehouse, array $data): LengthAwarePaginator
    {
        $data['warehouse'] = $warehouse;

        return $this->paginate((new ProductWarehouseInventoryDataTableQuery())->build($this->model, $data), $data['perPage']);
    }


    public function getRecommendedProducts(): Collection
    {
        $query = $this->model->has('recommended')
            ->with('cheapestVariant')
            ->orderByRaw('(SELECT position FROM recommended_products WHERE product_id = products.id) ASC');

        return $this->get($query, ['*'], true, ['recommended_products', 'products']);
    }


    public function getLatestProducts(): Collection
    {
        $query = $this->model->with('cheapestVariant')
            ->orderBy('created_at', 'DESC')
            ->take(7);

        return $this->get($query, ['*'], true, ['products']);
    }


    public function searchProducts(array $data, ?Category $category): LengthAwarePaginator
    {
        $query = $this->model->whereHas('variants', function ($q) {

            $q->whereHas('warehouseInventories', function ($q) {

                $q->where('quantity', '>' , 0);
            });
        })
        ->with('images', 'cheapestVariant');

        if ($this->checkIsParameterValid($data, 'term')) {

            $query->search($data['term']);
        }

        if ($this->checkIsParameterValid($data, 'price')) {

            $query->whereHas('variants', function ($q) use ($data) {

                $q->where('price', '>=', $data['price']);
            });
        }

        if ($this->checkIsParameterValid($data, 'brands')) {

            $query->whereIn('brand_id', explode(',', $data['brands']));
        }

        if ($this->checkIsParameterValid($data, 'subCategories')) {

            $query->whereHas('categories', function ($q) use ($data) {

                $q->whereIn('categories.id', explode(',', $data['subCategories']));
            });

        } elseif ($category) {

            $query->whereHas('categories', function ($q) use ($category) {

                $q->whereIn('categories.id', array_merge([$category->id], $category->children->pluck('id')->toArray()));
            });
        }

        $order  = $this->checkIsParameterValid($data, 'order') ? $data['order'] : 'created_at';
        $dir    = $this->checkIsParameterValid($data, 'dir') ? $data['dir'] : 'desc';

        if ($order) {

            $query->orderByRaw(DB::raw("(SELECT price FROM product_variants WHERE product_variants.product_id = products.id ORDER BY price ASC LIMIT 1 OFFSET 0) {$dir}"));

        } else {

            $query->orderBy($order, $dir);
        }

        return $this->paginate($query, 18);
    }
}