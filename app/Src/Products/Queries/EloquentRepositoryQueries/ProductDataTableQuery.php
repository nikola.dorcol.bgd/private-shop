<?php

namespace App\Src\Products\Queries\EloquentRepositoryQueries;

use App\Src\Base\Contracts\QueryInterface;
use App\Src\Traits\ParametersValidityTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProductDataTableQuery implements QueryInterface
{
    use ParametersValidityTrait;


    public function build(Model $product, array $data): Builder
    {
        $query = $product->query()->with('brand', 'images');

        /**
         * Method from trait.
         */
        if ($this->checkIsParameterValid($data, 'name')) {

            $query->search($data['name']);
        }

        if ($this->checkIsParameterValid($data, 'brand')) {

            $query->where('brand_id', $data['brand']);
        }

        if ($this->checkIsParameterValid($data, 'category')) {

            $query->whereHas('categories', function (Builder $q) use ($data) {

                $q->where('categories.id', $data['category']);
            });
        }
        
        if ($this->checkIsParameterValid($data, 'active')) {
            
            $query->where('active', $data['active']);
        }

        /**
         * Check only for sortParam because in DataTableRequest sortType is required if there is sortParam.
         */
        if ($this->checkIsParameterValid($data, 'sortParam')) {

            $query->orderBy($data['sortParam'], $data['sortType']);
        }

        return $query;
    }
}