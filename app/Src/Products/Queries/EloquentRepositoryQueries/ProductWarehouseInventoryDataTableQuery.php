<?php

namespace App\Src\Products\Queries\EloquentRepositoryQueries;

use App\Src\Base\Contracts\QueryInterface;
use App\Src\Traits\ParametersValidityTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProductWarehouseInventoryDataTableQuery implements QueryInterface
{
    use ParametersValidityTrait;


    /**
     * $data must have 'warehouse'.
     *
     * @param Model $product
     * @param array $data
     * @return Builder
     */
    public function build(Model $product, array $data): Builder
    {
        $warehouse = $data['warehouse'];

        $query = $product->query()->whereHas('variants', function ($q) use ($warehouse) {

            $q->whereHas('warehouseInventories', function ($q) use ($warehouse) {

                $q->where('warehouse_id', $warehouse->id);
            });
        })

        ->with([

            'brand',
            'images',
            'variants' => function ($q) use ($warehouse) {

                $q->with([
                        'productAttributeValues.attributeValue',
                        'productAttributeValues.attribute',
                        'warehouseInventories' => function ($q) use ($warehouse) {

                            $q->where('warehouse_id', $warehouse->id);
                        }
                    ])
                    ->whereHas('warehouseInventories', function ($q) use ($warehouse) {

                        $q->where('warehouse_id', $warehouse->id);
                    });
            }
        ]);

        /**
         * Method from trait.
         */
        if ($this->checkIsParameterValid($data, 'name')) {

            $query->search($data['name']);
        }

//        if ($this->checkIsParameterValid($data, 'brand')) {
//
//            $query->where('brand_id', $data['brand']);
//        }
//
//        if ($this->checkIsParameterValid($data, 'category')) {
//
//            $query->whereHas('categories', function (Builder $q) use ($data) {
//
//                $q->where('categories.id', $data['category']);
//            });
//        }

        if ($this->checkIsParameterValid($data, 'active')) {

            $query->where('active', $data['active']);
        }

        /**
         * Check only for sortParam because in DataTableRequest sortType is required if there is sortParam.
         */
        if ($this->checkIsParameterValid($data, 'sortParam')) {

            $query->orderBy($data['sortParam'], $data['sortType']);
        }

        return $query;
    }
}