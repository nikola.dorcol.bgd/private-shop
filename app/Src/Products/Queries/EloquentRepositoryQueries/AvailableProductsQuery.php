<?php

namespace App\Src\Products\Queries\EloquentRepositoryQueries;

use App\Src\Base\Contracts\QueryInterface;
use App\Src\Traits\ParametersValidityTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AvailableProductsQuery implements QueryInterface
{
    use ParametersValidityTrait;

    /**
     * Get all products with variants where has quantity in any warehouse.
     *
     * @param Model $product
     * @param array $data
     * @return Builder
     */
    public function build(Model $product, array $data): Builder
    {
        return $product->query()
            ->whereHas('variants', function ($q) {

                $q->whereHas('warehouseInventories', function ($q) {

                    $q->where('quantity', '>' , 0);
                });
            })
            ->with([
                'images',
                'variants.productAttributeValues.attributeValue.productAttribute',
                'variants' => function ($q) {

                    $q->with(['warehouseInventories' => function ($q) {

                        $q->where('quantity', '>', 0)
                            ->with('warehouse');
                    }]);
                }
            ]);
    }
}