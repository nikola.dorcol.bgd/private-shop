<?php

namespace App\Src\Products;

use App\Src\Attributes\Attribute;
use App\Src\Brands\Brand;
use App\Src\Categories\Category;
use App\Src\Images\Image;
use App\Src\Products\AttributeValue\ProductAttributeValue;
use App\Src\Products\Recommended\RecommendedProduct;
use App\Src\Products\Variants\ProductVariant;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model
{
    use SearchableTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id',
        'name',
        'slug',
        'unit',
        'description',
        'active',
        'price_note',
        'width',
        'height',
        'length',
        'weight',
        'thickness'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'main_image',
        'product_images'
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'products.name' => 10,
            'products.id'   => 5
        ]
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active'        => 'bool',
        'description'   => 'string'
    ];


    /**
     * Get main product image.
     *
     * @return Image|null
     */
    public function getMainImageAttribute(): ?Image
    {
        return $this->images()->mainProduct()->first();
    }


    /**
     * Get all product images.
     *
     * @return Collection
     */
    public function getProductImagesAttribute(): Collection
    {
        return $this->images()->product()->get();
    }


    /**
     * Scope query to include only active products.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('active', 1);
    }


    /**
     * Get brand that owns product.
     *
     * @return BelongsTo
     */
    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }


    /**
     * Get all of product images.
     *
     * @return MorphMany
     */
    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }


    /**
     * Get all categories that owns product.
     *
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'categories_products');
    }


    /**
     * Get all product variants.
     *
     * @return HasMany
     */
    public function variants(): HasMany
    {
        return $this->hasMany(ProductVariant::class, 'product_id');
    }


    /**
     * Get cheapest variant.
     *
     * @return HasOne
     */
    public function cheapestVariant(): HasOne
    {
        return $this->hasOne(ProductVariant::class, 'product_id')->orderBy('price', 'ASC');
    }


    /**
     * Get related products.
     *
     * @return BelongsToMany
     */
    public function relatedProducts(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'related_products', 'product_id', 'related_product_id');
    }


    /**
     * Get recommendation that owns product.
     *
     * @return HasOne
     */
    public function recommended(): HasOne
    {
        return $this->hasOne(RecommendedProduct::class, 'product_id');
    }
}