<?php

namespace App\Src\Products\Validations;

use App\Src\Base\Contracts\ValidationInterface;
use App\Src\Orders\Contracts\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyProductValidation implements ValidationInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;


    /**
     * DestroyProductValidation constructor.
     */
    public function __construct()
    {
        $this->orderRepository = app()->make(OrderRepositoryInterface::class);
    }


    public function validate(Model $product, ?array $data = null): bool
    {
        return $this->orderRepository->getProductOrders($product)->count() > 0;
    }
}