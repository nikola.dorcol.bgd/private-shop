<?php

namespace App\Src\Products\Validations;

use App\Src\Base\Contracts\ValidationInterface;
use App\Src\Products\Product;
use Illuminate\Database\Eloquent\Model;

class DestroyProductImageValidation implements ValidationInterface
{

    /**
     * Check if image belongs to product.
     *
     * @param Model $product
     * @param array|null|null $data
     * @return bool
     */
    public function validate(Model $product, ?array $data = null): bool
    {
        if (! isset($data['image'])) {

            return false;
        }

        $imageable = $data['image']->imageable;

        return $imageable instanceof Product && $imageable->id == $product->id;
    }
}