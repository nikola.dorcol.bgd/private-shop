<?php

namespace App\Src\Products\AttributeValue\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Products\AttributeValue\Contracts\ProductAttributeValueRepositoryInterface;
use App\Src\Products\AttributeValue\ProductAttributeValue;

class ProductAttributeValueEloquentRepository extends AbstractRepository implements ProductAttributeValueRepositoryInterface
{

    public function getModelClass(): string
    {
        return ProductAttributeValue::class;
    }
}