<?php

namespace App\Src\Products\AttributeValue;

use App\Src\Base\AbstractObserver;

class ProductAttributeValueObserver extends AbstractObserver
{
    /**
     * Handle the ProductAttributeValue "created" event.
     *
     * @param ProductAttributeValue $productAttributeValue
     * @return void
     */
    public function created(ProductAttributeValue $productAttributeValue): void
    {
        $this->clearCacheTags($productAttributeValue->getTable());
    }


    /**
     * Handle the ProductAttributeValue "updated" event.
     *
     * @param ProductAttributeValue $productAttributeValue
     * @return void
     */
    public function updated(ProductAttributeValue $productAttributeValue)
    {
        $this->clearCacheTags($productAttributeValue->getTable());
    }


    /**
     * Handle the ProductAttributeValue "deleted" event.
     *
     * @param ProductAttributeValue $productAttributeValue
     * @return void
     */
    public function deleted(ProductAttributeValue $productAttributeValue)
    {
        $this->clearCacheTags($productAttributeValue->getTable());
    }
}