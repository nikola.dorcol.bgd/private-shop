<?php

namespace App\Src\Products\Variants\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use App\Src\Products\Variants\ProductVariant;
use Illuminate\Database\Eloquent\Collection;

class ProductVariantEloquentRepository extends AbstractRepository implements ProductVariantRepositoryInterface
{

    public function getModelClass(): string
    {
        return ProductVariant::class;
    }


    public function findByIds(array $ids): Collection
    {
        $query = $this->model->with('productAttributeValues.attributeValue',
            'productAttributeValues.attribute',
            'product')
            ->whereIn('id', $ids);

        return $this->get($query);
    }


    public function findBySky(string $sku): ?ProductVariant
    {
        $query = $this->model->where('sku', $sku);

        return $this->first($query);
    }
}
