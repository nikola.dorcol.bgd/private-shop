<?php

namespace App\Src\Products\Variants\Contracts;

use App\Src\Products\Variants\ProductVariant;
use Illuminate\Database\Eloquent\Collection;

interface ProductVariantRepositoryInterface
{
    /**
     * Find product variants by id.
     *
     * @param array $ids
     * @return Collection
     */
    public function findByIds(array $ids): Collection;

    /**
     * Find product variant with given sku.
     *
     * @param string $sku
     * @return ProductVariant|null
     */
    public function findBySky(string $sku): ?ProductVariant;
}
