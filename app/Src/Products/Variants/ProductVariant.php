<?php

namespace App\Src\Products\Variants;

use App\Src\Carts\Cart;
use App\Src\Orders\ProductVariants\OrderProductVariant;
use App\Src\Products\AttributeValue\ProductAttributeValue;
use App\Src\Products\Product;
use App\Src\Warehouses\Inventory\WarehouseInventory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductVariant extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_variants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'sku',
        'price'
    ];


    /**
     * Get product that owns variant.
     *
     * @return BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id');
    }


    /**
     * Get all product attribute values that belong to product.
     *
     * @return HasMany
     */
    public function productAttributeValues(): HasMany
    {
        return $this->hasMany(ProductAttributeValue::class, 'product_variant_id');
    }


    /**
     * Get all carts that belongs to product variant.
     *
     * @return HasMany
     */
    public function carts(): HasMany
    {
        return $this->hasMany(Cart::class, 'product_variant_id');
    }


    /**
     * Get warehouse inventory that owns product variant.
     *
     * @return HasMany
     */
    public function warehouseInventories(): HasMany
    {
        return $this->hasMany(WarehouseInventory::class, 'product_variant_id');
    }


    /**
     * Get orders that belongs to product variant.
     *
     * @return HasMany
     */
    public function orderProductVariant(): HasMany
    {
        return $this->hasMany(OrderProductVariant::class, 'product_variant_id');
    }
}