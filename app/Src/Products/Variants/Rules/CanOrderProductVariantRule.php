<?php

namespace App\Src\Products\Variants\Rules;

use App\Src\Orders\Order;
use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class CanOrderProductVariantRule implements Rule
{
    /**
     * @var ProductVariantRepositoryInterface
     */
    private $productVariantRepository;

    /**
     * @var int
     */
    private $orderStatus;


    /**
     * Create a new rule instance.
     * @param int $orderStatus
     */
    public function __construct(int $orderStatus)
    {
        $this->productVariantRepository = app()->make(ProductVariantRepositoryInterface::class);
        $this->orderStatus              = $orderStatus;
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
    // @ symbol because no guarantee that all indexes are present.

        if (! $variant = $this->productVariantRepository->find(@$value['product_variant_id'])) {

            return false;
        }

        if ($this->orderStatus == Order::STATUS_ACCEPTED) {

            $inventory = $variant->warehouseInventories->where('warehouse_id', @$value['warehouse_id'])->first();

            if (! $inventory || $inventory->quantity < $value['quantity']) {

                return false;
            }
        }

        return true;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Selected product is out of stock in selected warehouse.';
    }
}