<?php

namespace App\Src\Products\Variants\Rules;

use App\Src\Products\Variants\ProductVariant;
use Illuminate\Contracts\Validation\Rule;

class CanCreateProductVariantCartRule implements Rule
{
    /**
     * @var ProductVariant
     */
    public $productVariant;


    /**
     * CanCreateProductVariantCartRule constructor.
     * @param ProductVariant $productVariant
     */
    public function __construct(ProductVariant $productVariant)
    {
        $this->productVariant = $productVariant;
    }


    /**
     * Validate if variant can be ordered.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->productVariant->load([
            'warehouseInventories' => function ($q) use ($value) {

                $q->whereHas('warehouse', function ($q) {

                        $q->public();
                    })
                    ->where('quantity', '>=', $value);
            }
        ]);

        return $this->productVariant->warehouseInventories->count() > 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return 'Product variant out of stock';
    }
}