<?php

namespace App\Src\Products\Variants;

use App\Src\Base\AbstractObserver;

class ProductVariantObserver extends AbstractObserver
{
    /**
     * Handle the ProductVariant "created" event.
     *
     * @param ProductVariant $productVariant
     * @return void
     */
    public function created(ProductVariant $productVariant): void
    {
        $this->clearCacheTags($productVariant->getTable());
    }


    /**
     * Handle the ProductVariant "updated" event.
     *
     * @param ProductVariant $productVariant
     * @return void
     */
    public function updated(ProductVariant $productVariant)
    {
        $this->clearCacheTags($productVariant->getTable());
    }


    /**
     * Handle the ProductVariant "deleted" event.
     *
     * @param ProductVariant $productVariant
     * @return void
     */
    public function deleted(ProductVariant $productVariant)
    {
        $this->clearCacheTags($productVariant->getTable());
    }
}