<?php

namespace App\Src\Products\Resources;

use App\Src\Brands\Resources\BrandResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductDataTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'brand'         => new BrandResource($this->brand),
            'name'          => $this->name,
            'active'        => $this->active,
            'main_image_src'=> $this->main_image->src,
            'created_at'    => $this->created_at,
            'variants'      => $this->whenLoaded('variants', $this->variants)
        ];
    }
}