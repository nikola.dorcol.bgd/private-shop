<?php

namespace App\Src\Products\Resources;

use App\Src\Brands\Resources\BrandResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductSearchResource extends ProductResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request) + ['cheapest_variant' => $this->cheapestVariant];
    }
}