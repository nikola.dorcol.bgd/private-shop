<?php

namespace App\Src\Products\Resources;

use App\Src\Brands\Resources\BrandResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductAutocompleteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'text'          => $this->name,
            'main_image'    => $this->main_image
        ];
    }
}