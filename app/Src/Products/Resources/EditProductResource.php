<?php

namespace App\Src\Products\Resources;

use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Attributes\Resources\AttributeResource;
use App\Src\Brands\Resources\BrandResource;
use App\Src\Categories\Resources\CategoryResource;
use App\Src\Images\Resources\ImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class EditProductResource extends JsonResource
{
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;


    /**
     * EditProductResource constructor.
     *
     * @param mixed $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);

        $this->attributeRepository = app()->make(AttributeRepositoryInterface::class);
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'base_info'         => [
                'id'            => $this->id,
                'brand_id'      => $this->brand_id,
                'main_image'    => $this->main_image,
                'name'          => $this->name,
                'slug'          => $this->slug,
                'unit'          => $this->unit,
                'description'   => $this->description,
                'active'        => $this->active,
                'created_at'    => $this->created_at,
                'updated_at'    => $this->updated_at,
                'price_note'    => $this->price_note,
                'width'         => $this->width,
                'height'        => $this->height,
                'length'        => $this->length,
                'weight'        => $this->weight,
                'thickness'     => $this->thickness
            ],
            'brand'             => new BrandResource($this->brand),
            'categories'        => CategoryResource::collection($this->categories),
            'images'            => ImageResource::collection($this->images),
            'related_products'  => ProductResource::collection($this->relatedProducts),
            // TODO do resource for variants
            'variants'          => $this->variants,
            'attributes'        => AttributeResource::collection($this->attributeRepository->getProductAttributes($this->resource))
        ];
    }
}