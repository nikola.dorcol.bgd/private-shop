<?php

namespace App\Src\Products\Resources;

use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Attributes\Resources\AttributeResource;
use App\Src\Brands\Resources\BrandResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductOrderAutocompleteResource extends JsonResource
{
    /**
     * @var AttributeRepositoryInterface
     */
    protected $attributeRepository;


    /**
     * EditProductResource constructor.
     *
     * @param mixed $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);

        $this->attributeRepository = app()->make(AttributeRepositoryInterface::class);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'brand'         => new BrandResource($this->brand),
            'main_image'    => $this->main_image,
            'name'          => $this->name,
            'text'          => $this->name,
            'description'   => $this->description,
            'variants'      => $this->variants,
            'attributes'    => AttributeResource::collection($this->attributeRepository->getProductAttributes($this->resource))
        ];
    }
}