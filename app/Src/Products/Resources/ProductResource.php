<?php

namespace App\Src\Products\Resources;

use App\Src\Brands\Resources\BrandResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'brand'         => new BrandResource($this->brand),
            'main_image'    => $this->main_image,
            'name'          => $this->name,
            'slug'          => $this->slug,
            'unit'          => $this->unit,
            'description'   => $this->description,
            'active'        => $this->active,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at
        ];
    }
}