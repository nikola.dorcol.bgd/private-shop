<?php

namespace App\Src\Products\Rules;

use App\Src\Products\Variants\Contracts\ProductVariantRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class EditVariantInitialRule implements Rule
{
    /**
     * @var ProductVariantRepositoryInterface
     */
    protected $productVariantRepository;


    /**
     * Create a new rule instance.
     */
    public function __construct()
    {
        $this->productVariantRepository = app()->make(ProductVariantRepositoryInterface::class);
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $variant
     * @return bool
     */
    public function passes($attribute, $variant)
    {
        if (! $variant['sku']) {

            return false;
        }

        if ($productVariant = $this->productVariantRepository->findBySky($variant['sku'])) {

            if (!isset($variant['id']) || (int)$variant['id'] !== $productVariant->id) {

                return false;
            }
        }

        return true;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute does not have unique sku';
    }
}
