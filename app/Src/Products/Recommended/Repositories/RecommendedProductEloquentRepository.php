<?php

namespace App\Src\Products\Recommended\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Products\Recommended\Contracts\RecommendedProductRepositoryInterface;
use App\Src\Products\Recommended\RecommendedProduct;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class RecommendedProductEloquentRepository extends AbstractRepository implements RecommendedProductRepositoryInterface
{
    function getModelClass(): string
    {
        return RecommendedProduct::class;
    }


    public function reorderAfterRemove(RecommendedProduct $recommendedProduct): bool
    {
        $query = $this->model->where('position', '>', $recommendedProduct->position);

        return $this->queryUpdate($query, ['position' => DB::raw('position - 1')], true, ['recommended_products']);
    }


    public function updatePosition(int $id, int $position): bool
    {
        $query = $this->model->where('product_id', $id);

        return $this->queryUpdate($query, ['position' => $position], true, ['recommended_products']);
    }
}