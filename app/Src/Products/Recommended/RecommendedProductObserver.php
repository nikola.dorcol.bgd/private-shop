<?php

namespace App\Src\Products\Recommended;

use App\Src\Base\AbstractObserver;

class RecommendedProductObserver extends AbstractObserver
{
    /**
     * Handle the Product "created" event.
     *
     * @param RecommendedProduct $recommendedProduct
     * @return void
     */
    public function created(RecommendedProduct $recommendedProduct): void
    {
        $this->clearCacheTags($recommendedProduct->getTable());
    }


    /**
     * Handle the Product "updated" event.
     *
     * @param RecommendedProduct $recommendedProduct
     * @return void
     */
    public function updated(RecommendedProduct $recommendedProduct)
    {
        $this->clearCacheTags($recommendedProduct->getTable());
    }


    /**
     * Handle the Product "deleted" event.
     *
     * @param RecommendedProduct $recommendedProduct
     * @return void
     */
    public function deleted(RecommendedProduct $recommendedProduct)
    {
        $this->clearCacheTags($recommendedProduct->getTable());
    }
}