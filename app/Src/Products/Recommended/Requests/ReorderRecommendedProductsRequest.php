<?php

namespace App\Src\Products\Recommended\Requests;

use App\Src\Base\AbstractFormRequest;

class ReorderRecommendedProductsRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'recommended_products'            => 'required|array',
            'recommended_products.*.id'       => 'required|exists:products,id',
            'recommended_products.*.position' => 'required|integer'
        ];
    }
}