<?php

namespace App\Src\Products\Recommended\Contracts;

use App\Src\Products\Recommended\RecommendedProduct;
use Illuminate\Database\Eloquent\Collection;

interface RecommendedProductRepositoryInterface
{
    /**
     * Update positions of products after one is removed.
     *
     * @param RecommendedProduct $recommendedProduct
     * @return bool
     */
    public function reorderAfterRemove(RecommendedProduct $recommendedProduct): bool;

    /**
     * Position update.
     *
     * @param int $id
     * @param int $position
     * @return bool
     */
    public function updatePosition(int $id, int $position): bool;
}