<?php

namespace App\Src\Products\Contracts;

use App\Src\Categories\Category;
use App\Src\Warehouses\Warehouse;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface ProductRepositoryInterface
{
    /**
     * Search for all available products.
     *
     * @param string $query
     * @param int $orderStatus
     * @return Collection
     */
    public function searchAvailableProducts(string $query, int $orderStatus): Collection;

    /**
     * Get product inventory for given warehouse.
     *
     * @param Warehouse $warehouse
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function getProductInventory(Warehouse $warehouse, array $data): LengthAwarePaginator;

    /**
     * Get recommended products.
     *
     * @return Collection
     */
    public function getRecommendedProducts(): Collection;

    /**
     * Get latest products
     *
     * @return Collection
     */
    public function getLatestProducts(): Collection;

    /**
     * Search products table.
     *
     * @param array $data
     * @param Category|null $category
     * @return LengthAwarePaginator
     */
    public function searchProducts(array $data, ?Category $category): LengthAwarePaginator;
}