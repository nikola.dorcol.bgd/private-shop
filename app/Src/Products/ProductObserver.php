<?php

namespace App\Src\Products;

use App\Src\Base\AbstractObserver;

class ProductObserver extends AbstractObserver
{
    /**
     * Handle the Product "created" event.
     *
     * @param Product $product
     * @return void
     */
    public function created(Product $product): void
    {
        $this->clearCacheTags($product->getTable());
    }


    /**
     * Handle the Product "updated" event.
     *
     * @param Product $product
     * @return void
     */
    public function updated(Product $product)
    {
        $this->clearCacheTags($product->getTable());
    }


    /**
     * Handle the Product "deleted" event.
     *
     * @param Product $product
     * @return void
     */
    public function deleted(Product $product)
    {
        $this->clearCacheTags($product->getTable());
    }
}