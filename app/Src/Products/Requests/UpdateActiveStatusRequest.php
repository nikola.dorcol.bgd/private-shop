<?php

namespace App\Src\Products\Requests;

use App\Src\Base\AbstractFormRequest;
use Illuminate\Validation\Rule;

class UpdateActiveStatusRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'active' => [
                'required',
                Rule::in([0, 1])
            ]
        ];
    }
}