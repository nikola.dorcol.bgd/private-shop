<?php

namespace App\Src\Products\Requests;

use App\Src\Base\AbstractFormRequest;
use Illuminate\Validation\Rule;

class StoreProductRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'base_info'                         => 'required',
            'base_info.name'                    => 'required',
            'base_info.description'             => 'present',
            'base_info.slug'                    => 'required',
            'base_info.brand_id'                => 'required|integer|exists:brands,id',
            'base_info.active'                  => [
                'required',
                Rule::in([0, 1])
            ],
            'base_info.price_note'              => 'present',
            'base_info.width'                   => 'present',
            'base_info.height'                  => 'present',
            'base_info.length'                  => 'present',
            'base_info.weight'                  => 'present',
            'base_info.thickness'               => 'present',

            'related_products'                  => 'sometimes|array',
            'related_products.*'                => 'exists:products,id',

            'categories'                        => 'required|array',
            'categories.*'                      => 'required|integer|exists:categories,id',

            'images'                            => 'required|array',
//            'image.*.description'               => 'required',
            'images.*.file'                     => 'required|file|mimes:jpeg,jpg,png|max:10000',
            'images.*.main'                     => [
                'required',
                Rule::in([0, 1])
            ],

            'variants'                          => 'required|array',
            'variants.*.attributes-values'      => 'required',
            'variants.*.sku'                    => 'required|distinct|unique:product_variants,sku',
            'variants.*.price'                  => 'required|numeric|min:0',
            'variants.*.warehouses'             => 'required|array',
            'variants.*.warehouses.*.id'        => 'required|integer|exists:warehouses,id',
            'variants.*.warehouses.*.quantity'  => 'required|integer|min:0',
        ];
    }
}
