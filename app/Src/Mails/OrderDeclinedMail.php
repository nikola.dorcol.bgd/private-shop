<?php

namespace App\Src\Mails;

use App\Src\Orders\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderDeclinedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    public $order;


    /**
     * Create a new message instance.
     */
    public function __construct(Order $order)
    {
        $this->order    = $order;
        $this->queue    = 'emails';
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Harc Fitness Porudžbina Odbijena')
            ->view('emails.client.order_declined');
    }
}
