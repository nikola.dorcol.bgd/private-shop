<?php

namespace App\Src\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var int
     */
    public $orderId;

    /**
     * If admin created.
     *
     * @var bool
     */
    public $admin;


    /**
     * Create a new message instance.
     * @param int $orderId
     * @param bool $admin
     */
    public function __construct(int $orderId, $admin = false)
    {
        $this->orderId  = $orderId;
        $this->admin    = $admin;
        $this->queue    = 'emails';
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Harc Fitness Potvrda Porudžbine')
            ->view('emails.client.order');
    }
}
