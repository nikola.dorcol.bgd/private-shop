<?php

namespace App\Src\Mails;

use App\Src\Orders\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderAcceptedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Order
     */
    public $order;


    /**
     * Create a new message instance.
     */
    public function __construct(Order $order)
    {
        $this->order    = $order;
        $this->queue    = 'emails';
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Harc Fitness Porudžbina Prihvaćena')
            ->view('emails.client.order_accepted');
    }
}
