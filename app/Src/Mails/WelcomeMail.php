<?php

namespace App\Src\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $verificationUrl;


    /**
     * Create a new message instance.
     *
     * @param string $name
     * @param string $verificationUrl
     */
    public function __construct(string $name, ?string $verificationUrl)
    {
        $this->name             = $name;
        $this->verificationUrl  = $verificationUrl;
        $this->queue            = 'emails';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'))
            ->subject('Dobro Došli')
            ->view('emails.client.welcome');
    }
}
