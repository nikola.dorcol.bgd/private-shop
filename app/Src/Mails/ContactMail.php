<?php

namespace App\Src\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    public $contact;


    /**
     * Create a new message instance.
     */
    public function __construct(array $contact)
    {
        $this->contact  = $contact;
        $this->queue    = 'emails';
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Harc Fitness Nova Poruka')
            ->replyTo($this->contact['email'])
            ->view('emails.client.contact');
    }
}
