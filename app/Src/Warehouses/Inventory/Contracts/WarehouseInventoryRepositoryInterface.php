<?php

namespace App\Src\Warehouses\Inventory\Contracts;

use App\Src\Warehouses\Inventory\WarehouseInventory;

interface WarehouseInventoryRepositoryInterface
{
    /**
     * Find product variant warehouse inventory.
     *
     * @param int $productVariantId
     * @param int $warehouseId
     * @return WarehouseInventory|null
     */
    public function getProductVariantWarehouseInventory(int $productVariantId, int $warehouseId): ?WarehouseInventory;
}