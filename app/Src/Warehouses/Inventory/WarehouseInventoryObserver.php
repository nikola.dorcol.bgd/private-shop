<?php

namespace App\Src\Warehouses\Inventory;

use App\Src\Base\AbstractObserver;

class WarehouseInventoryObserver extends AbstractObserver
{
    /**
     * Handle the Warehouse inventory "created" event.
     *
     * @param WarehouseInventory $warehouseInventory
     * @return void
     */
    public function created(WarehouseInventory $warehouseInventory): void
    {
        $this->clearCacheTags($warehouseInventory->getTable());
    }


    /**
     * Handle the Warehouse inventory "updated" event.
     *
     * @param WarehouseInventory $warehouseInventory
     * @return void
     */
    public function updated(WarehouseInventory $warehouseInventory)
    {
        $this->clearCacheTags($warehouseInventory->getTable());
    }


    /**
     * Handle the Warehouse inventory "deleted" event.
     *
     * @param WarehouseInventory $warehouseInventory
     * @return void
     */
    public function deleted(WarehouseInventory $warehouseInventory)
    {
        $this->clearCacheTags($warehouseInventory->getTable());
    }
}