<?php

namespace App\Src\Warehouses\Inventory\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Warehouses\Inventory\Contracts\WarehouseInventoryRepositoryInterface;
use App\Src\Warehouses\Inventory\WarehouseInventory;

class WarehouseInventoryEloquentRepository extends AbstractRepository implements WarehouseInventoryRepositoryInterface
{

    public function getModelClass(): string
    {
        return WarehouseInventory::class;
    }


    public function getProductVariantWarehouseInventory(int $productVariantId, int $warehouseId): ?WarehouseInventory
    {
        $query = $this->model->where('product_variant_id', $productVariantId)
            ->where('warehouse_id', $warehouseId);

        return $this->first($query);
    }
}