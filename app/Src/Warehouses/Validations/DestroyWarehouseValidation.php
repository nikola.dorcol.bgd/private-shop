<?php

namespace App\Src\Warehouses\Validations;

use App\Src\Base\Contracts\ValidationInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyWarehouseValidation implements ValidationInterface
{

    public function validate(Model $warehouse, ?array $data = null): bool
    {
        $warehouse->load('inventory', 'logsFrom', 'logsTo');

        return $warehouse->inventory->count() > 0 || $warehouse->logsFrom->count() > 0 || $warehouse->logsTo->count() > 0;
    }
}