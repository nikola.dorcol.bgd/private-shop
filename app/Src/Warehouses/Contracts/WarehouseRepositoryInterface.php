<?php

namespace App\Src\Warehouses\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface WarehouseRepositoryInterface
{
    /**
     * Get brands for data table.
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): LengthAwarePaginator;
}