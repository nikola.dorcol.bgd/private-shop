<?php

namespace App\Src\Warehouses\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WarehouseDataTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'public'        => $this->public,
            'value'         => $this->value,
            'quantity'      => $this->quantity,
            'created_at'    => $this->created_at
        ];
    }
}