<?php

namespace App\Src\Warehouses\Queries\EloquentRepositoryQueries;

use App\Src\Base\Contracts\QueryInterface;
use App\Src\Traits\ParametersValidityTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WarehouseDataTableQuery implements QueryInterface
{
    use ParametersValidityTrait;


    public function build(Model $attribute, array $data): Builder
    {
        $query = $attribute->query();

        /**
         * Method from trait.
         */
        if ($this->checkIsParameterValid($data, 'name')) {

            $query->search($data['name']);
        }

        if ($this->checkIsParameterValid($data, 'public')) {

            $query->where('public', $data['public']);
        }

        /**
         * Check only for sortParam because in DataTableRequest sortType is required if there is sortParam.
         */
        if ($this->checkIsParameterValid($data, 'sortParam')) {

            $query->orderBy($data['sortParam'], $data['sortType']);
        }

        return $query;
    }
}