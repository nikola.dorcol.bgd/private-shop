<?php

namespace App\Src\Warehouses\Requests;

use App\Src\Base\AbstractFormRequest;
use Illuminate\Validation\Rule;

class StoreWarehouseRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'          => 'required',
            'description'   => 'present',
            'address'       => 'required',
            'public'        => [
                'required',
                Rule::in([0, 1])
            ]
        ];
    }
}