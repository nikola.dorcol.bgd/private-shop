<?php

namespace App\Src\Warehouses\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Base\Contracts\DataTableRepositoryInterface;
use App\Src\Warehouses\Contracts\WarehouseRepositoryInterface;
use App\Src\Warehouses\Queries\EloquentRepositoryQueries\WarehouseDataTableQuery;
use App\Src\Warehouses\Warehouse;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class WarehouseEloquentRepository extends AbstractRepository implements WarehouseRepositoryInterface, DataTableRepositoryInterface
{

    public function getModelClass(): string
    {
        return Warehouse::class;
    }


    public function getAll(): LengthAwarePaginator
    {
        $query = $this->model->query();

        return $this->paginate($query);
    }


    public function dataTable(array $data): LengthAwarePaginator
    {
        return $this->paginate((new WarehouseDataTableQuery())->build($this->model, $data), $data['perPage']);
    }
}