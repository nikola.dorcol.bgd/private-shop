<?php

namespace App\Src\Warehouses\Log;

use App\Src\Users\User;
use App\Src\Warehouses\Warehouse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WarehouseLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'warehouse_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by',
        'warehouse_from',
        'warehouse_to',
        'product_variant_id',
        'value',
        'quantity',
        'comment'
    ];


    /**
     * Get user that created log.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }


    /**
     * Return warehouse that owns log.
     *
     * @return BelongsTo
     */
    public function from(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_from');
    }


    /**
     * Return warehouse that owns log.
     *
     * @return BelongsTo
     */
    public function to(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class, 'warehouse_to');
    }
}