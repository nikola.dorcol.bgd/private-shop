<?php

namespace App\Src\Warehouses\Log;

use App\Src\Base\AbstractObserver;

class WarehouseLogObserver extends AbstractObserver
{
    /**
     * Handle the Warehouse log "created" event.
     *
     * @param WarehouseLog $warehouseLog
     * @return void
     */
    public function created(WarehouseLog $warehouseLog): void
    {
        $this->clearCacheTags($warehouseLog->getTable());
    }


    /**
     * Handle the Warehouse log "updated" event.
     *
     * @param WarehouseLog $warehouseLog
     * @return void
     */
    public function updated(WarehouseLog $warehouseLog)
    {
        $this->clearCacheTags($warehouseLog->getTable());
    }


    /**
     * Handle the Warehouse log "deleted" event.
     *
     * @param WarehouseLog $warehouseLog
     * @return void
     */
    public function deleted(WarehouseLog $warehouseLog)
    {
        $this->clearCacheTags($warehouseLog->getTable());
    }
}