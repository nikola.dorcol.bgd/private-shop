<?php

namespace App\Src\Warehouses;

use App\Src\Warehouses\Inventory\WarehouseInventory;
use App\Src\Warehouses\Log\WarehouseLog;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Nicolaslopezj\Searchable\SearchableTrait;

class Warehouse extends Model
{
    use SearchableTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'warehouses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'address',
        'public',
        'value',
        'quantity'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'public' => 'booelan',
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'warehouses.name'       => 10,
            'warehouses.address'    => 10
        ]
    ];


    /**
     * Scope query to include only public warehouses.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopePublic(Builder $query): Builder
    {
        return $query->where('public', 1);
    }


    /**
     * Get warehouse inventory.
     *
     * @return HasMany
     */
    public function inventory()
    {
        return $this->hasMany(WarehouseInventory::class, 'warehouse_id');
    }


    /**
     * Get warehouse logs.
     *
     * @return HasMany
     */
    public function logsFrom(): HasMany
    {
        return $this->hasMany(WarehouseLog::class, 'warehouse_from');
    }


    /**
     * Get warehouse logs.
     *
     * @return HasMany
     */
    public function logsTo(): HasMany
    {
        return $this->hasMany(WarehouseLog::class, 'warehouse_to');
    }
}