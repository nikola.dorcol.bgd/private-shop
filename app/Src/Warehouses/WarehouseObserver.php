<?php

namespace App\Src\Warehouses;

use App\Src\Base\AbstractObserver;

class WarehouseObserver extends AbstractObserver
{
    /**
     * Handle the Warehouse "created" event.
     *
     * @param Warehouse $warehouse
     * @return void
     */
    public function created(Warehouse $warehouse): void
    {
        $this->clearCacheTags($warehouse->getTable());
    }


    /**
     * Handle the Warehouse "updated" event.
     *
     * @param Warehouse $warehouse
     * @return void
     */
    public function updated(Warehouse $warehouse)
    {
        $this->clearCacheTags($warehouse->getTable());
    }


    /**
     * Handle the Warehouse "deleted" event.
     *
     * @param Warehouse $warehouse
     * @return void
     */
    public function deleted(Warehouse $warehouse)
    {
        $this->clearCacheTags($warehouse->getTable());
    }
}