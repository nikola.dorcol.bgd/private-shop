<?php

namespace App\Src\Permissions;

use App\Src\Base\AbstractObserver;

class PermissionObserver extends AbstractObserver
{
    /**
     * Handle the Permission "created" event.
     *
     * @param Permission $permission
     * @return void
     */
    public function created(Permission $permission): void
    {
        $this->clearCacheTags($permission->getTable());
    }
}