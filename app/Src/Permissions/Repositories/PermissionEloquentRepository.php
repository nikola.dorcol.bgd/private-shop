<?php

namespace App\Src\Permissions\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Permissions\Contracts\PermissionRepositoryInterface;
use App\Src\Permissions\Permission;

class PermissionEloquentRepository extends AbstractRepository implements PermissionRepositoryInterface
{

    public function getModelClass(): string
    {
        return Permission::class;
    }
}