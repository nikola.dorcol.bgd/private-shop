<?php

namespace App\Src\Traits;

trait ParametersValidityTrait
{
    /**
     * Check request parameter validity.
     *
     * @param array $data
     * @param string $key
     * @return bool
     */
    public function checkIsParameterValid(array $data, string $key): bool
    {
        if (isset($data[$key])) {

            if ($data[$key] != '' && !is_null($data[$key])) {

                return true;
            }
        }

        return false;
    }
}