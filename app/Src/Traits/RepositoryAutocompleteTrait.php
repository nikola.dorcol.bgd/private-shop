<?php

namespace App\Src\Traits;

use Illuminate\Database\Eloquent\Collection;

trait RepositoryAutocompleteTrait
{
    /**
     * Autocomplete function for searching columns defined under searchable array in models.
     *
     * @param string $search
     * @param array|null $relations
     * @return Collection
     */
    public function autocomplete(string $search, ?array $relations = null): Collection
    {
        $query = $this->model->search($search);

        if ($relations) {

            $query->with($relations);
        }

        return $this->get($query);
    }
}