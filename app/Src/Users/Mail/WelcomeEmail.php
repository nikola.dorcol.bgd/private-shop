<?php

namespace App\Src\Users\Mail;

use App\Src\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    public $user;


    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;

    // If not sent from notification.
        $this->queue= 'emails';
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    // TODO ask for email subject
        return $this->from(config('mail.from.address'))
            ->to($this->user->email)
            ->subject('Dobro Došli')
            ->view('emails.client.welcome');
    }
}
