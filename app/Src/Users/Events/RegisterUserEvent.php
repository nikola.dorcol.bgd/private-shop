<?php

namespace App\Src\Users\Events;

class RegisterUserEvent
{
    /**
     * @var array
     */
    public $data;

    /**
     * @var string
     */
    public $registerType;


    /**
     * RegisterUserEvent constructor.
     *
     * @param array $userData
     * @param string $registerType
     */
    public function __construct(array $userData, string $registerType)
    {
        $this->data         = $userData;
        $this->registerType = $registerType;
    }
}