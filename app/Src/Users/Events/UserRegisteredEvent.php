<?php

namespace App\Src\Users\Events;

use App\Src\Users\User;

class UserRegisteredEvent
{

    /**
     * @var User
     */
    public $user;


    /**
     * UserRegisteredEvent constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}