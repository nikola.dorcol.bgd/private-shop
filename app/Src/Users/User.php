<?php

namespace App\Src\Users;

use App\Src\Carts\Cart;
use App\Src\Orders\Order;
use App\Src\Roles\Role;
use App\Src\Users\Oauth\Oauth;
use App\Src\Users\ShippingInformation\ShippingInformation;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable, SearchableTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'name',
        'email',
        'dob',
        'email_verified_at',
        'company_name',
        'phone_number',
        'password'
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'users.name'            => 10,
            'users.email'           => 10,
            'users.phone_number'    => 10,
            'users.company_name'    => 10
        ]
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'role.permissions'
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    /**
     * Automatically set bcrypted password.
     *
     * @param string $password
     * @return void
     */
    public function setPasswordAttribute(string $password): void
    {
        if (!empty($password)) {
            $this->attributes['password'] = bcrypt($password);
        }
    }


    /**
     * If null pass empty string.
     *
     * @param null|string $company
     */
    public function setCompanyNameAttribute(?string $company): void
    {
        $this->attributes['company_name'] = $company ? $company : '';
    }


    /**
     * Get oauth credentials for the user.
     *
     * @return HasMany
     */
    public function oauth(): HasMany
    {
        return $this->hasMany(Oauth::class, 'user_id');
    }


    /**
     * Get shipping information for the user.
     *
     * @return HasMany
     */
    public function shippingInformation(): HasMany
    {
        return $this->hasMany(ShippingInformation::class, 'user_id');
    }


    /**
     * Get role that user owns.
     *
     * @return HasOne
     */
    public function role(): HasOne
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }


    /**
     * Get user orders.
     *
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'user_id');
    }


    /**
     * Ge all user's carts.
     *
     * @return HasMany
     */
    public function carts(): HasMany
    {
        return $this->hasMany(Cart::class, 'user_id');
    }
}
