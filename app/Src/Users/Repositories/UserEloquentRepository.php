<?php

namespace App\Src\Users\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Base\Contracts\AutocompleteRepositoryInterface;
use App\Src\Base\Contracts\DataTableRepositoryInterface;
use App\Src\Products\Product;
use App\Src\Traits\RepositoryAutocompleteTrait;
use App\Src\Users\Contracts\UserRepositoryInterface;
use App\Src\Users\Queries\EloquentRepositoryQueries\UserDataTableQuery;
use App\Src\Users\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class UserEloquentRepository extends AbstractRepository implements UserRepositoryInterface, AutocompleteRepositoryInterface, DataTableRepositoryInterface
{
    use RepositoryAutocompleteTrait;

    function getModelClass(): string
    {
        return User::class;
    }


    public function dataTable(array $data): LengthAwarePaginator
    {
        return $this->paginate((new UserDataTableQuery())->build($this->model, $data), $data['perPage']);
    }


    public function getByProviderId(string $id, string $provider): ?User
    {
        $query = $this->model
            ->whereHas('oauth', function ($q) use($id, $provider) {
                $q->where('user_oauth.provider_user_id', $id)
                ->where('user_oauth.provider', $provider);
            });

        return $this->first($query);
    }


    public function getUsersThatHasProductInCart(Product $product): Collection
    {
        $query = $this->model->query()

            ->whereHas('carts', function ($q) use ($product) {

                $q->whereHas('productVariant', function ($q) use ($product) {

                    $q->whereHas('product', function ($q) use ($product) {

                        $q->where('id', $product->id);
                    });
                });
            })
            ->with([
                'carts' => function ($q) use ($product) {

                    $q->whereHas('productVariant', function ($q) use ($product) {

                        $q->whereHas('product', function ($q) use ($product) {

                            $q->where('id', $product->id);
                        });
                    });
                }
            ]);

        return $this->get($query);
    }
}