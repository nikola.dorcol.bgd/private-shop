<?php

namespace App\Src\Users\Requests;

use App\Src\Base\AbstractFormRequest;
use App\Src\Base\Rules\ValidatePhoneNumberRule;
use App\Src\Roles\Contracts\RoleRepositoryInterface;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @param RoleRepositoryInterface $roleRepository
     * @return array
     */
    public function rules(RoleRepositoryInterface $roleRepository): array
    {
        return [
            'name'          => 'required|min:2',
            'role_id'       => [
                'required',
                Rule::in($roleRepository->all()->pluck('id'))
            ],
            'email'         => 'required|email|unique:users,email,' . $this->route('user')->id,
            'dob'           => 'nullable|date_format:Y-m-d|before:' . date('Y-m-d'),
            'company_name'  => 'nullable',
            'phone_number'  => [
                'required',
                new ValidatePhoneNumberRule()
            ]
        ];
    }
}