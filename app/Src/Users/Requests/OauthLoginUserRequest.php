<?php

namespace App\Src\Users\Requests;

use App\Src\Base\AbstractFormRequest;

class OauthLoginUserRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code' => 'required'
        ];
    }
}