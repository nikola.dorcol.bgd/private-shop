<?php

namespace App\Src\Users\Requests;

use App\Src\Base\AbstractFormRequest;
use App\Src\Base\Rules\ValidatePhoneNumberRule;

class UserRegisterRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'          => 'required|min:2',
            'password'      => 'required|min:6|confirmed',
            'email'         => 'required|email|unique:users,email',
            'phone_number'  => [
                'required',
                new ValidatePhoneNumberRule()
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required'              => 'Polje je obavezno!',
            'password.confirmed'    => 'Lozinke se ne poklapaju!',
            'password.min'          => 'Lozinka mora biti minimum 6 karaktera',
            'email.unique'          => 'E-mail adresa je zauzeta!'
        ];
    }
}