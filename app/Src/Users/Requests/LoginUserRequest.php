<?php

namespace App\Src\Users\Requests;

use App\Src\Base\AbstractFormRequest;

class LoginUserRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'     => 'required|email',
            'password'  => 'required'
        ];
    }
}