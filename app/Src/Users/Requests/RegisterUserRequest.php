<?php

namespace App\Src\Users\Requests;

use App\Src\Base\AbstractFormRequest;
use App\Src\Base\Rules\ValidatePhoneNumberRule;

class RegisterUserRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'          => 'required|min:2',
            'password'      => 'required|min:6',
            'email'         => 'required|email|unique:users,email',
            'dob'           => 'present|date_format:Y-m-d',
            'company_name'  => 'present',
            'phone_number'  => [
                'present',
                new ValidatePhoneNumberRule()
            ]
        ];
    }


    public function attributes()
    {
        return [
            'first_name'    => 'first name',
            'last_name'     => 'last name',
            'company_name'  => 'company name',
            'phone_number'  => 'phone number'
        ];
    }
}