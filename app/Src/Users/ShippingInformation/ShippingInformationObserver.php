<?php

namespace App\Src\Users\ShippingInformation;

use App\Src\Base\AbstractObserver;

class ShippingInformationObserver extends AbstractObserver
{
    /**
     * Handle the ShippingInformation "created" event.
     *
     * @param ShippingInformation $shippingInfo
     * @return void
     */
    public function created(ShippingInformation $shippingInfo): void
    {
        $this->clearCacheTags($shippingInfo->getTable());
    }


    /**
     * Handle the ShippingInformation "updated" event.
     *
     * @param ShippingInformation $shippingInfo
     * @return void
     */
    public function updated(ShippingInformation $shippingInfo)
    {
        $this->clearCacheTags($shippingInfo->getTable());
    }


    /**
     * Handle the ShippingInformation "deleted" event.
     *
     * @param ShippingInformation $shippingInfo
     * @return void
     */
    public function deleted(ShippingInformation $shippingInfo)
    {
        $this->clearCacheTags($shippingInfo->getTable());
    }
}