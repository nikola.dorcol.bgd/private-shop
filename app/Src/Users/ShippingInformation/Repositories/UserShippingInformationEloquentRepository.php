<?php

namespace App\Src\Users\ShippingInformation\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Users\ShippingInformation\Contracts\UserShippingInformationRepositoryInterface;
use App\Src\Users\ShippingInformation\ShippingInformation;

class UserShippingInformationEloquentRepository extends AbstractRepository implements UserShippingInformationRepositoryInterface
{

    public function getModelClass(): string
    {
        return ShippingInformation::class;
    }
}