<?php

namespace App\Src\Users\Resources;

use App\Src\Roles\Resources\RoleResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserDataTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'email'         => $this->email,
            'phone_number'  => $this->phone_number,
            'created_at'    => $this->created_at,
            'role'          => $this->whenLoaded('role', new RoleResource($this->role))
        ];
    }
}