<?php

namespace App\Src\Users\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AutocompleteUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'dob'           => $this->dob,
            'email'         => $this->email,
            'company_name'  => $this->company_name,
            'phone_number'  => $this->phone_number,
            'text'          => "Name: {$this->name}, Email: {$this->email}"
        ];
    }
}