<?php

namespace App\Src\Users\Contracts;

use App\Src\Products\Product;
use App\Src\Users\User;
use Illuminate\Database\Eloquent\Collection;

interface UserRepositoryInterface
{
    /**
     * Get user record by provided provider id.
     *
     * @param string $id
     * @param string $provider
     * @return User|null
     */
    public function getByProviderId(string $id, string $provider): ?User;


    /**
     * Get all carts that belongs to user for given product.
     *
     * @param Product $product
     * @return Collection
     */
    public function getUsersThatHasProductInCart(Product $product): Collection;
}