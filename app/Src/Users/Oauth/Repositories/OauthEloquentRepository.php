<?php

namespace App\Src\Users\Oauth\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Users\Oauth\Contracts\OauthRepositoryInterface;
use App\Src\Users\Oauth\Oauth;

class OauthEloquentRepository extends AbstractRepository implements OauthRepositoryInterface
{

    public function getModelClass(): string
    {
        return Oauth::class;
    }
}