<?php

namespace App\Src\Users;

use App\Src\Base\AbstractObserver;

class UserObserver extends AbstractObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param User $user
     * @return void
     */
    public function created(User $user): void
    {
        $this->clearCacheTags($user->getTable());
    }


    /**
     * Handle the User "updated" event.
     *
     * @param User $user
     * @return void
     */
    public function updated(User $user)
    {
        $this->clearCacheTags($user->getTable());
    }


    /**
     * Handle the User "deleted" event.
     *
     * @param User $user
     * @return void
     */
    public function deleted(User $user)
    {
        $this->clearCacheTags($user->getTable());
    }
}