<?php

namespace App\Src\Users\Queries\EloquentRepositoryQueries;

use App\Src\Base\Contracts\QueryInterface;
use App\Src\Traits\ParametersValidityTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class UserDataTableQuery implements QueryInterface
{
    use ParametersValidityTrait;


    public function build(Model $user, array $data): Builder
    {
        $query = $user->query();

        /**
         * Method from trait.
         */
        if ($this->checkIsParameterValid($data, 'query')) {

            $query->search($data['query']);
        }

        if ($this->checkIsParameterValid($data, 'role')) {

            $query->where('role_id', $data['role']);
        }


        /**
         * Check only for sortParam because in DataTableRequest sortType is required if there is sortParam.
         */
        if ($this->checkIsParameterValid($data, 'sortParam')) {

            $query->orderBy($data['sortParam'], $data['sortType']);
        }

        return $query;
    }
}