<?php

namespace App\Src\Users\Validations;

use App\Src\Base\Contracts\ValidationInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyUserValidation implements ValidationInterface
{

    /**
     * Check if image belongs to product.
     *
     * @param Model $user
     * @param array|null|null $data
     * @return bool
     */
    public function validate(Model $user, ?array $data = null): bool
    {
        // TODO change when seeder is changed
        return $user->orders->count() > 0 || $user->email == 'harcstefan@gmail.com' ? false : true;
    }
}