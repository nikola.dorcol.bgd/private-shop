<?php

namespace App\Src\Users\Listeners;

use App\Src\Base\Errors;
use App\Src\Services\Contracts\RegisterUserInterface;
use App\Src\Users\Events\RegisterUserEvent;
use App\Src\Users\Exceptions\RegisterUserException;

class RegisterUserListener
{

    /**
     * @param RegisterUserEvent $event
     * @throws RegisterUserException
     * @return void
     */
    public function handle(RegisterUserEvent $event): void
    {
        $service = app()->make(RegisterUserInterface::class, ['type' => $event->registerType]);

        if (! $service->register($event->data)) {

            throw new RegisterUserException(Errors::USER_NOT_REGISTERED['error_message'], Errors::USER_NOT_REGISTERED['error_id']);
        }
    }
}