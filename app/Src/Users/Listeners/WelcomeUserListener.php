<?php

namespace App\Src\Users\Listeners;

use App\Src\Base\Exceptions\MailNotSentException;
use App\Src\Mails\WelcomeMail;
use App\Src\Users\Events\UserRegisteredEvent;
use Illuminate\Support\Facades\Mail;

class WelcomeUserListener
{
    /**
     * @param UserRegisteredEvent $event
     * @throws MailNotSentException
     */
    public function handle(UserRegisteredEvent $event): void
    {
        try {

            if ($event->user->email != '' && ! is_null($event->user->email)) {

                Mail::to($event->user->email)->queue(new WelcomeMail($event->user->name, null));
            }

        } catch (MailNotSentException $e) {

            throw new MailNotSentException($e->getMessage(), $e->getCode());
        }
    }
}