<?php

namespace App\Src\Roles;

use App\Src\Base\AbstractObserver;

class RoleObserver extends AbstractObserver
{
    /**
     * Handle the Role "created" event.
     *
     * @param Role $role
     * @return void
     */
    public function created(Role $role): void
    {
        $this->clearCacheTags($role->getTable());
    }
}