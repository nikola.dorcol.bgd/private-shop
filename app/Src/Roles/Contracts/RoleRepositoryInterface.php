<?php

namespace App\Src\Roles\Contracts;

use App\Src\Roles\Role;

interface RoleRepositoryInterface
{

    /**
     * Find role record by name with permissions as edger loading as optional.
     *
     * @param string $role
     * @param bool $permissions
     * @return Role|null
     */
    public function findByRoleName(string $role, bool $permissions = false): ?Role;
}