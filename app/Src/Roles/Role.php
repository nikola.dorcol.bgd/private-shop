<?php

namespace App\Src\Roles;

use App\Src\Permissions\Permission;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'deletable'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'deletable' => 'boolean',
    ];


    /**
     * Scope a query to specific role.
     *
     * @param Builder $query
     * @param string $role
     * @return Builder
     */
    public function scopeRole(Builder $query, string $role): Builder
    {
        return $query->where('name', $role);
    }


    /**
     * Permissions that belongs to roles.
     *
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class, 'permissions_roles', 'role_id', 'permission_id');
    }
}