<?php

namespace App\Src\Roles\Resources;

use App\Src\Permissions\Resources\PermissionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'permissions'   => $this->whenLoaded('permissions', PermissionResource::collection($this->permissions))
        ];
    }
}