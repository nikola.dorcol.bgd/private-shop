<?php

namespace App\Src\Roles\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Roles\Contracts\RoleRepositoryInterface;
use App\Src\Roles\Role;

class RoleEloquentRepository extends AbstractRepository implements RoleRepositoryInterface
{

    function getModelClass(): string
    {
        return Role::class;
    }


    public function findByRoleName(string $role, bool $permissions = false): ?Role
    {
        $query  = $this->model->role($role);
        $tags   = ['roles'];

        if ($permissions) {

            $query->with('permissions');

            array_push($tags, 'permissions');
        }

        return $this->first($query, ['*'], true, $tags);
    }
}