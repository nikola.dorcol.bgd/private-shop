<?php

namespace App\Src\Builders\Directors;

use App\Src\Builders\Contracts\ImageBuilderInterface;
use App\Src\Images\ImageObject;
use Illuminate\Http\UploadedFile;

class ImageDirector
{
    /**
     * @var ImageBuilderInterface
     */
    private $builder;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $resize;


    /**
     * ImageDirector constructor.
     *
     * @param ImageBuilderInterface $builder
     * @param UploadedFile $file
     * @param string $path
     * @param string $type
     * @param bool $resize
     */
    public function __construct(ImageBuilderInterface $builder, UploadedFile $file, string $path, string $type, bool $resize)
    {
        $this->builder  = $builder;
        $this->file     = $file;
        $this->path     = $path;
        $this->type     = $type;
        $this->resize   = $resize;
    }


    /**
     * Build instance of ImageObject via builder pattern.
     *
     * @return ImageObject
     */
    public function build(): ImageObject
    {
        if ($this->resize == true) {

            $this->builder->setImageConfig($this->type);
        }

        $this->builder->createImageObject();
        $this->builder->setFile($this->file);
        $this->builder->setExtension();
        $this->builder->setStorage();
        $this->builder->setPath($this->path);

        return $this->builder->getImageObject();
    }
}