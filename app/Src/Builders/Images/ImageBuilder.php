<?php

namespace App\Src\Builders\Images;

use App\Src\Base\Errors;
use App\Src\Builders\Contracts\ImageBuilderInterface;
use App\Src\Images\Exceptions\InvalidImageTypeException;
use App\Src\Images\ImageObject;
use Illuminate\Http\UploadedFile;

class ImageBuilder implements ImageBuilderInterface
{
    /**
     * @var ImageObject
     */
    protected $imageObject;

    /**
     * @var array
     */
    public $imageConfig = [];


    public function setImageConfig(string $imageType): void
    {
        $config = config('image');

        if (! in_array($imageType, $config['availableImageTypes'])) {

            throw new InvalidImageTypeException(Errors::INVALID_IMAGE_TYPE['error_message'], 400);
        }

        $this->imageConfig = $config[$imageType];
    }


    public function createImageObject(): void
    {
        $this->imageObject = app()->make(ImageObject::class);
    }


    public function setFile(UploadedFile $file): void
    {
        $this->imageObject->setFile($file);
    }


    public function setExtension(): void
    {
        $this->imageObject->setExtension($this->imageObject->getFile()->getClientOriginalExtension());
    }


    public function setPath(string $path): void
    {
        $this->imageObject->setPath($path);
    }


    public function setStorage(): void
    {
        $this->imageObject->setStorage(config('filesystems.default'));
    }


    public function getImageObject(): ImageObject
    {
        return $this->imageObject;
    }
}