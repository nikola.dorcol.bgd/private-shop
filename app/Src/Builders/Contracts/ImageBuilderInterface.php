<?php

namespace App\Src\Builders\Contracts;

use App\Src\Images\ImageObject;
use Illuminate\Http\UploadedFile;

interface ImageBuilderInterface
{
    /**
     * Set image config depending on image type.
     *
     * @param string $imageType
     */
    public function setImageConfig(string $imageType): void;

    /**
     * Create App\Src\Images\ImageObject instance
     */
    public function createImageObject(): void;

    /**
     * Set image file from request.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file): void;

    /**
     * Set image extension.
     */
    public function setExtension(): void;

    /**
     * Set image storage.
     */
    public function setStorage(): void;

    /**
     * Set path.
     *
     * @param string $path
     */
    public function setPath(string $path): void;

    /**
     * Get created image object.
     *
     * @return ImageObject
     */
    public function getImageObject(): ImageObject;
}