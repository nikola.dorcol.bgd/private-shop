<?php

namespace App\Src\Images\Contracts;

interface FilterInterface
{
    /**
     * Set filter attributes.
     *
     * @return void
     */
    public function setFilterAttributes(): void;

    /**
     * Set filter width attribute.
     *
     * @param int $width
     * @return void
     */
    public function setWidth(int $width): void;

    /**
     * Set filter height attribute.
     *
     * @param int $height
     * @return void
     */
    public function setHeight(int $height): void;

    /**
     * Set filter quality attribute.
     *
     * @param int $quality
     * @return void
     */
    public function setQuality(int $quality): void;

    /**
     * Set filter extension attribute.
     *
     * @param string $extension
     * @return void
     */
    public function setExtension(string $extension): void;
}