<?php

namespace App\Src\Images\Contracts;

interface ImageRepositoryInterface
{
    /**
     * When adding new image product main image this function will remove earlier.
     *
     * @param int $imageId
     * @param int $productId
     * @return bool
     */
    public function changeUnnecessaryProductMainTypes(int $imageId, int $productId): bool;
}