<?php

namespace App\Src\Images;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Image extends Model
{
    const TYPE_ICON = 1;

    const TYPE_LOGO = 2;

    const TYPE_PRODUCT = 3;

    const TYPE_PRODUCT_MAIN = 4;

    const TYPES = [
        self::TYPE_ICON,
        self::TYPE_LOGO,
        self::TYPE_PRODUCT,
        self::TYPE_PRODUCT_MAIN
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imageable_id',
        'imageable_type',
        'src',
        'type',
        'storage'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'imageable_id',
        'imageable_type',
        'storage'
    ];


    /**
     * Scope query to include only icon image type.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeIcon(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_ICON);
    }


    /**
     * Scope query to include only logo image type.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeLogo(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_LOGO);
    }


    /**
     * Scope query to include only product image type.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeProduct(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_PRODUCT);
    }


    /**
     * Scope query to include only main product image type.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeMainProduct(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_PRODUCT_MAIN);
    }


    /**
     * Get all of the owning imageable models.
     *
     * @return MorphTo
     */
    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }
}