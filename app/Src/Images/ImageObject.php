<?php

namespace App\Src\Images;

use Illuminate\Http\UploadedFile;

class ImageObject
{
    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var string
     */
    private $extension;

    /**
     * @var string
     */
    private $storage;

    /**
     * @var string
     */
    private $path;


    /**
     * Set file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file): void
    {
        $this->file = $file;
    }


    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile(): UploadedFile
    {
        return $this->file;
    }


    /**
     * Set extension.
     *
     * @param string $extension
     */
    public function setExtension(string $extension): void
    {
        $this->extension = $extension;
    }


    /**
     * Get extension.
     *
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }


    /**
     * Set storage.
     *
     * @param string $storage
     */
    public function setStorage(string $storage): void
    {
        $this->storage = $storage;
    }


    /**
     * Get storage.
     *
     * @return string
     */
    public function getStorage(): string
    {
        return $this->storage;
    }


    /**
     * Set path.
     *
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }


    /**
     * Get path.
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
}