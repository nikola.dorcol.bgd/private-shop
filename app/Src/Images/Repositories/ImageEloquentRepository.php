<?php

namespace App\Src\Images\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Images\Contracts\ImageRepositoryInterface;
use App\Src\Images\Image;
use App\Src\Products\Product;

class ImageEloquentRepository extends AbstractRepository implements ImageRepositoryInterface
{

    public function getModelClass(): string
    {
        return Image::class;
    }


    public function changeUnnecessaryProductMainTypes(int $imageId, int $productId): bool
    {
        $query = $this->model->where('id', '!=', $imageId)
            ->where('imageable_id', $productId)
            ->where('type', Image::TYPE_PRODUCT_MAIN);

        return $this->queryUpdate($query, ['type' => Image::TYPE_PRODUCT]);
    }
}