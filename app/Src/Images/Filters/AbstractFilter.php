<?php

namespace App\Src\Services\Images\Filters;

use App\Src\Base\Errors;
use App\Src\Images\Contracts\FilterInterface;
use Intervention\Image\Filters\FilterInterface as InterventionFilterInterface;
use App\Src\Images\Exceptions\InvalidImageTypeException;
use Intervention\Image\Image;

abstract class AbstractFilter implements FilterInterface, InterventionFilterInterface
{
    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @var int
     */
    protected $quality;

    /**
     * @var string
     */
    protected $extension;


    /**
     * AbstractFilter constructor.
     */
    public function __construct()
    {
        $this->setFilterAttributes();
    }


    /**
     * Get image configuration for given filter.
     *
     * @return array|null
     */
    abstract function getConfig(): ?array;


    public function applyFilter(Image $image): Image
    {
        return $image->fit($this->width, $this->height)
            ->encode($this->extension, $this->quality);
    }


    public function setFilterAttributes(): void
    {
        $config = $this->getConfig();

        if (! $config) {

            throw new InvalidImageTypeException(Errors::INVALID_IMAGE_TYPE['error_message'], 400);
        }

        $this->setWidth($config['width']);
        $this->setHeight($config['height']);
        $this->setQuality($config['quality']);
        $this->setExtension($config['extension']);
    }


    public function setWidth(int $width): void
    {
        $this->width = $width;
    }


    public function setHeight(int $height): void
    {
        $this->height = $height;
    }


    public function setQuality(int $quality): void
    {
        $this->quality = $quality;
    }


    public function setExtension(string $extension): void
    {
        $this->extension = $extension;
    }
}