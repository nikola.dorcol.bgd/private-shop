<?php

namespace App\Src\Services\Images\Filters;

class IconFilter extends AbstractFilter
{
    function getConfig(): ?array
    {
        return config('image.icon');
    }
}