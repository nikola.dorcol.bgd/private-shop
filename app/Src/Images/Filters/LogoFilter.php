<?php

namespace App\Src\Services\Images\Filters;

class LogoFilter extends AbstractFilter
{
    public function getConfig(): ?array
    {
        return config('image.logo');
    }
}