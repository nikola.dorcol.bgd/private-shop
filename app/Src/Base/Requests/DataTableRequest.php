<?php

namespace App\Src\Base\Requests;

use App\Src\Base\AbstractFormRequest;
use Illuminate\Validation\Rule;

/**
 * Base rules for every data table call.
 *
 * Class DataTableRequest
 * @package App\Src\Base\Requests
 */
class DataTableRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'page'      => 'required|min:1|integer',
            'perPage'   => 'required|min:1|integer',
            'sortParam' => 'nullable|string',
            'sortType'  => [
                'required_with:sortParam',
                Rule::in(['ASC', 'DESC'])
            ]
        ];
    }
}