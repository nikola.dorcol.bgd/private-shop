<?php

namespace App\Src\Base\Requests;

use App\Src\Base\AbstractFormRequest;

/**
 * Form request that should be injected in each controller's function for autocomplete.
 *
 * Class AutocompleteRequest
 * @package App\Src\Base\Requests
 */
class AutocompleteRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'query' => 'required|min:1'
        ];
    }
}