<?php

namespace App\Src\Base;

use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractFormRequest extends FormRequest
{
    /**
     *  Return if user is authorised to perform request.
     *
     * @return bool
     */
    public function authorised()
    {
        return true;
    }
}