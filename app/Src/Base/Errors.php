<?php

namespace App\Src\Base;

class Errors
{
    const GENERAL = [
        'error_id'      => 0,
        'error_code'    => 'general_error',
        'error_message' => 'Greška pri obradi zahteva'
    ];

    const USER_NOT_REGISTERED = [
        'error_id'      => 1,
        'error_code'    => 'user_note_registered',
        'error_message' => 'User could not be created!'
    ];

    const PROVIDER_BAD_REQUEST = [
        'error_id'      => 2,
        'error_code'    => 'provider_bad_request',
        'error_message' => 'Oauth bad request!'
    ];

    const USER_UNAUTHORISED = [
        'error_id'      => 3,
        'error_code'    => 'user_unauthorised',
        'error_message' => 'User unauthorised!'
    ];

    const MODEL_NOT_CREATED = [
        'error_id'      => 4,
        'error_code'    => 'model_not_created',
        'error_message' => 'Model not created!'
    ];

    const MODEL_NOT_UPDATED = [
        'error_id'      => 5,
        'error_code'    => 'model_not_updated',
        'error_message' => 'Model not updated!'
    ];

    const MODEL_NOT_DELETED = [
        'error_id'      => 6,
        'error_code'    => 'model_not_deleted',
        'error_message' => 'Model not deleted!'
    ];

    const MODEL_NOT_FOUND = [
        'error_id'      => 7,
        'error_code'    => 'model_not_found',
        'error_message' => 'Model not found!',
    ];

    const FILE_NOT_UPLOADED = [
        'error_id'      => 8,
        'error_code'    => 'file_not_uploaded',
        'error_message' => 'File not uploaded!'
    ];

    const FILE_NOT_DELETED = [
        'error_id'      => 9,
        'error_code'    => 'file_not_deleted',
        'error_message' => 'File not deleted!'
    ];

    const INVALID_IMAGE_TYPE = [
        'error_id'      => 10,
        'error_code'    => 'invalid_image_type',
        'error_message' => 'Invalid image type'
    ];

    const MODEL_HAS_RELATIONSHIP_DATA = [
        'error_id'      => 11,
        'error_code'    => 'model_has_relationship_data',
        'error_message' => 'Can not delete! All relationship data will be lost!'
    ];

    const MODELS_NOT_INSERTED = [
        'error_id'      => 12,
        'error_code'    => 'models_not_inserted',
        'error_message' => 'Models not inserted!'
    ];

    const MODEL_RELATION_NOT_SYNCED = [
        'error_id'      => 13,
        'error_code'    => 'model_relation_not_synced',
        'error_message' => 'Models pivot relation not synced!'
    ];

    const MISMATCH_DATA = [
        'error_id'      => 14,
        'error_code'    => 'mismatch_data',
        'error_message' => 'Some data matched incorrectly!'
    ];

    const MODELS_NOT_DELETED = [
        'error_id'      => 15,
        'error_code'    => 'models_not_deleted',
        'error_message' => 'Models not deleted!'
    ];

    const ORDER_IS_FINAL = [
        'error_id'      => 16,
        'error_code'    => 'ORDER_IS_FINAL',
        'error_message' => 'Can not update final orders!'
    ];

    const INVALID_ORDER_STATUS = [
        'error_id'      => 17,
        'error_code'    => 'INVALID_ORDER_STATUS',
        'error_message' => 'Invalid order status!'
    ];

    const MODElS_NOT_UPDATED = [
        'error_id'      => 18,
        'error_code'    => 'MODElS_NOT_UPDATED',
        'error_message' => 'Models not updated!'
    ];
}