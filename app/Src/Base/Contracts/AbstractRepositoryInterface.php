<?php

namespace App\Src\Base\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface AbstractRepositoryInterface
{
    /**
     * Create instance of Model.
     *
     * @return void
     */
    public function makeModel(): void;

    /**
     * Get Collection from db or cache.
     *
     * @param Builder $query
     * @param array $columns
     * @param bool $cache
     * @param array $tags
     * @return Collection
     */
    public function get(Builder $query, array $columns = ['*'], bool $cache = false, array $tags = []): Collection;

    /**
     * Get all resources without given query or relationship.
     *
     * @param array $columns
     * @param bool $cache
     * @param array $tags
     * @return Collection
     */
    public function all(array $columns = ['*'], bool $cache = false, array $tags = []): Collection;

    /**
     * Get paginated collection from db or cache.
     *
     * @param Builder $query
     * @param int $perPage
     * @param array $columns
     * @param bool $cache
     * @param array $tags
     * @return LengthAwarePaginator
     */
    public function paginate(Builder $query, int $perPage = 50, array $columns = ['*'], bool $cache = false, array $tags = []): LengthAwarePaginator;

    /**
     * Get first record for given query from db or cache.
     *
     * @param Builder $query
     * @param array $columns
     * @param bool $cache
     * @param array $tags
     * @return Model|null
     */
    public function first(Builder $query, array $columns = ['*'], bool $cache = false, array $tags = []): ?Model;

    /**
     * Find row in database by given id.
     *
     * @param int|null $id
     * @return Model|null
     */
    public function find(?int $id): ?Model;

    /**
     * Store new record to db.
     *
     * @param array $data
     * @return Model
     */
    public function store(array $data): Model;

    /**
     * Insert multiple records to db.
     *
     * @param array $data
     * @return bool
     */
    public function insert(array $data): bool;

    /**
     * Update row in db.
     *
     * @param Model $model
     * @param array $data
     * @return bool
     */
    public function update(Model $model, array $data): bool;

    /**
     * Update table for given query and data.
     *
     * @param Builder $query
     * @param array $data
     * @return bool
     */
    public function queryUpdate(Builder $query, array $data): bool;

    /**
     * Delete row from db.
     *
     * @param Model $model
     * @return bool
     */
    public function delete(Model $model): bool;


    /**
     * Delete multiple rows.
     *
     * @param array $ids
     * @return bool
     */
    public function destroy(array $ids): bool;
}