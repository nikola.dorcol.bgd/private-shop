<?php

namespace App\Src\Base\Contracts;

use Illuminate\Database\Eloquent\Model;

interface ValidationInterface
{
    /**
     * Validate if given model can be handled with additional data if need.
     *
     * @param Model $model
     * @param array|null|null $data
     * @return bool
     */
    public function validate(Model $model, ?array $data = null): bool;
}