<?php

namespace App\Src\Base\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface AutocompleteRepositoryInterface
{
    /**
     * Autocomplete function for searching columns defined under searchable array in models.
     *
     * @param string $search
     * @param array|null $relations
     * @return Collection
     */
    public function autocomplete(string $search, ?array $relations = null): Collection;
}