<?php

namespace App\Src\Base\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface DataTableRepositoryInterface
{
    /**
     * Get products for data table.
     *
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function dataTable(array $data): LengthAwarePaginator;
}