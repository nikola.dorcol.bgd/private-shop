<?php

namespace App\Src\Base\Contracts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

interface QueryInterface
{
    /**
     * Build eloquent query.
     *
     * @param Model $model
     * @param array $data
     * @return Builder
     */
    public function build(Model $model, array $data): Builder;
}