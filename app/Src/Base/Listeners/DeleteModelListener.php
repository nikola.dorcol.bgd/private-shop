<?php

namespace App\Src\Base\Listeners;

use App\Src\Base\Errors;
use App\Src\Base\Events\DestroyModelEvent;
use App\Src\Base\Exceptions\ModelNotDeletedException;

class DeleteModelListener
{
    /**
     * @param DestroyModelEvent $event
     * @throws ModelNotDeletedException
     * @return void
     */
    public function handle(DestroyModelEvent $event): void
    {
        if (! $event->service->destroy($event->model, $event->data)) {

            throw new ModelNotDeletedException(Errors::MODEL_NOT_DELETED['error_message'], 400);
        }
    }
}