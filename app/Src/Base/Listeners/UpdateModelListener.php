<?php

namespace App\Src\Base\Listeners;

use App\Src\Base\Errors;
use App\Src\Base\Events\EditModelEvent;
use App\Src\Base\Exceptions\ModelNotUpdatedException;

class UpdateModelListener
{
    /**
     * @param EditModelEvent $event
     * @throws ModelNotUpdatedException
     */
    public function handle(EditModelEvent $event): void
    {
        if (! $event->service->edit($event->model, $event->data)) {

            throw new ModelNotUpdatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }
    }
}