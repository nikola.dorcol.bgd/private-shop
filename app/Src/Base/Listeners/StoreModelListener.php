<?php

namespace App\Src\Base\Listeners;


use App\Src\Base\Errors;
use App\Src\Base\Events\CreateModelEvent;
use App\Src\Base\Exceptions\ModelNotCreatedException;

class StoreModelListener
{
    /**
     * @param CreateModelEvent $event
     * @throws ModelNotCreatedException
     * @return void
     */
    public function handle(CreateModelEvent $event): void
    {
        if (! $event->service->create($event->data)) {

            throw new ModelNotCreatedException(Errors::MODEL_NOT_CREATED['error_message'], 400);
        }
    }
}
