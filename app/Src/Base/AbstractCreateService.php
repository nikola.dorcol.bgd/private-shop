<?php

namespace App\Src\Base;

use App\Src\Services\Contracts\CreateServiceInterface;
use App\Src\Services\Contracts\StoreInterface;

abstract class AbstractCreateService implements CreateServiceInterface
{
    /**
     * @var StoreInterface
     */
    protected $storeService;


    /**
     * AbstractCreateService constructor.
     */
    public function __construct()
    {
        $this->storeService = app()->make(StoreInterface::class);
    }
}