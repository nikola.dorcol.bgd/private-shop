<?php

namespace App\Src\Base;

use App\Src\Services\Contracts\InsertInterface;
use App\Src\Services\Contracts\InsertServiceInterface;

abstract class AbstractInsertService implements InsertServiceInterface
{
    /**
     * @var InsertInterface
     */
    protected $insertService;


    /**
     * AbstractInsertService constructor.
     */
    public function __construct()
    {
        $this->insertService = app()->make(InsertInterface::class);
    }
}