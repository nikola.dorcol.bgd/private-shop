<?php

namespace App\Src\Base;

use App\Src\Base\Contracts\AbstractRepositoryInterface;
use App\Src\Services\Cache\RepositoryModelCache;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository implements AbstractRepositoryInterface
{

    /**
     * @var Model
     */
    public $model;


    /**
     * AbstractRepository constructor.
     */
    public function __construct()
    {
        $this->makeModel();
    }


    /**
     * Return full path to Model class.
     *
     * @return string
     */
    abstract function getModelClass(): string;


    public function makeModel(): void
    {
        $this->model = app()->make($this->getModelClass());
    }


    public function get(Builder $query, array $columns = ['*'], bool $cache = false, array $tags = []): Collection
    {
        if ($cache === true) {

            return app()->make(RepositoryModelCache::class)->get($query, $columns, $tags);
        }

        return $query->get($columns);
    }


    public function all(array $columns = ['*'], bool $cache = false, array $tags = []): Collection
    {
        return $this->get($this->model->query(), $columns, $cache, $tags);
    }


    public function paginate(Builder $query, int $perPage = 50, array $columns = ['*'], bool $cache = false, array $tags = []): LengthAwarePaginator
    {// TODO cache
        if ($cache === true) {

            return app()->make(RepositoryModelCache::class)->paginate($query, $perPage, $columns, $tags);
        }

        return $query->paginate($perPage, $columns);
    }


    public function first(Builder $query, array $columns = ['*'], bool $cache = false, array $tags = []): ?Model
    {
        return $this->get($query, $columns, $cache, $tags)->first();
    }


    public function find(?int $id, array $relations = []): ?Model
    {
        $query = $this->model->where('id', $id);

        if (! empty($relations)) {

            $query = $query->with($relations);
        }

        return $this->first($query);
    }


    public function store(array $data): Model
    {
        return $this->model->create($data);
    }


    public function insert(array $data): bool
    {
        return $this->model->insert($data);
    }


    public function update(Model $model, array $data): bool
    {
        return $model->update($data);
    }


    public function queryUpdate(Builder $query, array $data, bool $invalidate = false, array $tags = []): bool
    {
        if ($invalidate) {

            app()->make(RepositoryModelCache::class)->clearCache($tags);
        }

        return $query->update($data);
    }


    public function delete(Model $model): bool
    {
        return $model->delete();
    }


    public function destroy(array $ids): bool
    {
        return $this->model->destroy($ids);
    }
}