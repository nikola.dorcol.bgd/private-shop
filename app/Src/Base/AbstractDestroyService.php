<?php

namespace App\Src\Base;

use App\Src\Services\Contracts\DeleteInterface;
use App\Src\Services\Contracts\DestroyServiceInterface;

abstract class AbstractDestroyService implements DestroyServiceInterface
{
    /**
     * @var DeleteInterface
     */
    protected $deleteService;


    /**
     * AbstractDestroyService constructor.
     */
    public function __construct()
    {
        $this->deleteService = app()->make(DeleteInterface::class);
    }
}