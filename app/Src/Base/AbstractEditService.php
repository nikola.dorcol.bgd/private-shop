<?php

namespace App\Src\Base;

use App\Src\Services\Contracts\EditServiceInterface;
use App\Src\Services\Contracts\UpdateInterface;

abstract class AbstractEditService implements EditServiceInterface
{
    /**
     * @var UpdateInterface
     */
    protected $updateService;


    /**
     * AbstractEditService constructor.
     */
    public function __construct()
    {
        $this->updateService = app()->make(UpdateInterface::class);
    }
}