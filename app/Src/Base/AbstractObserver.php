<?php

namespace App\Src\Base;

use App\Src\Services\Cache\RepositoryModelCache;

abstract class AbstractObserver
{
    /**
     * Clear cache for given tags - usually db table name of Model.
     *
     * @param string $tags
     * @return void
     */
    protected function clearCacheTags(string $tags): void
    {
        app()->make(RepositoryModelCache::class)->clearCache([$tags]);
    }
}