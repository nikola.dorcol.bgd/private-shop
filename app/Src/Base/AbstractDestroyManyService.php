<?php

namespace App\Src\Base;

use App\Src\Services\Contracts\DeleteManyInterface;
use App\Src\Services\Contracts\DestroyManyServiceInterface;

abstract class AbstractDestroyManyService implements DestroyManyServiceInterface
{
    /**
     * @var DeleteManyInterface
     */
    protected $deleteManyService;


    /**
     * AbstractDestroyService constructor.
     */
    public function __construct()
    {
        $this->deleteManyService = app()->make(DeleteManyInterface::class);
    }
}