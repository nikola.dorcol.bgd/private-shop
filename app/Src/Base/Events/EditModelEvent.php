<?php

namespace App\Src\Base\Events;

use App\Src\Services\Contracts\EditServiceInterface;
use Illuminate\Database\Eloquent\Model;

class EditModelEvent
{
    /**
     * @var EditServiceInterface
     */
    public $service;

    /**
     * @var Model
     */
    public $model;

    /**
     * @var array
     */
    public $data;


    /**
     * EditModelEvent constructor.
     *
     * @param EditServiceInterface $service
     * @param Model $model
     * @param array $data
     */
    public function __construct(EditServiceInterface $service, Model $model, array $data)
    {
        $this->service  = $service;
        $this->model    = $model;
        $this->data     = $data;
    }
}