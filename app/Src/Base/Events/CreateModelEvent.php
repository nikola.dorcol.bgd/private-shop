<?php

namespace App\Src\Base\Events;

use App\Src\Services\Contracts\CreateServiceInterface;
use App\Src\Services\Contracts\StoreInterface;

class CreateModelEvent
{
    /**
     * @var StoreInterface
     */
    public $service;

    /**
     * @var array
     */
    public $data;


    /**
     * CreateModelEvent constructor.
     *
     * @param CreateServiceInterface $service
     * @param array $data
     */
    public function __construct(CreateServiceInterface $service, array $data)
    {
        $this->service  = $service;
        $this->data     = $data;
    }
}