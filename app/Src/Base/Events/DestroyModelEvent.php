<?php

namespace App\Src\Base\Events;

use App\Src\Services\Contracts\DestroyServiceInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyModelEvent
{
    /**
     * @var DestroyServiceInterface
     */
    public $service;

    /**
     * @var Model
     */
    public $model;

    /**
     * @var array
     */
    public $data;


    /**
     * DestroyModelEvent constructor.
     *
     * @param DestroyServiceInterface $service
     * @param Model $model
     * @param array $data
     */
    public function __construct(DestroyServiceInterface $service, Model $model, ?array $data = null)
    {
        $this->service  = $service;
        $this->model    = $model;
        $this->data     = $data;
    }
}