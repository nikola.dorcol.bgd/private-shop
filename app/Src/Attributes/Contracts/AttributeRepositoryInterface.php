<?php

namespace App\Src\Attributes\Contracts;

use App\Src\Attributes\Attribute;
use App\Src\Products\Product;
use Illuminate\Database\Eloquent\Collection;

interface AttributeRepositoryInterface
{
    /**
     * Find product attribute by name.
     *
     * @param string $name
     * @return Attribute|null
     */
    public function findByName(string $name): ?Attribute;

    /**
     * Get all attributes.
     *
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * Get all attributes that belongs to given product.
     *
     * @param Product $product
     * @return Collection
     */
    public function getProductAttributes(Product $product): Collection;
}