<?php

namespace App\Src\Attributes\Values\Repositories;

use App\Src\Attributes\Attribute;
use App\Src\Base\AbstractRepository;
use App\Src\Attributes\Values\Contracts\AttributeValueRepositoryInterface;
use App\Src\Attributes\Values\AttributeValue;

class AttributeValueEloquentRepository extends AbstractRepository implements AttributeValueRepositoryInterface
{

    public function getModelClass(): string
    {
        return AttributeValue::class;
    }


    public function checkIfAttributeHasValue(Attribute $attribute, string $value): ?AttributeValue
    {
        $query = $this->model->where('value', $value)
            ->where('attribute_id', $attribute->id);

        return $this->first($query);
    }
}