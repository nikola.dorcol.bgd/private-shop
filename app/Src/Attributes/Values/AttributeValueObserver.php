<?php

namespace App\Src\Attributes\Values;

use App\Src\Base\AbstractObserver;

class AttributeValueObserver extends AbstractObserver
{
    /**
     * Handle the AttributeValue "created" event.
     *
     * @param AttributeValue $attributeValue
     * @return void
     */
    public function created(AttributeValue $attributeValue): void
    {
        $this->clearCacheTags($attributeValue->getTable());
    }


    /**
     * Handle the AttributeValue "updated" event.
     *
     * @param AttributeValue $attributeValue
     * @return void
     */
    public function updated(AttributeValue $attributeValue)
    {
        $this->clearCacheTags($attributeValue->getTable());
    }


    /**
     * Handle the AttributeValue "deleted" event.
     *
     * @param AttributeValue $attributeValue
     * @return void
     */
    public function deleted(AttributeValue $attributeValue)
    {
        $this->clearCacheTags($attributeValue->getTable());
    }
}