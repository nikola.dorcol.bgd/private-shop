<?php

namespace App\Src\Attributes\Values;

use App\Src\Attributes\Attribute;
use App\Src\Products\AttributeValue\ProductAttributeValue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AttributeValue extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attribute_values';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'attribute_id',
        'value'
    ];


    /**
     * Set the attribute value's value.
     *
     * @param string $value
     */
    public function setValueAttribute(string $value): void
    {
        $this->attributes['value'] = strtolower($value);
    }


    /**
     * Get product attribute that owns value.
     *
     * @return BelongsTo
     */
    public function productAttribute(): BelongsTo
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }


    /**
     * Get all product attribute values that belongs to attribute value.
     *
     * @return HasMany
     */
    public function productAttributeValue(): HasMany
    {
        return $this->hasMany(ProductAttributeValue::class, 'attribute_value_id', 'id');
    }
}