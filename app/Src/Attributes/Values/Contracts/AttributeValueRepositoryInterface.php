<?php

namespace App\Src\Attributes\Values\Contracts;

use App\Src\Attributes\Attribute;
use App\Src\Attributes\Values\AttributeValue;

interface AttributeValueRepositoryInterface
{
    /**
     * Find product attribute value by value.
     *
     * @param Attribute $attribute
     * @param string $value
     * @return AttributeValue|null
     */
    public function checkIfAttributeHasValue(Attribute $attribute, string $value): ?AttributeValue;
}