<?php

namespace App\Src\Attributes\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AttributeDataTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'values_count'  => $this->values_count,
            'created_at'    => $this->created_at
        ];
    }
}