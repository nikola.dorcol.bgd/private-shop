<?php

namespace App\Src\Attributes\Resources;

use App\Src\Attributes\Values\Resources\AttributeValueResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AttributeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'values'=> AttributeValueResource::collection($this->whenLoaded('values'))
        ];
    }
}