<?php

namespace App\Src\Attributes\Queries\EloquentRepositoryQueries;

use App\Src\Base\Contracts\QueryInterface;
use App\Src\Traits\ParametersValidityTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ProductAttributesQuery implements QueryInterface
{
    use ParametersValidityTrait;


    public function build(Model $attribute, array $data): Builder
    {
        $product = $data['product'];

        return $attribute->whereHas('productAttributeValue', function ($q) use ($product) {

            $q->whereHas('productVariant', function ($q) use ($product) {

                $q->whereHas('product', function ($q) use ($product) {

                    $q->where('id', $product->id);
                });
            });
        })
            ->with([

                'values' => function ($q) use ($product) {

                    $q->whereHas('productAttributeValue', function ($q) use ($product) {

                        $q->whereHas('productVariant', function ($q) use ($product) {

                            $q->whereHas('product', function ($q) use ($product) {

                                $q->where('id', $product->id);
                            });
                        });
                    });
                }
            ]);
    }
}