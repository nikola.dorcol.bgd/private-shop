<?php

namespace App\Src\Attributes\Requests;

use App\Src\Base\AbstractFormRequest;
use Illuminate\Contracts\Validation\Validator;

/**
 * Sanitize attribute name in order to validate its uniqueness
 * since it is always stored as sanitizeString().
 *
 * Class AbstractAttributeRequest
 *
 * @package App\Src\Attributes\Requests
 */
abstract class AbstractAttributeRequest extends AbstractFormRequest
{
    /**
     * Get the validator instance for the request.
     *
     * @return Validator
     */
    public function getValidatorInstance(): Validator
    {
        $this->sanitizeValues();

        return parent::getValidatorInstance();
    }


    /**
     * Sanitize value string before validation.
     */
    private function sanitizeValues(): void
    {
        $this->merge([
            'name' => $this->has('name') ? sanitizeString($this->request->get('name')) : '',
        ]);
    }
}