<?php

namespace App\Src\Attributes\Requests;

class StoreAttributeRequest extends AbstractAttributeRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'      => 'required|string|unique:attributes,name',
            'values'    => 'required|array',
            'values.*'  => 'required|string'
        ];
    }
}