<?php

namespace App\Src\Attributes;

use App\Src\Attributes\Values\AttributeValue;
use App\Src\Products\AttributeValue\ProductAttributeValue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Nicolaslopezj\Searchable\SearchableTrait;

class Attribute extends Model
{
    use SearchableTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'attributes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Search rules.
     *
     * @var array
     */
    protected $searchable = [

        'columns' => [
            'attributes.name' => 10
        ]
    ];


    /**
     * Set the attribute's name.
     *
     * @param string $name
     */
    public function setNameAttribute(string $name): void
    {
        $this->attributes['name'] = strtolower($name);
    }


    /**
     * Get all values that belongs to product attribute.
     *
     * @return HasMany
     */
    public function values(): HasMany
    {
        return $this->hasMany(AttributeValue::class, 'attribute_id');
    }


    /**
     * Get all product attribute values that belongs to attribute.
     *
     * @return HasMany
     */
    public function productAttributeValue(): HasMany
    {
        return $this->hasMany(ProductAttributeValue::class, 'attribute_id', 'id');
    }
}