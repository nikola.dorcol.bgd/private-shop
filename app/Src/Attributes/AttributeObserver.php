<?php

namespace App\Src\Attributes;

use App\Src\Base\AbstractObserver;

class AttributeObserver extends AbstractObserver
{
    /**
     * Handle the Attribute "created" event.
     *
     * @param Attribute $attribute
     * @return void
     */
    public function created(Attribute $attribute): void
    {
        $this->clearCacheTags($attribute->getTable());
    }


    /**
     * Handle the Attribute "updated" event.
     *
     * @param Attribute $attribute
     * @return void
     */
    public function updated(Attribute $attribute)
    {
        $this->clearCacheTags($attribute->getTable());
    }


    /**
     * Handle the Attribute "deleted" event.
     *
     * @param Attribute $attribute
     * @return void
     */
    public function deleted(Attribute $attribute)
    {
        $this->clearCacheTags($attribute->getTable());
    }
}