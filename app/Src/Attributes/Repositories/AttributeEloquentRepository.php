<?php

namespace App\Src\Attributes\Repositories;

use App\Src\Attributes\Queries\EloquentRepositoryQueries\ProductAttributesQuery;
use App\Src\Base\AbstractRepository;
use App\Src\Attributes\Contracts\AttributeRepositoryInterface;
use App\Src\Attributes\Attribute;
use App\Src\Base\Contracts\DataTableRepositoryInterface;
use App\Src\Attributes\Queries\EloquentRepositoryQueries\AttributeDataTableQuery;
use App\Src\Products\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class AttributeEloquentRepository extends AbstractRepository implements AttributeRepositoryInterface, DataTableRepositoryInterface
{

    public function getModelClass(): string
    {
        return Attribute::class;
    }


    public function findByName(string $name): ?Attribute
    {
        $query = $this->model->where('name', $name);

        return $this->first($query);
    }


    public function getAll(): Collection
    {
        $query = $this->model->query();

        return $this->get($query);
    }


    public function dataTable(array $data): LengthAwarePaginator
    {
        return $this->paginate((new AttributeDataTableQuery())->build($this->model, $data), $data['perPage']);
    }


    public function getProductAttributes(Product $product): Collection
    {
        return $this->get((new ProductAttributesQuery())->build($this->model, ['product' => $product]));
    }
}