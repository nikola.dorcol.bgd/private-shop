<?php

namespace App\Src\Categories;

use App\Src\Images\Image;
use App\Src\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name',
        'description'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'icon'
    ];


    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlugAttribute(): string
    {
        return str_replace(' ', '-', strtolower($this->attributes['name']));
    }


    /**
     * Get icon path for category.
     *
     * @return Image|null
     */
    public function getIconAttribute(): ?Image
    {
        return $this->images()->icon()->first();
    }


    /**
     * Get all children categories for parent.
     *
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }


    /**
     * Get parent category.
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }


    /**
     * Get all products that belong to category.
     *
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'categories_products');
    }


    /**
     * Get all of category's images.
     *
     * @return MorphMany
     */
    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}