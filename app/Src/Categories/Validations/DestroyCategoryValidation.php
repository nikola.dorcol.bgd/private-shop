<?php

namespace App\Src\Categories\Validations;

use App\Src\Base\Contracts\ValidationInterface;
use Illuminate\Database\Eloquent\Model;

class DestroyCategoryValidation implements ValidationInterface
{

    public function validate(Model $category, ?array $data = null): bool
    {
        $category->load('products', 'children');

        return $category->products->count() > 0 || $category->children->count() > 0;
    }
}