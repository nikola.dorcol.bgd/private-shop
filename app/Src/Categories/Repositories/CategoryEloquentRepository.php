<?php

namespace App\Src\Categories\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Base\Contracts\AutocompleteRepositoryInterface;
use App\Src\Base\Contracts\DataTableRepositoryInterface;
use App\Src\Categories\Category;
use App\Src\Categories\Contracts\CategoryRepositoryInterface;
use App\Src\Categories\Queries\EloquentRepositoryQueries\CategoryDataTableQuery;
use App\Src\Traits\RepositoryAutocompleteTrait;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class CategoryEloquentRepository extends AbstractRepository implements CategoryRepositoryInterface, DataTableRepositoryInterface, AutocompleteRepositoryInterface
{
    use RepositoryAutocompleteTrait;

    public function getModelClass(): string
    {
        return Category::class;
    }


    public function getAll(): LengthAwarePaginator
    {
        $query = $this->model->query();

        return $this->paginate($query);
    }


    public function dataTable(array $data): LengthAwarePaginator
    {
        return $this->paginate((new CategoryDataTableQuery())->build($this->model, $data), $data['perPage']);
    }


    public function getMainCategories(): Collection
    {
        $query = $this->model->whereNull('parent_id')
            ->with('children')
            ->orderBy('name', 'ASC');

        return $this->get($query, ['*'], true, ['categories']);
    }
}