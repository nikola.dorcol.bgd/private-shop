<?php

namespace App\Src\Categories\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AutocompleteCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->name,
            'text'  => $this->name . ($this->parent ? ' Parent: ' . $this->parent->name : '')
        ];
    }
}