<?php

namespace App\Src\Categories\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DataTableCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'parent'            => new CategoryResource($this->parent),
            'children_count'    => $this->children_count,
            'created_at'        => $this->created_at
        ];
    }
}