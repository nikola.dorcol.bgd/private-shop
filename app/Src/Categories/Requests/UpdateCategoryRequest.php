<?php

namespace App\Src\Categories\Requests;

use App\Src\Base\AbstractFormRequest;

class UpdateCategoryRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'parent_id'     => 'nullable|exists:categories,id|not_in:' . $this->route('category')->id,
            'name'          => 'required',
            'description'   => 'nullable',
            'icon'          => 'nullable|file|mimes:jpeg,jpg,png|max:10000'
        ];
    }
}