<?php

namespace App\Src\Categories\Requests;

use App\Src\Base\AbstractFormRequest;

class StoreCategoryRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'parent_id'     => 'nullable|exists:categories,id',
            'name'          => 'required',
            'description'   => 'present',
            'icon'          => 'required_without:parent_id|file|mimes:jpeg,jpg,png|max:10000'
        ];
    }
}