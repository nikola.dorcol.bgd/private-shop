<?php

namespace App\Src\Categories;

use App\Src\Base\AbstractObserver;
use Illuminate\Support\Facades\Log;

class CategoryObserver extends AbstractObserver
{
    /**
     * Handle the Category "created" event.
     *
     * @param Category $category
     * @return void
     */
    public function created(Category $category): void
    {
        $this->clearCacheTags($category->getTable());
    }


    /**
     * Handle the Category "updated" event.
     *
     * @param Category $category
     * @return void
     */
    public function updated(Category $category)
    {
        $this->clearCacheTags($category->getTable());
    }


    /**
     * Handle the Category "deleted" event.
     *
     * @param Category $category
     * @return void
     */
    public function deleted(Category $category)
    {
        $this->clearCacheTags($category->getTable());
    }
}