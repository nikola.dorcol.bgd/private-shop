<?php

namespace App\Src\Categories\Contracts;

use App\Src\Categories\Category;
use App\Src\Images\Image;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface CategoryRepositoryInterface
{

    /**
     * Get categories for data table.
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): LengthAwarePaginator;

    /**
     * Get main categories (without parent).
     *
     * Recommendation: cache this query.
     *
     * @return Collection
     */
    public function getMainCategories(): Collection;
}