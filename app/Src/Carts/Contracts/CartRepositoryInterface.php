<?php

namespace App\Src\Carts\Contracts;

use App\Src\Carts\Cart;
use App\Src\Products\Variants\ProductVariant;
use App\Src\Users\User;
use Illuminate\Database\Eloquent\Collection;

interface CartRepositoryInterface
{
    /**
     * Get all items in cart for given user.
     *
     * @param User $user
     * @return Collection
     */
    public function getUserCart(User $user): Collection;

    /**
     * Find user item by variant id.
     *
     * @param User $user
     * @param ProductVariant $productVariant
     * @return Cart|null
     */
    public function findUserProductVariant(User $user, ProductVariant $productVariant): ?Cart;
}