<?php

namespace App\Src\Carts\Rules;

use App\Src\Carts\Contracts\CartRepositoryInterface;
use App\Src\Users\User;
use Illuminate\Contracts\Validation\Rule;

class CheckIfUserOwnsCartRule implements Rule
{
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * @var User
     */
    protected $user;


    /**
     * Create a new rule instance.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->cartRepository = app()->make(CartRepositoryInterface::class);
        $this->user = $user;
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param mixed $value
     * @return bool
     * @internal param mixed $variant
     */
    public function passes($attribute, $value)
    {
        $cart = $this->cartRepository->find($value);

        if (!$cart) {

            return false;
        }

        if ($this->user->id != $cart->user_id) {

            return false;
        }

        return true;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute does not belong to user';
    }
}