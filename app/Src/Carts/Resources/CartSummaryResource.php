<?php

namespace App\Src\Carts\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartSummaryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'cart_id'       => $this->id,
            'product_id'    => $this->productVariant->product->id,
            'slug'          => $this->productVariant->product->slug,
            'name'          => $this->productVariant->product->name,
            'main_image'    => $this->productVariant->product->main_image,
            'price'         => $this->productVariant->price,
            'quantity'      => $this->quantity,
            'variant'       => $this->productVariant
        ];
    }
}