<?php

namespace App\Src\Carts\Repositories;

use App\Src\Base\AbstractRepository;
use App\Src\Carts\Cart;
use App\Src\Carts\Contracts\CartRepositoryInterface;
use App\Src\Products\Variants\ProductVariant;
use App\Src\Users\User;
use Illuminate\Database\Eloquent\Collection;

class CartEloquentRepository extends AbstractRepository implements CartRepositoryInterface
{

    public function getModelClass(): string
    {
        return Cart::class;
    }


    public function getUserCart(User $user): Collection
    {
        $query = $this->model->with('productVariant.productAttributeValues.attributeValue',
                'productVariant.productAttributeValues.attribute',
                'productVariant.product')
            ->where('user_id', $user->id);

        return $this->get($query);
    }


    public function findUserProductVariant(User $user, ProductVariant $productVariant): ?Cart
    {
        $query = $this->model->where('user_id', $user->id)
            ->where('product_variant_id', $productVariant->id);

        return $this->first($query);
    }
}