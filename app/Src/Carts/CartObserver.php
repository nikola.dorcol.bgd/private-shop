<?php

namespace App\Src\Carts;

use App\Src\Base\AbstractObserver;

class CartObserver extends AbstractObserver
{
    /**
     * Handle the Cart "created" event.
     *
     * @param Cart $cart
     * @return void
     */
    public function created(Cart $cart): void
    {
        $this->clearCacheTags($cart->getTable());
    }


    /**
     * Handle the Cart "updated" event.
     *
     * @param Cart $cart
     * @return void
     */
    public function updated(Cart $cart)
    {
        $this->clearCacheTags($cart->getTable());
    }


    /**
     * Handle the Cart "deleted" event.
     *
     * @param Cart $cart
     * @return void
     */
    public function deleted(Cart $cart)
    {
        $this->clearCacheTags($cart->getTable());
    }
}