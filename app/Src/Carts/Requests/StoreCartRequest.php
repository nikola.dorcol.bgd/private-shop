<?php

namespace App\Src\Carts\Requests;

use App\Src\Base\AbstractFormRequest;
use App\Src\Products\Variants\Rules\CanCreateProductVariantCartRule;

class StoreCartRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'quantity' => [
                'required',
                'integer',
                'min:1',
//                new CanCreateProductVariantCartRule($this->route('productVariant'))
            ]
        ];
    }
}