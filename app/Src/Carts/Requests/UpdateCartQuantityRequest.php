<?php

namespace App\Src\Carts\Requests;

use App\Src\Base\AbstractFormRequest;
use App\Src\Carts\Rules\CheckIfUserOwnsCartRule;

class UpdateCartQuantityRequest extends AbstractFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'products'              => 'required|array',
            'products.*.id'         => [
                'required',
                new CheckIfUserOwnsCartRule(auth()->user())
            ],
            'products.*.quantity'   => [
                'required',
                'integer',
                'min:1',
//                new CanCreateProductVariantCartRule($this->route('productVariant'))
            ]
        ];
    }
}