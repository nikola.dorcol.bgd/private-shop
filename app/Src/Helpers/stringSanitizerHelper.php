<?php

if (! function_exists('sanitizeString')) {

    /**
     * Helper function for sanitizing attributes and values strings before inserting into db.
     *
     * @param string $string
     * @return string
     */
    function sanitizeString(string $string): string
    {
        return rtrim(ltrim(strtolower($string), ' '), ' ');
    }
}