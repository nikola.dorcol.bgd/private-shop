<?php

use App\Src\Base\AbstractRepository;
use App\Src\Base\Errors;
use App\Src\Base\Exceptions\InvalidRepositoryClassException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

if (! function_exists('bindRouteParam')) {

    /**
     * Helper function for Route::bind closure in order to find model via repository.
     *
     * @param int $value
     * @param string $repositoryClassName
     * @return Model
     * @throws InvalidRepositoryClassException
     * @throws ModelNotFoundException
     */
    function bindRouteParam(int $value, string $repositoryClassName): Model
    {
        $repo = app()->make($repositoryClassName);

        if (! $repo instanceof AbstractRepository) {

            throw new InvalidRepositoryClassException();
        }

        $model = $repo->find($value);

        if (! $model) {

            abort(request()->ajax() ? response()->json(Errors::MODEL_NOT_FOUND, 404) : 404);
        }

        return $model;
    }
}