
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap.js');


// Cookies
require('js-cookie');


// Moment
window.moment = require('moment');


// Tooltip
window.tooltip = require('tooltip');


// Perfect scroll bar
import PerfectScrollbar from 'perfect-scrollbar';

window.PerfectScrollbar = PerfectScrollbar;


// wNumb formatting
require('wnumb');