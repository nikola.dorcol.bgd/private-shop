
require('../admin.js');

import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import {routes} from './routes.js';

import adminStore from '../adminStore.js';
import storeData from './store.js';

import OrdersTemplate from './OrdersTemplate.vue';

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.component('orders-template', OrdersTemplate);

const router = new VueRouter({

    routes,

    mode: 'history',

    base: 'admin/orders/index'
});

const store = new Vuex.Store(_.merge(storeData, adminStore));

const app = new Vue({

    el: '#admin-app',

    router,

    store
});

