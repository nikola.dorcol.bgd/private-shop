
import Vue from 'vue';

/**
 * Products store
 */
export default {

    state: {

        title: 'Orders',

        entity: 'orders',

        order: {

            billing_info: {},

            products: []
        },

        finalOrder: {

            customer: {},

            billing: {},

            cart: [],

            final: {
                
                status: null,
                
                admin_comment: null,

                final: 1
            }
        }
    },

    mutations: {

        /**
         * @param state
         * @param order
         */
        order(state, order) {

            state.order = order;
        },

        /**
         * @param state
         * @param order
         */
        finalOrder(state, order) {
          
            state.finalOrder = order;
        },

        /**
         * @param state
         * @param final
         */
        final(state, final) {

            state.finalOrder.final = final;
        },

        /**
         * @param state
         * @param billingInfo
         */
        orderBillingInfo(state, billingInfo) {

            state.order.billing_info = billingInfo;
        },

        /**
         * @param state
         * @param products
         */
        orderProducts(state, products) {

            state.order.products = products;
        },

        /**
         * @param state
         * @param cart
         */
        finaOrderCart(state, cart) {

            state.finalOrder.cart.products = cart;
        }
    },

    getters: {

        getFinalOrderRequestObject(state) {

            return {

                status: state.finalOrder.final.status,
                admin_comment: state.finalOrder.final.admin_comment,
                cart: state.finalOrder.cart.products
            }
        }
    },

    actions: {

        /**
         * Set default order object.
         *
         * @param context
         */
        setDefaultOrder(context) {

            context.commit('order', { billing_info: {}, products: [] });
        },


        /**
         * Set order object.
         *
         * @param context
         * @param order
         */
        setOrder(context, order) {

            context.commit('order', {

                billing_info: order.billing_info,
                products: order.products
            });
        },
        
        
        /**
         * Set order base info.
         *
         * @param context
         * @param billingInfo
         */
        setOrderBillingInfo(context, billingInfo) {

            context.commit('orderBillingInfo', billingInfo);
        },

        
        /**
         * Set order products.
         *
         * @param context
         * @param products
         */
        setOrderProducts(context, products) {

            context.commit('orderProducts', products);
        },



        /**
         * Set final order object.
         *
         * @param context
         * @param order
         */
        setFinalOrder(context, order) {

            context.commit('finalOrder', {

                customer: order.customer,
                billing: order.billing,
                cart: order.cart,
                final: order.final
            });
        },


        /**
         * Set final information.
         *
         * @param context
         * @param final
         */
        setFinal(context, final) {

            context.commit('final', final);
        },


        /**
         * Set default final order
         *
         * @param context
         */
        setDefaultFinalOrder(context) {

            context.commit('finalOrder', {

                customer: {},
                billing: {},
                cart: [],
                final: { status: null, admin_comment: null }
            });
        },


        /**
         * Set cart
         *
         * @param context
         * @param cart
         */
        setFinalOrderCart(context, cart) {

            context.commit('finaOrderCart', cart);
        }
    }
}