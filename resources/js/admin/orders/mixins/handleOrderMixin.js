/**
 * Common methods for orders handling.
 */
export default {

    methods: {

        /**
         * Remove all saved attributes data from order.
         */
        removeOrder() {

            this.$store.dispatch('setDefaultOrder')
        },


        /**
         * Prepare order object for store request.
         */
        createEntityRequestObject() {

            return createRequestObject(this.order);
        }
    },

    computed: {

        /**
         * Get currently order from store.
         */
        order() {

            return this.$store.state.order;
        }
    },

    /**
     * Set order in store to default state. In order to always have empty fields on create page.
     */
    beforeDestroy() {

        this.removeOrder();
    }
}