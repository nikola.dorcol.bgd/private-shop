
import Vue from 'vue';

/**
 * Products store
 */
export default {

    state: {

        title: 'Attributes',

        entity: 'attributes',

        attribute: {}
    },

    mutations: {

        /**
         * @param state
         * @param data
         */
        attribute(state, data) {

            state.attribute = data;
        }
    },

    actions: {

        /**
         * Set attribute object.
         *
         * @param context
         * @param data
         */
        setAttribute(context, data) {

            context.commit('attribute', data);
        },


        /**
         * Set default attribute object.
         *
         * @param context
         */
        setDefaultAttribute(context) {

            context.commit('attribute', {});
        }
    }
}