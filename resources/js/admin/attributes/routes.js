
import Index from './components/index/Index.vue';
import Create from './components/create/Create.vue';

export const routes = [
    {
        path: '/',
        name: 'index',
        component: Index,
    },

    {
        path: '/create',
        name: 'create',
        component: Create,
    }
];
