/**
 * Group of attributes that are the same for create and edit user components.
 */
export default {

    methods: {

        /**
         * Remove all saved attributes data from user.
         */
        removeUser() {

            this.$store.dispatch('setDefaultUser')
        },


        /**
         * Prepare user object for store request.
         */
        createEntityRequestObject() {

            return createRequestObject(this.user);
        }
    },

    computed: {

        /**
         * Get currently user from store.
         */
        user() {

            return this.$store.state.user;
        }
    },


    /**
     * Set user in store to default state. In order to always have empty fields on create page.
     */
    beforeDestroy() {

        this.removeUser();
    }
}