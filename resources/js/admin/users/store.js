
import Vue from 'vue';

/**
 * Products store
 */
export default {

    state: {

        title: 'Users',

        entity: 'users',

        roles: [],

        user: {}
    },

    mutations: {

        /**
         * @param state
         * @param roles
         */
        roles(state, roles) {

            state.roles = roles;
        },


        /**
         * @param state
         * @param user
         */
        user(state, user) {

            state.user = user;
        }
    },

    getters: {

        /**
         * Get roles for input select.
         *
         * @param state
         */
        selectRoles(state) {

            return state.roles.map( (el) => { return { value: el.id, label: el.name }})
        }
    },

    actions: {

        /**
         * @param context
         * @param roles
         */
        setRoles(context, roles) {

            context.commit('roles', roles);
        },


        /**
         * Set user object.
         *
         * @param context
         * @param user
         */
        setUser(context, user) {

            context.commit('user', user);
        },


        /**
         * Set default user.
         *
         * @param context
         */
        setDefaultUser(context) {

            context.commit('user', {});
        }
    }
}