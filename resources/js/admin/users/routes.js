import Index from './components/index/Index.vue';
import Create from './components/create/Create.vue';
import Edit from './components/edit/Edit.vue';

export const routes = [
    {
        path: '/',
        name: 'index',
        component: Index,
    },

    {
        path: '/create',
        name: 'create',
        component: Create,
    },

    {
        path: '/edit/:id',
        name: 'edit',
        component: Edit
    }
];
