/**
 * Products store
 */
export default {

    state: {

        title: 'Warehouses',

        entity: 'warehouses',

        warehouse: {}
    },

    mutations: {

        /**
         * @param state
         * @param warehouse
         */
        warehouse(state, warehouse) {

            state.warehouse = warehouse;
        }
    },

    actions: {

        /**
         * Set warehouse object.
         *
         * @param context
         * @param warehouse
         */
        setWarehouse(context, warehouse) {

            context.commit('warehouse', warehouse);
        },


        /**
         * Set default warehouse object.
         *
         * @param context
         */
        setDefaultWarehouse(context) {

            context.commit('warehouse', {});
        }
    }
}