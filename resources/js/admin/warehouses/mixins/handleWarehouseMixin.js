/**
 * Group of attributes that are the same for create and edit warehouse components.
 */
export default {

    methods: {

        /**
         * Remove all saved attributes data from warehouse.
         */
        removeWarehouse() {

            this.$store.dispatch('setDefaultWarehouse')
        },


        /**
         * Prepare warehouse object for store request.
         */
        createEntityRequestObject() {

            return createRequestObject(this.warehouse);
        }
    },

    computed: {

        /**
         * Get currently warehouse from store.
         *
         * @return {computed.warehouse|state.warehouse|{}|mutations.warehouse|*}
         */
        warehouse() {

            return this.$store.state.warehouse;
        }
    },

    /**
     * Set warehouse in store to default state. In order to always have empty fields on create page.
     */
    beforeDestroy() {

        this.removeWarehouse();
    }
}