/**
 * Props for data table referring to warehouse inventory entity.
 *
 * @type {{data: (())}}
 */
export const warehouseInventoryDataTableProps = {

    data() {

        return {

            routeName: 'admin.warehouses.inventory.dataTable',

            routeParams: {
                warehouse: this.$route.params.id
            },

            searchColumns: [

                {
                    label: 'Product Name',
                    prop: 'name',
                    placeholder: 'E.g. Protein',
                    value: '',
                    type: 'text'
                },

                {
                    label: 'Active',
                    prop: 'active',
                    values: [
                        {
                            value: 1,
                            label: 'Active'
                        },

                        {
                            value: 0,
                            label: 'Inactive'
                        }
                    ],
                    type: 'select'
                }
            ],

            tableColumns: [
                {
                    label: 'ID',
                    prop: 'id',
                },

                {
                    label: 'Name',
                    prop: 'name'
                },

                {
                    label: 'Brand',
                    prop: 'brand.name',
                    sortable: false
                },

                {
                    label: 'Active',
                    prop: 'active',

                    formatter(row, column, cellValue, index) {

                        return cellValue === true ? 'Active' : 'Inactive';
                    }
                },

                {
                    label: 'Created At',
                    prop: 'created_at',

                    formatter(row, columnm, cellValue, index) {

                        return moment(cellValue).format('YYYY-MM-DD');
                    }
                }
            ],

            actionColumns: {

                label: 'Action',

                buttons: [

                    {
                        props: {

                            class: 'm-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill',
                            icon: 'la la-edit'
                        },

                        handler: (row) => {

                            if (row.final == 1) {

                                toastr.error('Can\'t Edit Finished Order!');

                            } else {

                                // Because this is referring to parent class in our example Index.vue, se we need to trigger event in child.
                                this.$refs.warehouseInventoryDataTable.$emit('edit-row', row);
                            }
                        },

                        tooltip: 'Edit'
                    }
                ]
            }
        }
    }
};