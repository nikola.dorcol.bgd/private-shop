/**
 * Props for data table referring to category entity.
 *
 * @type {{data: (())}}
 */
export const categoriesDataTableProps = {

    data() {

        return {

            routeName: 'admin.categories.dataTable',

            searchColumns: [

                {
                    label: 'Category Name',
                    prop: 'name',
                    placeholder: 'E.g. Man',
                    value: '',
                    type: 'text'
                }
            ],

            tableColumns: [
                {
                    label: 'ID',
                    prop: 'id'
                },

                {
                    label: 'Name',
                    prop: 'name'
                },

                {
                    label: 'Parent Category',
                    prop: 'parent.name',
                    sortable: false,

                    formatter(row, column, cellValue, index) {

                        return cellValue !== null ? cellValue : '/';
                    }
                },

                {
                    label: 'Number Of Subcategories',
                    prop: 'children_count',
                    sortable: false
                },

                {
                    label: 'Created At',
                    prop: 'created_at',

                    formatter(row, columnm, cellValue, index) {

                        return moment(cellValue).format('YYYY-MM-DD');
                    }
                }
            ]
        }
    }
};