/**
 * Props for data table referring to product entity.
 *
 * @type {{data: (())}}
 */
export const productsDataTableProps = {

    mounted() {

        /**
         * Setters and getters required on each mount because if switching through components
         * values wont update if there wasn't any change in store.
         */
        this.setSearchBrandsValues();
        this.setSearchCategoriesValues();
    },

    data() {

        return {

            routeName: 'admin.products.dataTable',

            searchColumns: [

                {
                    label: 'Product Name',
                    prop: 'name',
                    placeholder: 'E.g. Protein',
                    value: '',
                    type: 'text'
                },

                {
                    label: 'Brand',
                    prop: 'brand',
                    values: [],
                    type: 'select'
                },

                {
                    label: 'Category',
                    prop: 'category',
                    values: [],
                    type: 'select'
                },

                {
                    label: 'Active',
                    prop: 'active',
                    values: [
                        {
                            value: 1,
                            label: 'Active'
                        },

                        {
                            value: 0,
                            label: 'Inactive'
                        }
                    ],
                    type: 'select'
                }
            ],

            customColumns: [
                {
                    label: 'Thumbnail',
                    prop: 'main_image_src',
                    type: 'image',
                    loaded: false,
                    sortable: false
                },
            ],

            tableColumns: [
                {
                    label: 'ID',
                    prop: 'id'
                },

                {
                    label: 'Name',
                    prop: 'name'
                },

                {
                    label: 'Brand',
                    prop: 'brand.name',
                    sortable: false
                },

                {
                    label: 'Active',
                    prop: 'active',

                    formatter(row, column, cellValue, index) {

                        return cellValue === true ? 'Active' : 'Inactive';
                    }
                },

                {
                    label: 'Created At',
                    prop: 'created_at',

                    formatter(row, columnm, cellValue, index) {

                        return moment(cellValue).format('YYYY-MM-DD');
                    }
                }
            ]
        }
    },

    methods: {

        setSearchBrandsValues() {

            this.searchColumns[1].values = this.getStoreBrandsList();
        },


        setSearchCategoriesValues() {

            this.searchColumns[2].values = this.getStoreCategoriesList();
        },


        getStoreBrandsList() {

            return this.$store.getters.selectBrands;
        },


        getStoreCategoriesList() {

            return this.$store.getters.selectCategories;
        }
    },

    computed: {

        /**
         * Data for brands select from store getter.
         **/
        selectBrandsList() {

            return this.$store.getters.selectBrands;
        },


        /**
         * Data for categories select from store getter.
         **/
        selectCategoriesList() {

            return this.$store.getters.selectCategories;
        }
    },

    watch: {

        /**
         * If brands change in store update search array.
         *
         * @param newSelectBrandsList
         */
        selectBrandsList(newSelectBrandsList) {

            this.searchColumns[1].values = newSelectBrandsList;
        },


        /**
         * If brands change in store update search array.
         *
         * @param newSelectCategoriesList
         */
        selectCategoriesList(newSelectCategoriesList) {

            this.searchColumns[2].values = newSelectCategoriesList;
        }
    }
};