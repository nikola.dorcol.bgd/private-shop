/**
 * Props for data table referring to users entity.
 *
 * @type {{data: (())}}
 */
export const usersDataTableProps = {

    mounted() {

        /**
         * Setters and getters required on each mount because if switching through components
         * values wont update if there wasn't any change in store.
         */
        this.setSearchRolesValues();
    },

    data() {

        return {

            routeName: 'admin.users.dataTable',

            searchColumns: [

                {
                    label: 'User Info',
                    prop: 'query',
                    placeholder: 'Enter user info',
                    value: '',
                    type: 'text'
                },

                {
                    label: 'Role',
                    prop: 'role',
                    values: [],
                    type: 'select'
                }
            ],

            tableColumns: [
                {
                    label: 'ID',
                    prop: 'id'
                },

                {
                    label: 'Role',
                    prop: 'role.name',
                    sortable: false
                },

                {
                    label: 'Name',
                    prop: 'name'
                },

                {
                    label: 'Email',
                    prop: 'email'
                },

                {
                    label: 'Phone',
                    prop: 'phone_number'
                },

                {
                    label: 'Created At',
                    prop: 'created_at',

                    formatter(row, columnm, cellValue, index) {

                        return moment(cellValue).format('YYYY-MM-DD');
                    }
                }
            ]
        }
    },

    methods: {

        setSearchRolesValues() {

            this.searchColumns[1].values = this.getStoreRolesList();
        },


        getStoreRolesList() {

            return this.$store.getters.selectRoles;
        },
    },

    computed: {

        /**
         * Data for roles select from store getter.
         **/
        selectRolesList() {

            return this.$store.getters.selectRoles;
        }
    },

    watch: {

        /**
         * If roles change in store update search array.
         *
         * @param newSelectRolesList
         */
        selectRolesList(newSelectRolesList) {

            this.searchColumns[1].values = newSelectRolesList;
        },
    }
};