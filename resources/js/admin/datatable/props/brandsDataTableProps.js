/**
 * Props for data table referring to brand entity.
 *
 * @type {{data: (())}}
 */
export const brandsDataTableProps = {

    data() {

        return {

            routeName: 'admin.brands.dataTable',

            searchColumns: [

                {
                    label: 'Brand Name',
                    prop: 'name',
                    placeholder: 'E.g. Biotech',
                    value: '',
                    type: 'text'
                }
            ],

            tableColumns: [
                {
                    label: 'ID',
                    prop: 'id'
                },

                {
                    label: 'Name',
                    prop: 'name'
                },

                {
                    label: 'Number Of Products',
                    prop: 'products_count',
                    sortable: false
                },

                {
                    label: 'Created At',
                    prop: 'created_at',

                    formatter(row, columnm, cellValue, index) {

                        return moment(cellValue).format('YYYY-MM-DD')
                    }
                }
            ]
        }
    }
};