/**
 * Props for data table referring to attribute entity.
 *
 * @type {{data: (())}}
 */
export const attributesDataTableProps = {

    data() {

        return {

            routeName: 'admin.attributes.dataTable',

            searchColumns: [

                {
                    label: 'Attribute Name',
                    prop: 'name',
                    placeholder: 'E.g. Size',
                    value: '',
                    type: 'text'
                }
            ],

            tableColumns: [
                {
                    label: 'ID',
                    prop: 'id'
                },

                {
                    label: 'Name',
                    prop: 'name'
                },

                {
                    label: 'Number Of Values',
                    prop: 'values_count',
                    sortable: false
                },

                {
                    label: 'Created At',
                    prop: 'created_at',

                    formatter(row, columnm, cellValue, index) {

                        return moment(cellValue).format('YYYY-MM-DD');
                    }
                }
            ]
        }
    }
};