/**
 * Props for data table referring to order entity.
 *
 * @type {{data: (())}}
 */
export const ordersDataTableProps = {

    data() {

        return {

            routeName: 'admin.orders.dataTable',

            searchColumns: [

                {
                    label: 'Processed',
                    prop: 'final',
                    values: [
                        {
                            value: 1,
                            label: 'Yes'
                        },

                        {
                            value: 0,
                            label: 'No'
                        }
                    ],
                    type: 'select'
                },

                {
                    label: 'Billing Info',
                    prop: 'billing',
                    placeholder: 'Type any billing information',
                    value: '',
                    type: 'text'
                }
            ],

            tableColumns: [

                {
                    label: 'ID',
                    prop: 'id'
                },

                {
                    label: 'Customer',
                    prop: 'user.name',
                    sortable: false
                },

                {
                    label: 'Total Amount',
                    prop: 'total'
                },

                {
                    label: 'State',
                    prop: 'final',

                    formatter(row, columnm, cellValue, index) {

                        return cellValue == 1 ? 'Finished' : 'In Progress'
                    }
                },

                {
                    label: 'Status',
                    prop: 'status',

                    formatter(row, columnm, cellValue, index) {

                        switch (cellValue) {

                            case 0: return 'Pending';

                            case 1: return 'Accepted';

                            case 2: return 'Declined';
                        }

                        return '/';
                    }
                },

                {
                    label: 'Created At',
                    prop: 'created_at',

                    formatter(row, columnm, cellValue, index) {

                        return moment(cellValue).format('YYYY-MM-DD');
                    }
                }
            ],

            actionColumns: {

                label: 'Action',

                buttons: [
                    {
                        props: {

                            class: 'm-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill',
                            icon: 'la la-shopping-cart'
                        },

                        handler: (row) => {

                        // Because this is referring to parent class in our example Index.vue, se we need to trigger event in child.
                            this.$refs.orderDataTable.$emit('show-row', row);
                        },

                        tooltip: 'Show'
                    },

                    {
                        props: {

                            class: 'm-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill',
                            icon: 'la la-edit'
                        },

                        handler: (row) => {

                            if (row.final == 1) {

                                toastr.error('Can\'t Edit Finished Order!');

                            } else {

                            // Because this is referring to parent class in our example Index.vue, se we need to trigger event in child.
                                this.$refs.orderDataTable.$emit('edit-row', row);
                            }
                        },

                        tooltip: 'Edit'
                    },

                    {
                        props: {

                            class: 'm-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill',
                            icon: 'la la-trash'
                        },

                        handler: (row) => {

                            if (row.final == 1) {

                                toastr.error('Can\'t Delete Finished Order!');

                            } else {

                            // Because this is referring to parent class in our example Index.vue, se we need to trigger event in child.
                                this.$refs.orderDataTable.$emit('delete-row', row);
                            }
                        },

                        tooltip: 'Delete'
                    }
                ]
            }
        }
    }
};