/**
 * Props for data table referring to warehouse entity.
 *
 * @type {{data: (())}}
 */
export const warehousesDataTableProps = {

    data() {

        return {

            routeName: 'admin.warehouses.dataTable',

            searchColumns: [

                {
                    label: 'Warehouse Name or Address',
                    prop: 'name',
                    placeholder: 'Enter warehouse name or address',
                    value: '',
                    type: 'text'
                },

                {
                    label: 'Public',
                    prop: 'public',
                    values: [
                        {
                            value: 1,
                            label: 'Public'
                        },

                        {
                            value: 0,
                            label: 'Private'
                        }
                    ],
                    type: 'select'
                }
            ],

            tableColumns: [
                {
                    label: 'ID',
                    prop: 'id'
                },

                {
                    label: 'Name',
                    prop: 'name'
                },

                {
                    label: 'Public',
                    prop: 'public',

                    formatter(row, column, cellValue, index) {

                        return cellValue === 1 ? 'Public' : 'Private';
                    }
                },

                {
                    label: 'Total Value',
                    prop: 'value'
                },

                {
                    label: 'Total Quantity',
                    prop: 'quantity'
                },

                {
                    label: 'Created At',
                    prop: 'created_at',

                    formatter(row, columnm, cellValue, index) {

                        return moment(cellValue).format('YYYY-MM-DD');
                    }
                }
            ],

            actionColumns: {

                label: 'Action',

                buttons: [
                    {
                        props: {

                            class: 'm-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill',
                            icon: 'la la-tag'
                        },

                        handler: (row) => {

                            // Because this is referring to parent class in our example Index.vue, se we need to trigger event in child.
                            this.$refs.warehouseDataTable.$emit('show-row', row);
                        },

                        tooltip: 'Show'
                    },

                    {
                        props: {

                            class: 'm-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill',
                            icon: 'la la-edit'
                        },

                        handler: (row) => {

                            if (row.final == 1) {

                                toastr.error('Can\'t Edit Finished Order!');

                            } else {

                                // Because this is referring to parent class in our example Index.vue, se we need to trigger event in child.
                                this.$refs.warehouseDataTable.$emit('edit-row', row);
                            }
                        },

                        tooltip: 'Edit'
                    },

                    {
                        props: {

                            class: 'm-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill',
                            icon: 'la la-trash'
                        },

                        handler: (row) => {

                            if (row.final == 1) {

                                toastr.error('Can\'t Delete Finished Order!');

                            } else {

                                // Because this is referring to parent class in our example Index.vue, se we need to trigger event in child.
                                this.$refs.warehouseDataTable.$emit('delete-row', row);
                            }
                        },

                        tooltip: 'Delete'
                    }
                ]
            }
        }
    }
};