
require('../admin.js');

import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import {routes} from './routes.js';

import adminStore from '../adminStore.js';
import storeData from './store.js';

import BrandsTemplate from './BrandsTemplate.vue';

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.component('brands-template', BrandsTemplate);

const router = new VueRouter({

    routes,

    mode: 'history',

    base: 'admin/brands/index'
});

const store = new Vuex.Store(_.merge(storeData, adminStore));

const app = new Vue({

    el: '#admin-app',

    router,

    store
});

