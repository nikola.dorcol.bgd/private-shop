/**
 * Group of attributes that are the same for create and edit brand components.
 */
export default {

    methods: {

        /**
         * Remove all saved attributes data from brand.
         */
        removeBrand() {

            this.$store.dispatch('setDefaultBrand')
        },


        /**
         * Prepare brand object for store request.
         */
        createEntityRequestObject() {

            return createRequestObject(this.brand);
        }
    },

    computed: {

        /**
         * Get currently brand from store.
         */
        brand() {

            return this.$store.state.brand;
        }
    },


    /**
     * Set brand in store to default state. In order to always have empty fields on create page.
     */
    beforeDestroy() {

        this.removeBrand();
    }
}