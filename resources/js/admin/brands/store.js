
import Vue from 'vue';

/**
 * Products store
 */
export default {

    state: {

        title: 'Brands',

        entity: 'brands',

        brand: {}
    },

    mutations: {

        /**
         * @param state
         * @param brand
         */
        brand(state, brand) {

            state.brand = brand;
        }
    },

    actions: {

        /**
         * Set brand object.
         *
         * @param context
         * @param brand
         */
        setBrand(context, brand) {

            context.commit('brand', brand);
        },


        /**
         * @param context
         */
        setDefaultBrand(context) {

            context.commit('brand', {})
        }
    }
}