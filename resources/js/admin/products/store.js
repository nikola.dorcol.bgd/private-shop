
import Vue from 'vue';

/**
 * Products store
 */
export default {

    state: {

        title: 'Products',

        entity: 'products',

        brands: [],

        categories: [],

        warehouses: [],

        attributes: [],

        product: {

            base_info: {},
            
            categories: [],

            images: [],

            related_products: [],

            variants: []
        }
    },

    mutations: {

        /**
         * @param state
         * @param brands
         */
        brands(state, brands) {

            state.brands = brands;
        },


        /**
         * @param state
         * @param categories
         */
        categories(state, categories) {

            state.categories = categories;
        },


        /**
         * @param state
         * @param warehouses
         */
        warehouses(state, warehouses) {

            state.warehouses = warehouses;
        },


        /**
         * @param state
         * @param attributes
         */
        attributes(state, attributes) {

            state.attributes = attributes;
        },


        /**
         * @param state
         * @param product
         */
        product(state, product) {

            state.product = product;
        },


        /**
         * @param state
         * @param baseInfo
         */
        productBaseInfo(state, baseInfo) {

            state.product.base_info = baseInfo;
        },


        /**
         * @param state
         * @param categories
         */
        productCategories(state, categories) {

            state.product.categories = categories;
        },


        /**
         * @param state
         * @param images
         */
        productImages(state, images) {

            state.product.images = images;
        },


        /**
         * @param state
         * @param relatedProducts
         */
        productRelatedProducts(state, relatedProducts) {

            state.product.related_products = relatedProducts;
        },


        /**
         * @param state
         * @param variants
         */
        productVariants(state, variants) {

            state.product.variants = variants;
        }
    },

    getters: {

        /**
         * Remove preview from object.
         *
         * @param state
         * @return {Array}
         */
        getProductImagesForStoreRequest(state) {

            return state.product.images.map( (el) => {

                return { description: el.description, main: el.main, file: el.file };
            });
        },


        /**
         * Set active attribute from true/false to 1/0.
         *
         * @param state
         * @return {state.product.base_info|{}}
         */
        getProductBaseInfoForStoreRequest(state) {

            let baseInfo = state.product.base_info;

            baseInfo.active = baseInfo.active === true ? 1 : 0;

            return baseInfo
        },


        /**
         * Return array of related products ids.
         *
         * @param state
         * @return {Array}
         */
        getProductRelatedProductsIDs(state) {

            return state.product.related_products.map( (el) => el.id);
        },

        /**
         * Make array for input select.
         *
         * @param state
         * @returns {Array}
         */
        selectBrands(state) {

            return state.brands.map( (el) => {

                return { value: el.id, label: el.name }
            });
        },


        /**
         * Make array for input select.
         *
         * @param state
         * @returns {Array}
         */
        selectCategories(state) {

            return state.categories.map( (el) => {

                return { value: el.id, label: el.name }
            });
        },


        /**
         * Make array for autocomplete component.
         *
         * @param state
         * @returns {Array}
         */
        autocompleteAttributes(state) {

            return state.attributes.map((el) => el.name);
        }
    },

    actions: {

        /**
         * @param context
         * @param brands
         */
        setBrands(context, brands) {

            context.commit('brands', brands);
        },

        /**
         * @param context
         * @param attributes
         */
        setAttributes(context, attributes) {

            context.commit('attributes', attributes);
        },


        /**
         * @param context
         * @param warehouses
         */
        setWarehouses(context, warehouses) {

            context.commit('warehouses', warehouses);
        },


        /**
         * @param context
         * @param categories
         */
        setCategories(context, categories) {

            context.commit('categories', categories);
        },


        /**
         * Set default product with empty attributes.
         *
         * @param context
         */
        setDefaultProduct(context) {

            context.commit('product', { base_info: {}, categories: [], images: [], related_products: [], variants: [] });
        },


        /**
         * @param context
         * @param product
         */
        setProduct(context, product) {

            context.commit('product', {

                base_info: product.base_info,
                categories: product.categories,
                images: product.images,
                related_products: product.related_products,
                variants: product.variants,
                attributes: product.attributes
            });
        },


        /**
         * @param context
         * @param baseInfo
         */
        setProductBaseInfo(context, baseInfo) {

            context.commit('productBaseInfo', baseInfo);
        },


        /**
         * @param context
         * @param categories
         */
        setProductCategories(context, categories) {

            context.commit('productCategories', categories);
        },


        /**
         * @param context
         * @param images
         */
        setProductImages(context, images) {

            context.commit('productImages', images);
        },


        /**
         * @param context
         * @param relatedProducts
         */
        setProductRelatedProducts(context, relatedProducts) {

            context.commit('productRelatedProducts', relatedProducts);
        },


        /**
         * @param context
         * @param variants
         */
        setProductVariants(context, variants) {

            context.commit('productVariants', variants);
        }
    }
}