/**
 * Group of attributes that are the same for create and edit product components.
 */
export default {

    methods: {

        /**
         * Remove all saved attributes data from product.
         */
        removeProduct() {

            this.$store.dispatch('setDefaultProduct')
        },


        /**
         * Prepare product object for store request.
         */
        createEntityRequestObject() {

            return createRequestObject({

                base_info: this.$store.getters.getProductBaseInfoForStoreRequest,

                categories: this.product.categories,

                images: this.$store.getters.getProductImagesForStoreRequest,

                related_products: this.$store.getters.getProductRelatedProductsIDs,

                variants: this.product.variants
            });
        }
    },

    computed: {

        /**
         * Product object from store.
         *
         * @returns {computed.product|product|{base_info, categories, images, related_products, variants}|state.product|string}
         */
        product() {

            return this.$store.state.product;
        }
    },

    /**
     * Set product in store to default state. In order to always have empty fields on create page.
     */
    beforeDestroy() {

        this.removeProduct();
    }
}