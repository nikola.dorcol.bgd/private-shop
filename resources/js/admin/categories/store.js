
import Vue from 'vue';

/**
 * Products store
 */
export default {

    state: {

        title: 'Categories',

        entity: 'categories',

        category: {},

        // TODO remove after parent is autocomplete
        categories: []
    },

    mutations: {

        /**
         * @param state
         * @param category
         */
        category(state, category) {

            state.category = category;
        },

        // TODO remove after change parent to autocomplete
        /**
         * @param state
         * @param categories
         */
        categories(state, categories) {

            state.categories = categories;
        }
    },

    getters: {

        // TODO remove after parent is autocomplete
        /**
         * Make array for input select.
         *
         * @param state
         * @returns {Array}
         */
        selectCategories(state) {

            return state.categories.map( (el) => {

                return { value: el.id, label: el.name }
            });
        }
    },

    actions: {

        /**
         * @param context
         * @param category
         */
        setCategory(context, category) {

            context.commit('category', category);
        },

        // TODO when change parent to autocomplete
        /**
         * @param context
         * @param categories
         */
        setCategories(context, categories) {

            context.commit('categories', categories);
        },

        /**
         * @param context
         */
        setDefaultCategory(context) {

            context.commit('category', {})
        }
    }
}