/**
 * Common methods for categories handling.
 */
export default {

    methods: {

        /**
         * Remove all saved attributes data from category.
         */
        removeCategory() {

            this.$store.dispatch('setDefaultCategory')
        },


        /**
         * Prepare category object for store request.
         */
        createEntityRequestObject() {

            return createRequestObject(this.category);
        }
    },

    computed: {

        /**
         * Get currently category from store.
         */
        category() {

            return this.$store.state.category;
        }
    },

    /**
     * Set category in store to default state. In order to always have empty fields on create page.
     */
    beforeDestroy() {

        this.removeCategory();
    }
}