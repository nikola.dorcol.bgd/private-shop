/**
 * Mixin where are methods for settings errors to Validation Providers from vee-validate package
 */
export default {

    methods: {

        /**
         * Set new errors.
         *
         * @param errors
         */
        setNewErrors(errors) {

            Object.keys(errors).forEach( (key) => {

                if (typeof this.$refs[key] !== 'undefined') {

                    /**
                     * Each validation component has to have defined ref with name of validation key from backend.
                     * Check if array because in v-for it creates it as array with one element.
                     * Since we are naming refs as backend validation attribute response, [0] will be always correct one.
                     */

                    Array.isArray(this.$refs[key]) ? this.$refs[key][0].setErrors(errors[key]) : this.$refs[key].setErrors(errors[key])
                }
            });
        },


        /**
         * Remove old front validation errors.
         */
        removeOldErrors() {

            Object.keys(this.$refs).forEach( (key) => {

                if (typeof this.$refs[key] !== 'undefined') {

                    let ref = Array.isArray(this.$refs[key]) ? this.$refs[key][0] : this.$refs[key];

                    // For refs created in v-for, there could be empty one left
                    if (typeof ref === 'object') {

                        if (typeof ref.$_veeObserver !== 'undefined') {

                            ref.setErrors([]);
                        }

                    } else {

                    // Remove empty refs.
                        delete this.$refs[key];
                    }
                }
            });
        }
    },

    computed: {

        /**
         * because of multiple tabs for creating certain entities,
         * check if there is any error in different tabs by passing substring to look in errors.
         *
         * @return {function(*=)}
         */
        validationErrorsBySubstring() {

            return (substring) => {

                return typeof Object.keys(this.validationErrors).find((key) => {

                        return key.includes(substring)

                    }) === 'undefined';
            }
        },


        /**
         * All errors from store
         *
         * @return {watch.validationErrors|{handler, deep}|computed.validationErrors|state.validationErrors|{}|mutations.validationErrors|*}
         */
        validationErrors() {

            return this.$store.state.validationErrors;
        }
    },

    watch: {

        /**
         * When validation errors are changed in store, set messages to Validation Providers
         */
        validationErrors: {

            handler(newValidationErrors) {

                this.removeOldErrors();

                this.setNewErrors(newValidationErrors);
            },

            deep: true
        }
    },

    /**
     * When component is destroyed remove errors.
     */
    beforeDestroy() {

        this.$store.dispatch('setValidationErrors', {});
    }
};
