/**
 * Return object of url params.
 *
 * @return {{}}
 */
export const parseUrlParams = () => {

    let query = location.search.substr(1);

    const result = {};

    if (query !== '') {

        query.split('&').forEach((part) => {

            let item = part.split('=');

            result[item[0]] = decodeURIComponent(item[1]);
        });
    }

    return result;
};