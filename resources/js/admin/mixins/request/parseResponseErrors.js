/**
 * Function for parsing response errors
 *
 * @param response
 * @param vm
 */
export const parseResponseErrors = (response, vm = null) => {

    if (typeof response.status !== 'undefined') {

        let status = response.status;

        switch (status) {

            case 413: {

                toastr.error('Please select smaller file(s), Max: 100mb!');

                break;
            }

            case 422: {

                if (vm) {

                    vm.$store.dispatch('setValidationErrors', response.data.errors);
                }

                toastr.error(response.data.message);

                break;
            }

            case 500: {

                toastr.error('Server error 500');

                break;
            }

            default: {

                toastr.error(response.data.error_message);
            }
        }

    } else {

        toastr.error(response);
    }
};