/**
 * Create request object based on FormData.
 *
 * @param obj
 * @param form
 * @param namespace
 * @return {*}
 */
export function createRequestObject(obj, form, namespace) {

    let fd = form || new FormData();
    let formKey;

    for (let property in obj) {

        if(obj.hasOwnProperty(property)) {

            if (namespace) {

                formKey = namespace + '[' + property + ']';

            } else {

                formKey = property;
            }

            /**
             * If the property is an object, but not a File, use recursivity.
             */
            if (obj[property] instanceof Date) {

                fd.append(formKey, obj[property].toISOString());

            } else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

                createRequestObject(obj[property], fd, formKey);

            } else {

                /**
                 * If it's a string or a File object
                 */
                fd.append(formKey, obj[property]);
            }
        }
    }

    return fd;
}