/**
 * Format url params in order to share searches.
 *
 * @param params
 */
export const formatUrlParams = (params) => {

    let url     = window.location.href,
        query   = '';

    url = url.substring(0, url.indexOf('?'));

    for (let param in params) {

        if (params.hasOwnProperty(param)) {

            if (params[param] !== '' && params[param] !== null) {

                if (Array.isArray(params[param])) {

                    if (params[param].length > 0) {

                        query += param + '=' + params[param].toString() + '&';
                    }

                } else {

                    query += param + '=' + params[param] + '&';
                }
            }
        }
    }

    query = query.substring(0, query.length - 1);

    history.pushState(null, '', url + (query !== '' ? '?' + query : ''));
};