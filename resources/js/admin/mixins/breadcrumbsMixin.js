/**
 * Breadcrumb mixin for dynamic breadcrumbs load.
 *
 * @type {{data: (()), mounted: (()), methods: {setStoreBreadcrumb: (())}, beforeDestroy: (())}}
 */
export const breadcrumbsMixin = {

    data() {

        return {

            /**
             * Breadcrumbs to be shown in sub header.
             */
            breadcrumbs: [
                {
                    name: this.$router.currentRoute.name,
                    path: this.$router.currentRoute.fullPath
                }
            ]
        }
    },

    created() {

        this.setStoreBreadcrumbs();
    },


    methods: {

        /**
         * Push previously created breadcrumbs into store.
         */
        setStoreBreadcrumbs() {

            this.$store.dispatch('setBreadcrumbs', this.breadcrumbs);
        },


        /**
         * Remove breadcrumbs from store.
         */
        removeBreadcrumbs() {

            this.$store.dispatch('setBreadcrumbs', {});
        }
    },

    beforeDestroy() {

        this.removeBreadcrumbs();
    }
};