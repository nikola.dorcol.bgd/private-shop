/**
 * Mixin for deleting specific entity with given id.
 *
 * Finish every method name with "Mixin" in order to prevent possible collisions.
 */
export default {

    methods: {

        /**
         * Delete entity.
         *
         * @param entity - name of entity to get
         * @param params
         * @param eventName
         * @param attribute
         */
        deleteEntityMixin(entity, params, eventName = 'entity-deleted', attribute = null) {

            this.setEntityLoadingStatus(entity, true);

            axios.get(route('admin.' + entity + '.destroy' + (attribute ? '.' + attribute : ''), params), {

                    onUploadProgress: ( progressEvent ) => {

                        this.$store.dispatch('setPercentageStatus', parseInt( Math.round( ( progressEvent.loaded * 100 ) / progressEvent.total ) ));
                    }
                })

                .then( (response) => {

                    toastr.success('Successfully deleted!');

                    eventBus.$emit(eventName);
                })

                .catch( (errors) => parseResponseErrors(errors.response, this))

                .then( () => this.setEntityLoadingStatus(entity, false));
        }
    }
};