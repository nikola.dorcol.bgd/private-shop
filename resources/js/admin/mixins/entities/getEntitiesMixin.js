/**
 * Mixin for getting all entities and set them into store.
 *
 * Finish every method name with "Mixin" in order to prevent possible collisions.
 */
export default {

    methods: {

        /**
         * Get all entities.
         *
         * @param entity - name of entity to get
         * @param dispatch - method to dispatch
         */
        getEntitiesMixin(entity, dispatch) {

            this.setEntityLoadingStatus(entity, true);

            axios.get(route('admin.' + entity + '.all'))

                .then( (response) => this.setEntitiesMixin(dispatch, response.data.data))

                .catch( (errors) => parseResponseErrors(errors.response, this))

                .then( () => this.setEntityLoadingStatus(entity, false));
        },


        /**
         * Put brands into store.
         *
         * @param dispatch
         * @param brands
         */
        setEntitiesMixin(dispatch, brands) {

            this.$store.dispatch(dispatch, brands);
        }
    }
};