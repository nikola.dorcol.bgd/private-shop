/**
 * Mixin for getting specific entity with given id and set them into store.
 *
 * Finish every method name with "Mixin" in order to prevent possible collisions.
 */
export default {

    mounted() {

        eventBus.$on('entity-loaded', () => this.parseEntityStoreObject())
    },

    methods: {

        /**
         * Get entity.
         *
         * @param entity - name of entity to get
         * @param id
         * @param dispatch - method to dispatch
         * @param edit
         */
        getEntityMixin(entity, id, dispatch, edit = true) {

            this.setEntityLoadingStatus(entity, true);

            axios.get(route('admin.' + entity + '.' + (edit === true ? 'edit' : 'show'), id))

                .then( (response) => {

                    this.setEntityMixin(dispatch, response.data.data);

                    eventBus.$emit('entity-loaded');
                })

                .catch( (errors) => parseResponseErrors(errors.response, this))

                .then( () => this.setEntityLoadingStatus(entity, false));
        },


        /**
         * Put entity into store.
         *
         * @param dispatch
         * @param entity
         */
        setEntityMixin(dispatch, entity) {

            this.$store.dispatch(dispatch, entity);
        },


        /**
         * Each component that implements this mixin should override this method in order to parse store object.
         */
        parseEntityStoreObject() {}
    }
};