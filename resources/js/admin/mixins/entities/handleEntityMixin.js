/**
 * Mixin for making create/update request for given entity.
 */
export default {

    methods: {

        /**
         * Create/Update entity and redirect to given action (index, create, edit).
         *
         * @param entity
         * @param action
         * @param route
         */
        handleEntity(entity, action, route) {

            this.setEntityLoadingStatus(entity, true);

            axios.post(route, this.createEntityRequestObject(), {

                    onUploadProgress: ( progressEvent ) => {

                        this.$store.dispatch('setPercentageStatus', parseInt( Math.round( ( progressEvent.loaded * 100 ) / progressEvent.total ) ));
                    }
                })

                .then( (response) => this.redirectAfter(action))

                .catch( (errors) => parseResponseErrors(errors.response, this))

                .then( () => {

                    this.setEntityLoadingStatus(entity, false);

                    this.$store.state.loads.percentage = null;
                });
        },


        /**
         * Define in each component that has this mixin.
         *
         * @return {{}}
         */
        createEntityRequestObject() {

            return {};
        },


        /**
         * Action can be create, edit or index.
         */
        redirectAfter(action) {

            toastr.success('Successfully ' + (action == 'edit' ? 'Edited!' : 'Created!'));

            switch (action) {

                case 'edit':

                    this.$store.dispatch('setValidationErrors', {});
                    break;

                case 'create' :

                    this.$router.go(this.$router.currentRoute);
                    break;

                case 'index' :

                    this.$router.back();
                    break;

                default :

                    toastr.error('Invalid action!');
                    return;
            }
        }
    }
}