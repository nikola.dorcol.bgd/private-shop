/**
 * Set loading status of given entity in store.
 */
export default {

    methods: {

        /**
         * Set entity loading status.
         *
         * @param entity
         * @param status
         */
        setEntityLoadingStatus(entity, status) {

            this.$store.dispatch('setLoadingEntityStatus', { entity: entity, status: status });
        }
    },

    computed: {

        /**
         * Check if any entity is loading from backend.
         */
        loadingStatus() {

            return Object.values(this.$store.state.loads).filter( (el) => el === true).length > 0;
        },


        /**
         * Check specific entity loading status.
         */
        entityLoadingStatus() {

            return (entity) => {

                return this.$store.state.loads && typeof this.$store.state.loads[entity] !== 'undefined';
            }
        },

        /**
         * @return {computed.percentage|null|*}
         */
        percentage() {

            return this.$store.state.loads.percentage;
        }
    }
}