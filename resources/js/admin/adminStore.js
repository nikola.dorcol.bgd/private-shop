import Vue from 'vue';

/**
 * Admin store template object
 */
export default {

    state: {

        validationErrors: {},

        loads: {
            percentage: null
        },

        breadcrumbs: [],
    },

    mutations: {

        /**
         * Set entity loads status.
         *
         * @param state
         * @param data
         */
        loads(state, data) {

            Vue.set(state.loads, data.entity, data.status);
        },


        /**
         * Set percentage
         *
         * @param state
         * @param data
         */
        percentage(state, data) {

            state.loads.percentage = data;
        },


        /**
         * Set validation errors.
         *
         * @param state
         * @param errors
         */
        validationErrors(state, errors) {

            state.validationErrors = errors;
        },


        /**
         * @param state
         * @param breadcrumbs
         */
        breadcrumbs(state, breadcrumbs) {

            state.breadcrumbs = breadcrumbs;
        }
    },

    getters: {

        /**
         * Get loads entity status.
         *
         * @param state
         * @param entity
         * @returns {*}
         */
        getEntityLoadingStatus(state, entity) {

            return state.loads[entity];
        }
    },

    actions: {

        /**
         * Set loads entity status true/false.
         *
         * @param context
         * @param data
         */
        setLoadingEntityStatus(context, data) {

            context.commit('loads', data);
        },


        /**
         * Set percentage status.
         *
         * @param context
         * @param data
         */
        setPercentageStatus(context, data) {

            context.commit('percentage', data);
        },


        /**
         * Set validation errors.
         *
         * @param context
         * @param data
         */
        setValidationErrors(context, data) {

            context.commit('validationErrors', data);
        },


        /**
         * @param context
         * @param breadcrumbs
         */
        setBreadcrumbs(context, breadcrumbs) {

            context.commit('breadcrumbs', breadcrumbs);
        }
    }
}