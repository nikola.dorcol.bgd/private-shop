// Notifications

import toastr from 'toastr';
import Swal from 'sweetalert2';

// Global Toastr notifications
window.toastr = toastr;

toastr.options = {
    "debug": false,
    "closeButton": true,
    "positionClass": "toast-bottom-right",
    "onclick": null,
    "fadeIn": 300,
    "fadeOut": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 1000
};


// Global sweet alert notifications
window.Swal = Swal;

/**
 * Global function for swal confirmation modal.
 *
 * @return {Promise<SweetAlertResult>}
 */
window.swalConfirm = () => {

    return Swal.fire({

        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#716aca',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    })
};

/**
 *
 * Global function for swal notification modal
 *
 * @param title
 * @param message
 * @param status
 */
window.swalNotification = (title = null, message = null, status = 'success') => {

    Swal.fire(
        title !== null ? title : 'Success!',
        message !== null ? message : null,
        status
    )
};