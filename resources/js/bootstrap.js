
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}


/**
 * Require global notifications settings.
 */
require('./notifications.js');


/**
 * Global function that creates request FormData object.
 */
import { createRequestObject } from './admin/mixins/request/createRequestObject.js';

window.createRequestObject = createRequestObject;


/**
 * Global function that updates url with params.
 */
import { formatUrlParams } from './admin/mixins/request/formatUrlParams.js';

window.formatUrlParams = formatUrlParams;


/**
 * Global function for parsing errors.
 */
import { parseResponseErrors } from './admin/mixins/request/parseResponseErrors.js';

window.parseResponseErrors = parseResponseErrors;

/**
 * Global function for creating object from url params.
 */
import { parseUrlParams } from './admin/mixins/request/parseUrlParams.js';

window.parseUrlParams = parseUrlParams;


/**
 * Make Ziggy route package global.
 */
window.route = route;


/**
 * Global Vue declarations.
 */
import Vue from 'vue';

/**
 * lodash to be available in Vue templates.
 */
Vue.prototype.$_ = window._;

/**
 * Define global validator
 */
import { ValidationProvider, ValidationObserver } from 'vee-validate/dist/vee-validate.full';

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

/**
 * Loading status vue global mixin
 */
import loadingEntityStatus from './admin/mixins/entities/loadingEntityStatusMixin.js';


Vue.mixin(_.merge(

    loadingEntityStatus,

    {
        methods: {

            route: (name, params, absolute) => route(name, params, absolute, Ziggy),

            consoleLog(log) {

                console.log(log);
            }
        },
    }
));


/**
 * Define global event bus
 *
 * @type {VueConstructor}
 */
window.eventBus = new Vue();


String.prototype.trunc = function ( n, useWordBoundary ) {

    if (this.length <= n) { return this; }

    let subString = this.substr(0, n-1);

    return useWordBoundary ? subString.substr(0, subString.lastIndexOf(' ')) : subString;
};
