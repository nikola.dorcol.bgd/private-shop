
import Vue from 'vue';
import Search from './Search.vue';

Vue.component('search', Search);

let app = new Vue({
    el: '#app'
});