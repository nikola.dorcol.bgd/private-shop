require('../client');

import Vue from 'vue';
import Product from './Summary.vue';

Vue.component('product-info', Product);

let app = new Vue({
    el: '#app'
});