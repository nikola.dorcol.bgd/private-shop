
import Vue from 'vue';
import Product from './Product.vue';

Vue.component('product-info', Product);

let app = new Vue({
    el: '#app'
});