require('../client');

import Vue from 'vue';
import Order from './Order.vue';

Vue.component('order-info', Order);

let app = new Vue({
    el: '#app'
});