@extends('admin.layouts.index')

@section('main')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">

        <div class="m-content">

            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                                    <span class="m-portlet__head-icon m--hide">
                                                        <i class="la la-gear"></i>
                                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        Most Selling Items
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <chartjs-bar :labels="labels" :data="dataset" :bind="true"></chartjs-bar>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')

    <script src="{{ asset('js/admin/admin.js') }}"></script>
    <script src='//cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js'></script>
    <script src='//unpkg.com/vue-chartjs@2.6.0/dist/vue-chartjs.full.min.js'></script>
    <script src='//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.js'></script>
    <script src='//unpkg.com/hchs-vue-charts@1.2.8'></script>
    <script >
        'use strict';

        Vue.use(VueCharts);

        var app = new Vue({
            el: '#admin-app',
            data: function data() {
                return {
                    dataentry: null,
                    datalabel: null,
                    labels: ['Product 1', 'Product 2', 'Product 3', 'Product 4'],
                    dataset: [100, 300, 50, 500]
                };
            }
        });
    </script>

@endsection