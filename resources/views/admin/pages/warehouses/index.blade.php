@extends('admin.layouts.index')

@section('main')

    <warehouses-template></warehouses-template>

@endsection

@section('scripts')

    <script src="{{ asset('js/admin/warehouses/warehouses.js') }}"></script>

@endsection