@extends('admin.layouts.index')

@section('main')

    <users-template></users-template>

@endsection

@section('scripts')

    <script src="{{ asset('js/admin/users/users.js') }}"></script>

@endsection