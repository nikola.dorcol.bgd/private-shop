@extends('admin.layouts.index')

@section('main')

    <brands-template></brands-template>

@endsection

@section('scripts')

    <script src="{{ asset('js/admin/brands/brands.js') }}"></script>

@endsection