@extends('admin.layouts.index')

@section('main')

    <attributes-template></attributes-template>

@endsection

@section('scripts')

    <script src="{{ asset('js/admin/attributes/attributes.js') }}"></script>

@endsection