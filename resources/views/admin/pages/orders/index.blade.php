@extends('admin.layouts.index')

@section('main')

    <orders-template></orders-template>

@endsection

@section('scripts')

    <script src="{{ asset('js/admin/orders/orders.js') }}"></script>

@endsection