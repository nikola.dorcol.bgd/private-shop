@extends('admin.layouts.index')

@section('main')

    <products-template></products-template>

@endsection

@section('scripts')

    <script src="{{ asset('js/admin/products/products.js') }}"></script>

@endsection