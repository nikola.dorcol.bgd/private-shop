@extends('admin.layouts.index')

@section('main')

    <categories-template></categories-template>

@endsection

@section('scripts')

    <script src="{{ asset('js/admin/categories/categories.js') }}"></script>

@endsection