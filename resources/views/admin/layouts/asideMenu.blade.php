<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <li class="{{ Route::currentRouteName() == 'admin.dashboard.index' ? 'm-menu__item  m-menu__item--active' : 'm-menu__item' }}" aria-haspopup="true">
            <a href="{{ route('admin.dashboard.index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-chart-line"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Dashboard</span>
                            {{--<span class="m-menu__link-badge">--}}
                                {{--<span class="m-badge m-badge--danger">2</span>--}}
                            {{--</span> --}}
                    </span>
                </span>
            </a>
        </li>

        <li class="{{ Route::currentRouteName() == 'admin.users.index' ? 'm-menu__item  m-menu__item--active' : 'm-menu__item' }}" aria-haspopup="true">
            <a href="{{ route('admin.users.index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-users"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Users</span>
                        {{--<span class="m-menu__link-badge">--}}
                        {{--<span class="m-badge m-badge--danger">2</span>--}}
                        {{--</span> --}}
                    </span>
                </span>
            </a>
        </li>

        <li class="{{ Route::currentRouteName() == 'admin.orders.index' ? 'm-menu__item  m-menu__item--active' : 'm-menu__item' }}" aria-haspopup="true">
            <a href="{{ route('admin.orders.index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-shopping-cart"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Orders</span>
                        {{--<span class="m-menu__link-badge">--}}
                        {{--<span class="m-badge m-badge--danger">2</span>--}}
                        {{--</span> --}}
                    </span>
                </span>
            </a>
        </li>

        <li class="{{ Route::currentRouteName() == 'admin.brands.index' ? 'm-menu__item  m-menu__item--active' : 'm-menu__item' }}" aria-haspopup="true">
            <a href="{{ route('admin.brands.index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-copyright"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Brands</span>
                        {{--<span class="m-menu__link-badge">--}}
                        {{--<span class="m-badge m-badge--danger">2</span>--}}
                        {{--</span> --}}
                    </span>
                </span>
            </a>
        </li>

        <li class="{{ Route::currentRouteName() == 'admin.categories.index' ? 'm-menu__item  m-menu__item--active' : 'm-menu__item' }}" aria-haspopup="true">
            <a href="{{ route('admin.categories.index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-list"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Categories</span>
                        {{--<span class="m-menu__link-badge">--}}
                        {{--<span class="m-badge m-badge--danger">2</span>--}}
                        {{--</span> --}}
                    </span>
                </span>
            </a>
        </li>

        {{--<li class="{{ Route::currentRouteName() == 'admin.attributes.index' ? 'm-menu__item  m-menu__item--active' : 'm-menu__item' }}" aria-haspopup="true">--}}
            {{--<a href="{{ route('admin.attributes.index') }}" class="m-menu__link ">--}}
                {{--<i class="m-menu__link-icon fa fa-font"></i>--}}
                {{--<span class="m-menu__link-title">--}}
                    {{--<span class="m-menu__link-wrap">--}}
                        {{--<span class="m-menu__link-text">Attributes</span>--}}
                        {{--<span class="m-menu__link-badge">--}}
                        {{--<span class="m-badge m-badge--danger">2</span>--}}
                        {{--</span> --}}
                    {{--</span>--}}
                {{--</span>--}}
            {{--</a>--}}
        {{--</li>--}}

        <li class="{{ Route::currentRouteName() == 'admin.warehouses.index' ? 'm-menu__item  m-menu__item--active' : 'm-menu__item' }}" aria-haspopup="true">
            <a href="{{ route('admin.warehouses.index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-warehouse"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Warehouses</span>
                        {{--<span class="m-menu__link-badge">--}}
                        {{--<span class="m-badge m-badge--danger">2</span>--}}
                        {{--</span> --}}
                    </span>
                </span>
            </a>
        </li>

        <li class="{{ Route::currentRouteName() == 'admin.products.index' ? 'm-menu__item  m-menu__item--active' : 'm-menu__item' }}" aria-haspopup="true">
            <a href="{{ route('admin.products.index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-dumbbell"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Products</span>
                        {{--<span class="m-menu__link-badge">--}}
                        {{--<span class="m-badge m-badge--danger">2</span>--}}
                        {{--</span> --}}
                    </span>
                </span>
            </a>
        </li>
    </ul>
</div>