@routes()

@yield('scripts')

<!--begin::Global Theme Bundle -->
<script src="{{ asset('metronic') }}/assets/demo/base/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

</body>

<!-- end::Body -->
</html>