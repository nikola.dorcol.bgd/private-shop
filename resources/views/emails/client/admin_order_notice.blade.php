@extends('emails.client.template')

@section('title')

    Nova Porudžbina

@endsection


@section('main')

    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
        Pristigla je nova porudžbina na sajt!
    </p>

    <br><br><br>

    <table style="font-family: arial, sans-serif; border-collapse: collapse; width: 100%;">
        <tr style="border: 1px solid #dddddd;">
            <th>Proizvod</th>
            <th>Varijanta</th>
            <th>Kolicina</th>
        </tr>
        @foreach($order->productVariants as $variant)
            <tr style="border: 1px solid #dddddd;">
                <td style="border: 1px solid #dddddd;">{{ $variant->product->name }}</td>
                <td style="border: 1px solid #dddddd;">

                    @foreach($variant->productAttributeValues as $av)

                        {{ $av->attribute->name }}: {{ $av->attributeValue->value }} <br>

                    @endforeach
                </td>
                <td style="border: 1px solid #dddddd;">{{ $variant->pivot->quantity }}</td>
            </tr>
        @endforeach
    </table>

@endsection
