@extends('emails.client.template')

@section('title')

    Potvrda Porudžbine

@endsection


@section('main')

    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
        Vaša porudžbina je uspešno kreirana!
    </p>

    <br>
    <br>

    @if(!$admin)
        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
           U najkracem vremenskom roku neko od naših zaposlenih će vas kontaktirati radi potvrde porudžbine!
        </p>
    @endif

    <br>
    <br>

    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
       Detalje vaše porudžbine možete videti na sledecem <a href="{{ route('client.order.show', $orderId) }}">linku</a>!
    </p>
    <br>
    <br>

@endsection