<html>
    <head>
        <style>
            /* -------------------------------------
                RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
            @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                }

                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                }

                table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                }

                table[class=body] .content {
                    padding: 0 !important;
                }

                table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important;
                }

                table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                }

                table[class=body] .btn table {
                    width: 100% !important;
                }

                table[class=body] .btn a {
                    width: 100% !important;
                }

                table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                }
            }

            /* -------------------------------------
                PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
                .ExternalClass {
                    width: 100%;
                }

                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                }

                .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                }

                #MessageViewBody a {
                    color: inherit;
                    text-decoration: none;
                    font-size: inherit;
                    font-family: inherit;
                    font-weight: inherit;
                    line-height: inherit;
                }

                .btn-primary table td:hover {
                    background-color: #34495e !important;
                }

                .btn-primary a:hover {
                    background-color: #34495e !important;
                    border-color: #34495e !important;
                }
            }
        </style>
    </head>
</html>
<body>
    <table border="0" cellpadding="0" cellspacing="0" class="body"
           style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
        <tr>
            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
            <td class="container"
                style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 768px; padding: 10px; width: 100%;">
                <div class="content"
                     style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 768px; padding: 10px;">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <span class="preheader"
                          style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">This is preheader text. Some clients will show this text as a preview.</span>
                    <table class="main"
                           style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">

                        <!-- START HEADER -->
                        <tr>
                            <td style="background-color: #272727; text-align: left; padding-left: 10px">
                                <a href="#"><img style="width: 120px" src="{{ asset('img/logo.png') }}"
                                                 alt=""></a>
                            </td>
                        </tr>


                        <!-- START MAIN CONTENT AREA -->
                        <tr style="width: 100%; height: 100%; background-position: center; background-repeat: no-repeat; background-size: cover; background-image: url({{ asset('/img/email/email-back.png') }})">
                            <td class="wrapper"
                                style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 40px 60px 100px 60px;">
                                <table border="0" cellpadding="0" cellspacing="0"
                                       style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                    <tr>
                                        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                                            <h2 style="font-family: sans-serif; font-size: 22px; font-weight: normal; margin: 0; Margin-bottom: 50px; text-align: center; font-style: normal">
                                                @yield('title')
                                            </h2>

                                            @yield('main')
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>

                    <!-- START FOOTER -->
                    <div class="footer" style="clear: both; text-align: center; width: 100%;">
                        <table border="0" cellpadding="0" cellspacing="0"
                               style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                            <tr>
                                <td style="background-color:#272727; font-family: sans-serif; vertical-align: middle; font-size: 12px; color: #999999; text-align: left;">
                                    <a href="www.harcfitness.rs" style="color: #ffffff; font-size: 16px; padding-left: 15px; font-style: normal">www.harcfitness.rs</a>
                                </td>
                                <td class="content-block"
                                    style=" width: 33%; background-color:#272727; font-family: sans-serif; vertical-align: middle; padding-bottom: 10px; font-style: normal; padding-top: 10px; font-size: 12px; color: #ffffff; text-align: left;">
                                                                        <div style="margin-bottom: 5px">
                                                                            <img style="width: 20px; margin-right: 3px" src="{{asset('/img/email/face.png')}}">
                                                                            <img style="width: 20px; margin-left: 3px" src="{{asset('/img/email/insta.png')}}">
                                                                        </div>
                                    <p style="margin-bottom: 5px">Internet Prodaja, 11080 Zemun</p>
                                    <p style="margin-bottom: 5px">Tel: +381 64 469 72 70</p>
                                    <p style="margin-bottom: 5px">Email: harcstefan@gmail.com</p>
                                    {{--                                    <span class="apple-link"--}}
                                    {{--                                          style="color: #999999; font-size: 12px; text-align: center;">Harc fitness, 11000 Beograd, Srbija</span>--}}
                                    {{--                                    <br> Ne zelite da dobijate email-ove od nas? <a href="#"--}}
                                    {{--                                                                                    style="text-decoration: underline; color: #999999; font-size: 12px; text-align: center;">Unsubscribe</a>.--}}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- END FOOTER -->

                    <!-- END CENTERED WHITE CONTAINER -->
                </div>
            </td>
            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        </tr>
    </table>
</body>