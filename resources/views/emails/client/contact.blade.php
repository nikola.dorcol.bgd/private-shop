@extends('emails.client.template')

@section('title')

    Nova Poruka

@endsection


@section('main')

    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
        Ime: {{ $contact['name'] }}
    <br>
    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
        Email: {{ $contact['email'] }}
    </p>

    <br>

    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
        Poruka: {{ $contact['message'] }}
    </p>
    <br>
    <br>

@endsection