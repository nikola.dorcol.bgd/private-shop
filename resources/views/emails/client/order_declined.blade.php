@extends('emails.client.template')

@section('title')

    Porudžbina Odbijena

@endsection


@section('main')

    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
        Vaša porudžbina je odbijena!
    </p>

    @if($order->admin_comment !== '')
        <br>
        <br>

        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
            {{ $order->admin_comment }}
        </p>
    @endif

    <br>
    <br>

    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
        Detalje vaše porudžbine možete videti na sledecem <a href="{{ route('client.order.show', $order->id) }}">linku</a>!
    </p>
    <br>
    <br>

@endsection