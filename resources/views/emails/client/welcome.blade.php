@extends('emails.client.template')

@section('title')

    DOBRO DOŠLI

@endsection


@section('main')

    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
        Dobro došli, {{ $name }},</p>
    <br>
    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
        Uspešno ste se registrovali na
        <a style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;" href="{{ route('client.home') }}">Harc Fitness</a>
    </p>

    <br>
    <br>

    @if($verificationUrl)
        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;">
            <b>
            Kako biste koristili naše usluge molimo Vas da verifikujete vašu e-mail adresu na sledećem
            <a style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;" href="{{ $verificationUrl }}">Linku</a>
            </b>
        </p>
        <br>
        <br>
    @endif

    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
        Za bilo kakve nejasnoće možete nas kontaktirati na sledeću email harcstefan@gmail.com
    </p>

@endsection