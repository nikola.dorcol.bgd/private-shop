<link rel="stylesheet" href="{{ asset('css/app.css') }}">

{{--<script src="{{ mix('css/app.css') }}"></script>--}}
<script src="{{ asset('js/app.js') }}"></script>


<nav class="">
    <div class="shop-navigation">
        <div class="shop-basketWrap">
            <img src="{{asset('/img/cart.png')}}" alt="Harc Fitness Korpa">
            <a href="#">
                Korpa
            </a>
        </div>
        <div class="shop-userWrap">
            <img src="{{asset('/img/user.png')}}" alt="Harc Fitness Korisnik">
            <a href="">Korisnik</a>
        </div>
    </div>
    <div class="main-navigation">
        <a class="main-logo" href="">
            <img src="{{asset('/img/logo.png')}}" alt="Harc Fitness">
        </a>

        <ul class="nav justify-content-center">
            <li class="nav-item">
                <a class="nav-link" href="#">Pocetna</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Proizvodi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="#">O nama</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Kontakt</a>
            </li>
        </ul>
    </div>
</nav>

<div class="about-wrapper">
    <section class="about container">
        <div class="row">
            <h1 class="col-sm-12 title">
                O nama
            </h1>
        </div>

        <div class="row">
            <div class="col-lg-6 order-1 order-lg-0">
                <div class="about-images">
                    <img src="{{asset('/img/about/lik1.jpg')}}" alt="Harc Fitness O Nama">
                    <img src="{{asset('/img/about/lik2.jpg')}}" alt="Harc Fitness O Nama">
                </div>
                <div class="about-image">
                    <img src="{{asset('/img/about/tegovi.jpg')}}" alt="Harc Fitness O Nama">
                </div>
            </div>
            <div class="col-lg-6 order-0">
                <h2 class="subtitle">
                    Bavimo se
                </h2>
                <p class="content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium amet asperiores consectetur
                    deleniti
                    dolorem eius, eligendi ipsum iusto laborum laudantium minus nesciunt, nobis officiis perferendis
                    possimus
                    quasi similique ut velit.
                </p>

                <h2 class="subtitle">
                    Jos nesto o firmi
                </h2>
                <p class="content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium amet asperiores consectetur
                    deleniti
                    dolorem eius, eligendi ipsum iusto laborum laudantium minus nesciunt, nobis officiis perferendis
                    possimus
                    quasi similique ut velit.
                </p>
            </div>
        </div>
    </section>

    <section class="about container">
        <div class="row">
            <h1 class="col-sm-12 title">
                Nasi klijenti
            </h1>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="our-clients main-preview">
                    <div class="item">
                        <img src="{{asset('/img/about/tegovi.jpg')}}">
                    </div>
                    <div class="item">
                        <img src="{{asset('/img/about/tegovi.jpg')}}">
                    </div>
                    <div class="item">
                        <img src="{{asset('/img/about/tegovi.jpg')}}">
                    </div>
                    <div class="item">
                        <img src="{{asset('/img/about/tegovi.jpg')}}">
                    </div>
                    <div class="item">
                        <img src="{{asset('/img/about/tegovi.jpg')}}">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<footer>
    <div class="row">
        <div class="col">
            <a href="#" class="footer-logo">
                <img src="{{asset('/img/graylogo.png')}}">
            </a>
        </div>
        <div class="col">
            <ul class="list-group">
                <li class="list-group-item border-0">Harcfitnes</li>
                <li class="list-group-item border-0">Pocetna</li>
                <li class="list-group-item border-0">Proizvodi</li>
                <li class="list-group-item border-0">O nama</li>
                <li class="list-group-item border-0">Kontakt</li>
            </ul>
        </div>
        <div class="col">
            <ul class="list-group">
                <li class="list-group-item border-0">Kategorije</li>
                <li class="list-group-item border-0">Tegovi</li>
                <li class="list-group-item border-0">vijace</li>
                <li class="list-group-item border-0">kugle</li>
                <li class="list-group-item border-0">strunjace</li>
                <li class="list-group-item border-0">flase</li>
                <li class="list-group-item border-0">steznici</li>
            </ul>
        </div>
        <div class="col">
            <ul class="list-group">
                <li class="list-group-item border-0">Zastita potrosaca?</li>
                <li class="list-group-item border-0">Garancija</li>
                <li class="list-group-item border-0">Kvalitet</li>
                <li class="list-group-item border-0">Dostava</li>
                <li class="list-group-item border-0">Brzina</li>
            </ul>
        </div>
        <div class="col">
            <ul class="list-group">
                <li class="list-group-item border-0">Kontakt</li>
                <li class="list-group-item border-0">
                    <a href="tel:+381644697270">
                        <img src="{{asset('/img/mobileicon.png')}}" alt="phone">
                        +381 64 469 72 70
                    </a>
                    <a href="mailto:harcstefan@gmail.com?Subject=Hello%20again" class="email">
                        harcstefan@gmail.com
                    </a>
                </li>
                <li class="list-group-item border-0">
                    <a href="tel:+381644697270">
                        <img src="{{asset('/img/facebook.png')}}" alt="phone">
                    </a>
                    <a href="tel:+381644697270">
                        <img src="{{asset('/img/instagram.png')}}" alt="phone">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>
