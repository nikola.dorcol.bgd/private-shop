@extends('client.template')

@section('metas')

    <meta name="title" content="Harc Fitness Zaboravljena Lozinka"/>
    <meta name="description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici"/>
    <meta property="og:locale" content="sr_RS" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="HarcFitness Zaboravljena Lozinka" />
    <meta property="og:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:site_name" content="Harc Fitness" />
    <meta property="og:image" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:secure_url" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta name="twitter:card" content="Harc Fitness Zaboravljena Lozinka" />
    <meta name="twitter:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta name="twitter:title" content="Harc Fitness Zaboravljena Lozinka" />
    <meta name="twitter:image" content="{{ asset('img/meta_logo.png') }}" />

@endsection

@section('main')

    <div class="container login">

        <div class="row">

            <div class="col-md-8 offset-md-2">

                <p class="subtitle heading text-center mb-5">
                    Zaboravili Ste Lozinku?
                </p>

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        Uspešno smo vam poslali email sa daljim upustvima!
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="col-sm-3 col-form-label subtitle">Korisnički email</label>

                        <div class="col-sm-5">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row submit">
                        <div class="col-sm-8">
                            <button type="submit" class="btn btn-outline-warning">Pošalji</button>
                        </div>
                    </div>
                </form>

                <div class="registration text-center mb-5">
                    <p class="subtitle heading">Ukoliko nemate nalog <span>Registrujte se</span></p>
                    <a href="{{ route('register') }}" class="btn btn-outline-warning">Registruj se</a>
                </div>

            </div>

        </div>

    </div>

@endsection
