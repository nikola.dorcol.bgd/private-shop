@extends('client.template')

@section('metas')

    <meta name="title" content="Harc Fitness Registracija"/>
    <meta name="description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici"/>
    <meta property="og:locale" content="sr_RS" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="HarcFitness Registracija" />
    <meta property="og:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:site_name" content="Harc Fitness" />
    <meta property="og:image" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:secure_url" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta name="twitter:card" content="Harc Fitness Registracija" />
    <meta name="twitter:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta name="twitter:title" content="Harc Fitness Registracija" />
    <meta name="twitter:image" content="{{ asset('img/meta_logo.png') }}" />

@endsection

@section('main')

    <div class="container register mb-5">

        <div class="row mt-5">
            <p class="subtitle heading text-center col-sm-12 mb-5">
                Registruj se na <span>HarcFitness.rs</span>
            </p>

            @error('message')
                <div class="alert alert-danger" style="width: 100%; text-align: center;">{{ $message }}</div>
            @enderror
        </div>

        <form class="mb-5" method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="validationDefault01">Ime i Prezime</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="validationDefault01" value="{{ old('name') }}" required>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationDefault02">Broj Telefona</label>
                    <input type="text" name="phone_number" placeholder="+381..." class="form-control @error('phone_number') is-invalid @enderror" value="{{ old('phone_number') }}" id="validationDefault02" required>
                    @error('phone_number')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationDefaultEmail">Email</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend2">@</span>
                        </div>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" id="validationDefaultEmail"
                               aria-describedby="inputGroupPrepend2" required>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="validationDefaultPass">Lozinka</label>
                    <div class="input-group">
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="validationDefaultPass"
                               aria-describedby="inputGroupPrepend7" required>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                </div>
                <div class="col-md-6 mb-3">
                    <label for="validationDefaultRepeatPass">Potvrda Lozinke</label>
                    <div class="input-group">
                        <input type="password" name="password_confirmation" class="form-control @error('password') is-invalid @enderror" id="validationDefaultRepeatPass"
                               aria-describedby="inputGroupPrepend8" required>
                    </div>
                </div>
            </div>
            {{--<div class="form-row">--}}
                {{--<div class="col-md-6 mb-3">--}}
                    {{--<label for="validationDefault03">Address</label>--}}
                    {{--<input type="text" class="form-control" id="validationDefault03" required>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 mb-3">--}}
                    {{--<label for="validationDefault04">City</label>--}}
                    {{--<select class="custom-select" id="validationDefault04" required>--}}
                        {{--<option selected disabled value="">Choose...</option>--}}
                        {{--<option value="beograd">Beograd</option>--}}
                        {{--<option value="novi_sad">Novi Sad</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 mb-3">--}}
                    {{--<label for="validationDefault05">Zip</label>--}}
                    {{--<input type="text" class="form-control" id="validationDefault05" required>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-row">
                <div class="form-check col-md-6 mb-3">
                    <input class="form-check-input ml-0" type="checkbox" value="" id="invalidCheck2" required>
                    <label class="form-check-label ml-3" for="invalidCheck2">
                        Prihvatam uslove korišćenja.
                    </label>
                </div>
                <div class="col-md-6 mb-3 text-right">
                    <a href="{{ route('login') }}" class="btn btn-secondary" type="submit">Uloguj se</a>
                    <button class="btn btn-primary" type="submit">Registruj se</button>
                </div>
            </div>
        </form>
    </div>
@endsection
