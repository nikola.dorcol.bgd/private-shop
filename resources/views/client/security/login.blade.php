@extends('client.template')

@section('metas')

    <meta name="title" content="Harc Fitness Prijava"/>
    <meta name="description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici"/>
    <meta property="og:locale" content="sr_RS" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="HarcFitness Prijava" />
    <meta property="og:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:site_name" content="Harc Fitness" />
    <meta property="og:image" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:secure_url" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta name="twitter:card" content="Harc Fitness Prijava" />
    <meta name="twitter:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta name="twitter:title" content="Harc Fitness Prijava" />
    <meta name="twitter:image" content="{{ asset('img/meta_logo.png') }}" />

@endsection

@section('main')

    <div class="container login">

        <div class="row">

            <div class="col-md-8 offset-md-2">

                <p class="subtitle heading text-center mb-5">
                    Ulogujte se na <span>HarcFitness.rs</span>
                </p>

                @if (session('status'))
                    <div class="alert alert-success" role="alert" style="width: 100%; text-align: center">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group row">
                        <label for="username" class="col-sm-3 col-form-label subtitle">Korisnički email</label>
                        <div class="col-sm-5">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label subtitle">Lozinka</label>
                        <div class="col-sm-5">
                            <input id="password" type="password" class="form-control @error('email') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row submit">
                        <div class="col-sm-8">
                            @if (Route::has('password.request'))
                                <a class="forgot-pass" href="{{ route('password.request') }}">
                                    Zaboravili ste lozinku?
                                </a>
                            @endif
                            <button type="submit" class="btn btn-outline-warning">Ulogujte se</button>
                        </div>
                    </div>
                </form>

                <p class="subtitle text-center mb-5">
                    Ulogujte se preko:
                    <span class="ml-1 mr-1"><a href="{{ route('client.auth.login', 'facebook') }}"><i class="fab fa-facebook"></i></a></span>
                    {{--<span><a href="#"><i class="fab fa-google"></i></a></span>--}}
                </p>

                <div class="registration text-center mb-5">
                    <p class="subtitle heading">Ukoliko nemate nalog <span>Registrujte se</span></p>
                    <a href="{{ route('register') }}" class="btn btn-outline-warning">Registruj se</a>
                </div>

            </div>

        </div>

    </div>

@endsection
