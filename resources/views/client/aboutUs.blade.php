@extends('client.template')

@section('metas')

    <meta name="title" content="Harc Fitness O Nama"/>
    <meta name="description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici"/>
    <meta property="og:locale" content="sr_RS" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="HarcFitness O Nama" />
    <meta property="og:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta property="og:url" content="{{ route('client.about_us') }}" />
    <meta property="og:site_name" content="Harc Fitness" />
    <meta property="og:image" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:secure_url" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta name="twitter:card" content="Harc Fitness O Nama" />
    <meta name="twitter:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta name="twitter:title" content="Harc Fitness O Nama" />
    <meta name="twitter:image" content="{{ asset('img/meta_logo.png') }}" />

@endsection

@section('main')

<div class="about-wrapper">
    <section class="about container" style="padding-bottom: 0px">
        <div class="row">
            <h1 class="col-sm-12 title">
                O nama
            </h1>
        </div>

        <div class="row">
            <div class="col-lg-6 order-1 order-lg-0">
                <div class="about-images">
                    <img src="{{asset('/img/about/lik1.jpg')}}" alt="Harc Fitness O Nama">
                    <img src="{{asset('/img/about/lik2.jpg')}}" alt="Harc Fitness O Nama">
                </div>
                <div class="about-image">
                    <img src="{{asset('/img/about/tegovi.jpg')}}" alt="Harc Fitness O Nama">
                </div>
            </div>
            <div class="col-lg-6 order-0">
                <h2 class="subtitle">
                    Bavimo se
                </h2>
                <p class="content">
                    Više od 10 godina iskustva u svetu sporta i fitnesa objedinili smo u jednu celinu, koja od 2016. godine nosi naziv "Harc Fitness".
                    <br>Posvetili smo se poboljšanju treninga kako profesionalnih sportista, tako i rekreativaca, kreirajući najinovativnije proizvode jedinstvenog dizajna.
                    <br>
                    <br>
                    Prvenstvo smo fokusirani na funkcionalni fitnes i trening snage.
                    <br>No, u želji da dodatno napredujemo i učvrstimo poziciju na tržištu, nastojimo da naš asortiman obogatimo i opremom za jogu, pilates,  plivanje, kao i druge aktuelne sportove.
                </p>

                <h2 class="subtitle">
                    Vizija
                </h2>
                <p class="content">
                    Od svog nastanka pa sve do danas, "Harc Fitness" ima jedinstvenu viziju da svojim kupcima po veoma pristupačnim cenama pruži proizvode vrhunskog kvaliteta, uz mnoštvo pažljivo osmišljenih detalja.
                    <br>Ostvarenje ovog cilja rezultat je napornog rada, snažne volje ali i velike ljubavi prema sportu.
                </p>

                <h2 class="subtitle">
                    Proizvodi
                </h2>
                <p class="content">
                    Stoga, ako ste u potrazi za prosečnim proizvodima standardnog kvaliteta, one koje nudi svaki veći market ili prodavnica u komšiluku, znate kome da se obratite.
                    <br>S druge strane, ako tražite ruske girje, olimpijske šipke, vijače i druge rekvizite koji su stvoreni da traju i da vam služe pri svakom treningu, onda ih nećete naći na tim mestima. Za takve potrage, "Harc Fitness" je jedina destinacija.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p class="content" style="margin-top: 20px">
                    Ukoliko nam ne verujete na reč, o kvalitetu naših proizvoda i usluga možete čuti od brojnih klijenata, partnera i saradnika. Ipak, mi verujemo da je neposredni utisak nezamenljiv, pa vas pozivamo da se u sve navedeno lično uverite i postanete deo naše zajednice!
                </p>
            </div>
        </div>
    </section>

    <section class="about container">
        <div class="row">
            <h1 class="col-sm-12 title">
                Naši klijenti
            </h1>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="our-clients main-preview">
                    <div class="item" onclick="window.location='https://www.gym.rs/'">
                        <img src="{{asset('/img/about/Athletics-gym.png')}}" alt="Harc Fitness Saradnik">
                    </div>
                    <div class="item" onclick="window.location='https://bodyline-fitness-wellness.business.site/'">
                        <img src="{{asset('/img/about/Body-LINE.png')}}" alt="Harc Fitness Saradnik">
                    </div>
                    <div class="item" onclick="window.location='http://www.sinergija.rs/'">
                        <img src="{{asset('/img/about/Centar-sinergija.png')}}" alt="Harc Fitness Saradnik">
                    </div>
                    <div class="item" onclick="window.location='https://www.legionari.net/'">
                        <img src="{{asset('/img/about/Elite-Fitness-Legionari.png')}}" alt="Harc Fitness Saradnik">
                    </div>
                    <div class="item" onclick="window.location='http://www.factorynis.com/'">
                        <img src="{{asset('/img/about/Factory-Nis.png')}}" alt="Harc Fitness Saradnik">
                    </div>
                    <div class="item" onclick="window.location='https://prvitim.rs/'">
                        <img src="{{asset('/img/about/Prvi-Tim-Crossfit.png')}}" alt="Harc Fitness Saradnik">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

