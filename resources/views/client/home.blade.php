@extends('client.template')

@section('metas')

    <meta name="title" content="Harc Fitness Početna"/>
    <meta name="description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici"/>
    <meta property="og:locale" content="sr_RS" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="HarcFitness Početna" />
    <meta property="og:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:site_name" content="Harc Fitness" />
    <meta property="og:image" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:secure_url" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta name="twitter:card" content="Harc Fitness Početna" />
    <meta name="twitter:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta name="twitter:title" content="Harc Fitness Početna" />
    <meta name="twitter:image" content="{{ asset('img/meta_logo.png') }}" />

@endsection

@section('main')

    <div class="slider-wrapper">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img style="width: 150px" src="{{asset('/img/slider/slider1.png')}}" class="d-block w-100" alt="Harc Fitness Medicinke">
                </div>
                <div class="carousel-item">
                    <img style="width: 150px" src="{{asset('/img/slider/slider2.png')}}" class="d-block w-100" alt="Harc Fitness Tegovi">
                </div>
                <div class="carousel-item">
                    <img style="width: 150px" src="{{asset('/img/slider/slider3.png')}}" class="d-block w-100" alt="Harc Fitness Elasticne Trake">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    {{--<div class="home-back">--}}
        {{--<img class="home-left" src="{{asset('/img/left-back.jpg')}}">--}}
        {{--<img class="home-right" src="{{asset('/img/right-back.jpg')}}">--}}

        {{--<div class="why-wrapper">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-sm-8">--}}
                        {{--<h2 class="title">Zasto kod nas?</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="row">--}}
                    {{--<div class="col-sm-12 col-md-3 why-frame">--}}
                        {{--<img src="{{asset('/img/truck.png')}}">--}}
                        {{--<p>Brzina isporuke</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-12 col-md-3 why-frame">--}}
                        {{--<img src="{{asset('/img/discount.png')}}">--}}
                        {{--<p>Popust na dostavu</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-12 col-md-3 why-frame">--}}
                        {{--<img src="{{asset('/img/badge.png')}}">--}}
                        {{--<p>Kvalitet proizvoda</p>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-12 col-md-3 why-frame">--}}
                        {{--<img src="{{asset('/img/mobile.png')}}">--}}
                        {{--<p>Online porucivanje</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="recommend-wrapper recommend-wrapper-home">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="title">Preporučujemo za vas</h2>
                    </div>
                </div>

                <div class="row">
                    @foreach($recommended_products as $product)
                        <div class="col-6 col-lg-3" onclick="window.location='{{ route('client.product', [$product->id, $product->slug]) }}';">
                            <div class="product-frame">
                                <img src="{{ route('images.cache', ['medium', $product->main_image->src]) }}" alt="{{ $product->name }}">

                                <div class="text-frame">
                                    <h4><a href="{{ route('client.product', [$product->id, $product->slug]) }}">{{ $product->name }}</a></h4>
                                </div>

                                @if($product->active)
                                    <p class="price">Cena Od: {{ number_format((float) $product->cheapestVariant->price, 2, '.', '') }} rsd</p>
                                @else
                                    <p class="price">Proizvod nije dostupan</p>
                                @endif

                                <a href="{{ route('client.product', [$product->id, $product->slug]) }}" class="btn btn-outline-primary">DETALJNIJE</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    {{--</div>--}}

    <div class="slider-wrapper">
        <img width="100%" height="100%" src="{{asset('/img/baner.png')}}" alt="Harc Fitnes Sportska Oprema">
    </div>

    <div class="recommend-wrapper recommend-wrapper-home">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="title">Najnoviji proizvodi</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="similar-products">
                        @foreach($latest_products as $product)
                            <div class="product-frame" onclick="window.location='{{ route('client.product', [$product->id, $product->slug]) }}';">
                                <img src="{{ route('images.cache', ['medium', $product->main_image->src]) }}" alt="{{ $product->name }}">

                                <div class="text-frame">
                                    <h4><a href="{{ route('client.product', [$product->id, $product->slug]) }}">{{ $product->name }}</a></h4>
                                </div>

                                @if($product->active)
                                    <p class="price">Cena Od: {{ number_format((float) $product->cheapestVariant->price, 2, '.', '') }} rsd</p>
                                @else
                                    <p class="price">Proizvod nije dostupan</p>
                                @endif

                                <a href="{{ route('client.product', [$product->id, $product->slug]) }}" class="btn btn-outline-primary">DETALJNIJE</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
