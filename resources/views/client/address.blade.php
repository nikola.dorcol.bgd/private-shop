@extends('client.template')

@section('main')

    <div class="container checkout-address">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-md-10">
                <form>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Ime</label>
                            <input type="text" class="form-control" id="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="last">Prezime</label>
                            <input type="text" class="form-control" id="last">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="street">Ulica i broj</label>
                            <input type="text" class="form-control" id="street">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="city">Grad</label>
                            <select id="city" class="form-control">
                                <option selected>Izaberi...</option>
                                <option>Novi Sad</option>
                                <option>Beograd</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="mobile">Broj telefona</label>
                            <input type="tel" class="form-control" id="mobile">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-xs-12 col-sm-6 text-center text-sm-left mb-2 mb-sm-0">
                            <a class="btn btn-outline-dark" href="#">Prethodni korak</a>
                        </div>
                        <div class="col-xs-12 col-sm-6 text-center text-sm-right">
                            <a class="btn btn-primary" href="#">Sledeci korak</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

@endsection
