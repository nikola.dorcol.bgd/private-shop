<nav class="">
    <div class="shop-navigation">
        <div class="shop-basketWrap">
            <a href="{{ route('client.cart.' . (auth()->check() ? 'index' : 'preview')) }}">
                <i class="fas fa-shopping-cart"></i>
                <span class="shop-basket-items">{{ $productsInCart }}</span>
                <span>Korpa</span>
            </a>
        </div>
        <div class="shop-userWrap">
            <a href="#"
               class="dropdown-toggle"
               role="button"
               id="registration"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false"
            >
                <i class="fas fa-user"></i>
                <span>{{ auth()->check() ? auth()->user()->name : 'Korisnik' }}</span>
            </a>
            <div class="dropdown-menu registration-dropdown" aria-labelledby="registration">
                @if(! auth()->check())
                    <a class="dropdown-item" href="{{ route('login') }}">Uloguj se</a>
                    <a class="dropdown-item" href="{{ route('register') }}">Registruj se</a>
                @else
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Izloguj se
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endif
            </div>
        </div>
    </div>
    <div class="main-navigation">

        <a class="main-logo" href="{{ route('client.home') }}">
            <img src="{{asset('/img/logo.png')}}" alt="Harc Fitness Logo">
        </a>

        <nav class="navbar navbar-expand-lg">

            {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#smallNav"--}}
                    {{--aria-controls="smallNav" aria-expanded="false" aria-label="Toggle navigation">--}}
                {{--<i class="fas fa-bars"></i>--}}
            {{--</button>--}}

            {{--<div class="collapse navbar-collapse" id="smallNav">--}}

                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'client.home' ? 'active' : '' }}"
                           href="{{ route('client.home') }}">Početna</a>
                    </li>
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'client.products' ? 'active' : '' }}"--}}
                           {{--href="{{ route('client.search') }}">Proizvodi</a>--}}
                    {{--</li>--}}
                    <li class="nav-item">
                        <a class="nav-link {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'client.about_us' ? 'active' : '' }}"
                           href="{{ route('client.about_us') }}">O nama</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'client.contact' ? 'active' : '' }}"
                           href="{{ route('client.contact') }}">Kontakt</a>
                    </li>
                </ul>

            @if(\Illuminate\Support\Facades\Route::currentRouteName() != 'client.contact')

                <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

            @endif

            {{--</div>--}}
        </nav>
    </div>
    @if(\Illuminate\Support\Facades\Route::currentRouteName() != 'client.contact')
        <div class="large-navigation">
            <nav class="navbar navbar-expand-lg navbar-light" style="padding: 0 !important;">
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        @foreach($categories as $category)
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="{{ route('client.search', [$category->id, $category->slug]) }}" id="navbarDropdownMenuLink" role="button"
                                   data-toggle="" aria-haspopup="true" aria-expanded="true">

                                    <div class="item-frame">
                                        @if($category->icon)
                                            <img class="item-img" src="{{ route('images.cache', ['original', $category->icon->src]) }}" alt="Harc Fitness {{ $category->name }}">
                                        @endif
                                        <p class="item-name">{{ $category->name }}</p>
                                    </div>
                                </a>
                                @if ($category->children->count())
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <div class="d-flex">
                                            <div>
                                                {{--<h4 class="dropdown-item">{{ $category->name }}</h4>--}}
                                                @foreach($category->children as $child)
                                                    <a class="dropdown-item" href="{{ route('client.search', [$category->id, $category->slug, 'subCategories' => $child->id]) }}">{{ $child->name }}</a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </nav>
        </div>
    @endif
</nav>
