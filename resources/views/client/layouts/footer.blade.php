<footer>
    <div class="row text-center text-md-left">
        <div class="col-sm-12 col-md">
            <a href="{{ route('client.home') }}" class="logo">
                <img src="{{ asset('/img/logo.png') }}" alt="Harc Fitness Logo">
            </a>
        </div>
        <div class="col-sm-6 col-md mt-3 mt-md-0">
            <ul class="list-group">
                <li class="list-group-item border-0">Harcfitnes</li>
                <li class="list-group-item border-0"><a href="{{ route('client.home') }}">Početna</a></li>
                <li class="list-group-item border-0"><a href="{{ route('client.about_us') }}">O nama</a></li>
                <li class="list-group-item border-0"><a href="{{ route('client.contact') }}">Kontakt</a></li>
            </ul>
        </div>
        <div class="col-sm-6 col-md mt-3 mt-md-0">
            <ul class="list-group">
                <li class="list-group-item border-0">Kategorije</li>
                @foreach($categories as $category)
                    <li class="list-group-item border-0"><a href="{{ route('client.search', $category->id, $category->slug) }}">{{ ucfirst($category->name) }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="col-sm-6 col-md mt-3 mt-md-0">
            <ul class="list-group">
                <li class="list-group-item border-0">Zaštita potrošaca?</li>
                <li class="list-group-item border-0">Garancija</li>
                <li class="list-group-item border-0">Kvalitet</li>
                <li class="list-group-item border-0">Dostava</li>
                <li class="list-group-item border-0">Brzina</li>
            </ul>
        </div>
        <div class="col-sm-6 col-md mt-3 mt-md-0">
            <ul class="list-group">
                <li class="list-group-item border-0">Kontakt</li>
                <li class="list-group-item border-0">
                    <a href="tel:+381644697270" class="contact-icon">
                        <i class="fas fa-phone"></i>
                        +381 64 469 72 70
                    </a>
                    <a href="{{ route('client.contact') }}" class="email">
                        harcstefan@gmail.com
                    </a>
                </li>
                <li class="list-group-item border-0">
                    <a href="#" class="social-icons">
                        <i class="fab fa-facebook-square"></i>
                    </a>
                    <a href="#" class="social-icons">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>
