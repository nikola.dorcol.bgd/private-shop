<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="fb:app_id" content="{{ config('services.facebook.client_id') }}"/>

    @yield('metas')


    <title>HarcFitness</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/') }}apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/') }}favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/') }}favicon-16x16.png">
    <link rel="manifest" href="{{ asset('/') }}site.webmanifest">

    <link rel="stylesheet" href="{{ mix('css/client/client.css') }}">

</head>
<div class="se-pre-con">
    <span class="spinner-border spinner-border-sm"
          role="status" aria-hidden="true"
          style="width: 3rem; height: 3rem; color: #69cce3; margin: auto; top: 45%; left: 48%; position: relative"></span>
</div>
<body>
