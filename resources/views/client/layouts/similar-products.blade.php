<div class="similar-slider recommend-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2 class="title">Slicni proizvodi</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="similar-products">
                    @foreach($product->relatedProducts as $product)
                        <div class="product-frame" onclick="window.location='{{ route('client.product', [$product->id, $product->slug]) }}';">
                            <img src="{{ route('images.cache', ['medium', $product->main_image->src]) }}" alt="{{ $product->name }}">

                            <div class="text-frame">
                                <h4><a href="{{ route('client.product', [$product->id, $product->slug]) }}">{{ $product->name }}</a></h4>
                            </div>

                            @if($product->active)
                                <p class="price">Cena Od: {{ number_format((float) $product->cheapestVariant->price, 2, '.', '') }} rsd</p>
                            @else
                                <p class="price">Proizvod nije dostupan</p>
                            @endif

                            <a href="{{ route('client.product', [$product->id, $product->slug]) }}" class="btn btn-outline-primary">DETALJNIJE</a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
