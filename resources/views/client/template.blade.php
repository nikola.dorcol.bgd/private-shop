@include('client.layouts.head')

<body>

    @include('client.layouts.header')

    @yield('main')

    @include('client.layouts.footer')

</body>

@include('client.layouts.foot')