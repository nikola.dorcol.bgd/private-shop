@extends('client.template')

@section('metas')

    <meta name="title" content="Harc Fitness Korpa"/>
    <meta name="description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici"/>
    <meta property="og:locale" content="sr_RS" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="HarcFitness Korpa" />
    <meta property="og:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta property="og:url" content="{{ route('client.cart.index') }}" />
    <meta property="og:site_name" content="Harc Fitness" />
    <meta property="og:image" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:secure_url" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta name="twitter:card" content="Harc Fitness Korpa" />
    <meta name="twitter:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta name="twitter:title" content="Harc Fitness Korpa" />
    <meta name="twitter:image" content="{{ asset('img/meta_logo.png') }}" />

@endsection

@section('main')

    <div id="app" class="category-wrapper">
        <order-info :order-data="{{ $order }}"></order-info>
    </div>

@endsection

@section('scripts')

    <script src="{{ mix('js/client/order.js') }}"></script>

@endsection