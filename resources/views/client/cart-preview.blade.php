@extends('client.template')

@section('main')

    <div class="about-wrapper">
        <section class="about container">
            <div class="row">
                <h1 class="col-sm-12 title">
                    Privremena Korpa
                </h1>
            </div>

            @if(count($variants) < 1)

                <div class="row justify-content-center checkout-empty">
                    <div class="col-sm-12">
                        <i class="fas fa-shopping-cart"></i>
                        <p v-else>Trenutno nemate proizvoda u vašoj korpi.</p>
                    </div>
                </div>

            @else
                <div class="row justify-content-center">

                    <div class="col-xs-12 col-lg-10">

                        @foreach($variants as $variant)
                            <div class="checkout-frame">
                                <div class="checkout-product">
                                    <div class="checkout-product-img">
                                        <img src="{{ route('images.cache', ['medium', $variant->product->main_image->src]) }}" alt="{{ $variant->product->name }}">
                                    </div>

                                    <div class="checkout-product-info">
                                        <a href="{{ route('client.product', [ $variant->product->id, $variant->product->slug]) }}" target="_blank" class="link">{{ $variant->product->name }}
                                        </a>
                                        <br>
                                        @foreach($variant->productAttributeValues as $attributeValue)
                                            <p class="text" style="display: inline">
                                                <b> {{ $attributeValue->attribute->name }}:</b>
                                                <span class="{{ $attributeValue->attribute->name == 'boja' ? 'fa fa-square' : '' }}"
                                                      style="{{ $attributeValue->attribute->name == 'boja' ? 'color: ' . $attributeValue->attributeValue->value : '' }}">
                                            {{ $attributeValue->attribute->name == 'boja' ? '' : $attributeValue->attributeValue->value }}
                                        </span>
                                            </p>
                                        @endforeach
                                    </div>

                                </div>

                                <div class="checkout-price-wrapper">

                                    <p class="checkout-product-price">{{ $variant->price * session()->get('carts')[array_search($variant->id, array_column(session()->get('carts'), 'product_variant'))]['quantity'] }} rsd</p>

                                    <div class="add-product">
                                        <div class="plus-minus d-flex">
{{--                                            <button class="btn btn-outline-primary">--}}
{{--                                                <i class="fas fa-minus"></i>--}}
{{--                                            </button>--}}

                                            <input type="number" value="{{ session()->get('carts')[array_search($variant->id, array_column(session()->get('carts'), 'product_variant'))]['quantity'] }}" disabled>

{{--                                            <button class="btn btn-outline-primary">--}}
{{--                                                <i class="fas fa-plus"></i>--}}
{{--                                            </button>--}}
                                        </div>
                                    </div>

                                </div>

{{--                                <button class="btn btn-link">--}}
{{--                                    <i class="fas fa-times"></i>--}}
{{--                                </button>--}}
                            </div>
                        @endforeach
                    </div>

                </div>

                <div class="row justify-content-center">
                    <div class="col-xs-12 col-lg-10 text-right">
                        <small>Ulogujte se kako biste imali pun pristup proizvodima u korpi</small>
                        <a class="btn btn-primary" href="{{ route('login') }}">
                            Ulogujte Se
                        </a>
                    </div>
                </div>
                <!-- End Cart -->
            @endif
        </section>
    </div>

@endsection
