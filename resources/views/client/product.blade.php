@extends('client.template')

@section('metas')

    <meta name="title" content="{{ $product->name }}"/>
    <meta name="description" content="{{ $product->description }}"/>
    <meta property="og:locale" content="sr_RS" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $product->name }}" />
    <meta property="og:description" content="{{ $product->description }}" />
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:site_name" content="Harc Fitness" />
    <meta property="og:image" content="{{ route('images.cache', ['medium', $product->main_image->src]) }}" />
    <meta property="og:image:secure_url" content="{{ route('images.cache', ['medium', $product->main_image->src]) }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta name="twitter:card" content="{{ $product->name }}" />
    <meta name="twitter:description" content="{{ $product->description }}" />
    <meta name="twitter:title" content="{{ $product->name }}" />
    <meta name="twitter:image" content="{{ route('images.cache', ['medium', $product->main_image->src]) }}" />

@endsection

@section('main')

<div id="app" class="category-wrapper">
    <product-info :product-data="{{ $product }}" :attributes-values-data="{{ $attributes }}" :auth="{{ auth()->check() ? 1 : 0 }}"></product-info>
</div>

    @if($product->relatedProducts->count())
        @include('.client.layouts.similar-products')
    @endif

@endsection

@section('scripts')

    <script src="{{ mix('js/client/product.js') }}"></script>

@endsection
