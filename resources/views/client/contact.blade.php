@extends('client.template')

@section('metas')

    <meta name="title" content="Harc Fitness Kontakt"/>
    <meta name="description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici"/>
    <meta property="og:locale" content="sr_RS" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="HarcFitness Kontakt" />
    <meta property="og:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta property="og:url" content="{{ route('client.contact') }}" />
    <meta property="og:site_name" content="Harc Fitness" />
    <meta property="og:image" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:secure_url" content="{{ asset('img/meta_logo.png') }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta name="twitter:card" content="Harc Fitness Kontakt" />
    <meta name="twitter:description" content="Harc Fitness prodaja sportske opreme i rekvizita. Tegovi, šipke, trake, vijače, ruska zvona, ploče, bučice, steznici" />
    <meta name="twitter:title" content="Harc Fitness Kontakt" />
    <meta name="twitter:image" content="{{ asset('img/meta_logo.png') }}" />

@endsection

@section('main')

    <div class="contact-wrapper">
        <div class="contact-overlay"></div>
        <div class="container">
            <div class="row text-center">
                <h1 class="col-sm-12 title">
                    Kontakt
                </h1>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-5 contact-form">
                    <form method="POST" action="{{ route('client.contact.send') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Ime</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" placeholder="" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" placeholder="" required>
                        </div>
                        <div class="form-group">
                            <label for="message">Poruka</label>
                            <textarea type="text" class="form-control @error('message') is-invalid @enderror" id="message" name="message" placeholder="" required>
                                {{ old('message') }}
                            </textarea>
                        </div>

                        @if (session('status'))
                            <p class="content">
                                {{ session('status') }}
                            </p>
                        @endif

                        <button type="submit" class="btn btn-primary">
                            <i class="far fa-envelope"></i>
                            Pošalji</button>
                    </form>
                </div>
                <div class="col-sm-12 offset-md-1 col-md-5 contact-info">
                    <a href="https://goo.gl/maps/i3gizz6Vg4ExvgyW9" target="_blank">Internet Prodaja, 11080 Zemun</a>
                    <a href="tel:+381644697270" target="_blank">Tel: +381 64 469 72 70</a>
                    <a href="mailto:harcstefan@gmail.com" target="_blank">Email: harcstefan@gmail.com</a>
                </div>
            </div>
        </div>
    </div>

@endsection