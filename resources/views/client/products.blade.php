@extends('client.template')

@section('metas')
    <meta name="title" content="Harc Fitness, {{ $category->name }}"/>
    <meta name="description" content="{{ $category->description }}"/>
    <meta property="og:locale" content="sr_RS" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Harc Fitness, {{ $category->name }}" />
    <meta property="og:description" content="{{ $category->description }}" />
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:site_name" content="Harc Fitness" />
    <meta property="og:image" content="{{ route('images.cache', ['original', $category->icon->src]) }}" />
    <meta property="og:image:secure_url" content="{{ route('images.cache', ['original', $category->icon->src]) }}" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta name="twitter:card" content="Harc Fitness, {{ $category->name }}" />
    <meta name="twitter:description" content="{{ $category->description }}" />
    <meta name="twitter:title" content="Harc Fitness, {{ $category->name }}" />
    <meta name="twitter:image" content="{{ route('images.cache', ['original', $category->icon->src]) }}" />
@endsection

@section('main')

    <div class="category-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8">
                    <h2 class="title">Kategorija : {{ $category->name }}</h2>
                </div>
                <div class="col-sm-12 col-md-4">

                </div>
            </div>
        </div>
    </div>

    <div id="app" class="search-wrapper filter-style">
        <div class="container">
            <search :base-url="'{{ $products->currentPage() }}'" :next-page-url="'{{ $products->nextPageUrl() }}'">

                <template v-slot:products>

                    @forelse($products as $product)

                        <div class="col-sm-12 col-md-6 col-lg-4" onclick="window.location='{{ route('client.product', [$product->id, $product->slug]) }}';">

                            <div class="product-frame">

                                <img src="{{ route('images.cache', ['medium', $product->main_image->src]) }}" alt="{{ $product->name }}">

                                <div class="text-frame">
                                    <h4><a href="{{ route('client.product', [$product->id, $product->slug]) }}">{{ $product->name }}</a></h4>
                                </div>

                                @if($product->active)
                                    <p class="price">Cena Od: {{ number_format((float) $product->cheapestVariant->price, 2, '.', '') }} rsd</p>
                                @else
                                    <p class="price">Proizvod nije dostupan</p>
                                @endif
                                <a href="{{ route('client.product', [$product->id, $product->slug]) }}" class="btn btn-outline-primary">DETALJNIJE</a>
                            </div>
                        </div>

                    @empty

                        <h4 class="separate">Ne postoje proizvodi za datu pretragu</h4>

                    @endforelse
                </template>

                @if ($brands->count())

                    <template v-slot:brands="slotProps">

                        <div class="col-sm-12 pt-3 filter-wrapper">

                            <h5>Brendovi</h5>

                            <ul class="list-group">

                                @foreach($brands as $brand)

                                    <li class="list-group-item border-0 filter-item">

                                        <div class="input-group">

                                            <div class="input-group-prepend">

                                                <label class="container-checkbox">{{ $brand->name }}
                                                    <input type="checkbox" value="{{ $brand->id }}" v-model="slotProps.search.brands">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </template>
                @endif

                @if ($subCategories->count())

                    <template v-slot:categories="slotProps">

                        <div class="col-sm-12 pt-3 filter-wrapper">

                            <h5>Pod Kategorije</h5>

                            <ul class="list-group">

                                @foreach($subCategories as $category)

                                    <li class="list-group-item border-0 filter-item">

                                        <div class="input-group">

                                            <div class="input-group-prepend">

                                                <label class="container-checkbox">{{ $category->name }}
                                                    <input type="checkbox" value="{{ $category->id }}" v-model="slotProps.search.subCategories">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </template>
                @endif
            </search>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ mix('js/client/search.js') }}"></script>
@endsection