@extends('client.template')

@section('main')

    <div id="app" class="category-wrapper">
        <product-info user-data="{{ auth()->user()->toJson() }}"></product-info>
    </div>

@endsection

@section('scripts')

    <script src="{{ mix('js/client/summary.js') }}"></script>

@endsection