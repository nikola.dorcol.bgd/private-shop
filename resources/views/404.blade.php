@extends('client.template')

@section('main')

    <div class="container">
        <div class="row d-flex wrapper-404">
            <div class="col-md-6 d-flex align-content-center justify-content-center">
                <a class="main-logo" href="{{ route('client.home') }}">
                    <img src="{{ asset('/img/logo.png') }}">
                </a>
            </div>
            <div class="col-md-6">
                <h2>Greska 404</h2>
                <p>Nazalost, stranica nije pronadjena.</p>
                <a href="{{ route('client.home')  }}" class="btn btn-primary">Pocetna strana</a>
            </div>
        </div>
    </div>

@endsection
