{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}

        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@endsection--}}

@extends('client.template')

@section('main')

    <div class="container login">

        <div class="row">

            <div class="col-md-8 offset-md-2">

                <p class="subtitle heading text-center mb-5">
                    Verifikacija Email adrese!
                </p>

                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        Verifikacioni email je poslat!
                    </div>
                @endif

                <p class="subtitle text-center mb-5">
                    Pre nego sto zatrazite novi email proverite vase sanduce jos jednom! <br>
                    <a class="btn btn-primary" href="{{ route('verification.resend') }}">Posaljite</a>.
                </p>
            </div>

        </div>

    </div>

@endsection

